import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import loginRoutes from "routes/login";

import Header from "components/Header";

class Login extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="loginWrapper">
				<Switch>
					{loginRoutes.map((prop, key) => {
						return <Route path={`${this.props.match.url + prop.path}`} component={prop.component} key={key} />;
					})}
				</Switch>
			</div>
		);
	}
}

export default Login;
