import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import portalRoutes from "routes/portal";

import Header from "components/Header";

class Portal extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const RouteComponent = Route;
    return (
      <div className="portalWrapper">
        <Header />
        <Switch>
          {portalRoutes.map((prop, key) => {
            return <Route path={`${this.props.match.url + prop.path}`} component={prop.component} key={key} />;
          })}
        </Switch>
      </div>
    );
  }
}

export default Portal;
