import axios from "axios";

export const verifyAccount = async accountID => {
	const res = await axios.get("/api/accounts/" + accountID + "/verifyAccount");
	return res.data;
};

export const currencyFormat = (currency, num) => {
	const USD = 1;
	let prefix, separator, decimal;
	switch (currency) {
		case USD:
			prefix = "$";
			separator = ",";
			decimal = true;
			break;
		default:
			prefix = "Rp. ";
			separator = ".";
			decimal = false;
			break;
	}
	if (num || num === 0) {
		return decimal
			? prefix +
					num
						.toFixed(2)
						.toString()
						.replace(/\B(?=(\d{3})+(?!\d))/g, separator)
			: prefix + num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
	}
};
