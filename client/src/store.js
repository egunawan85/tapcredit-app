import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import { combineReducers } from "redux";

import authReducer from "modules/auth";
import snapReducer from "modules/snap";
import accountsReducer from "modules/accounts";
import schedulesReducer from "modules/schedules";
import withdrawReducer from "modules/withdraw";

const allReducers = Object.assign(
	{},
	accountsReducer,
	authReducer,
	snapReducer,
	schedulesReducer,
	withdrawReducer
);
const rootReducer = combineReducers(allReducers);

const store = createStore(rootReducer, {}, applyMiddleware(reduxThunk));

export default store;
