import Login from "views/login/Login";
import LostPassword from "views/login/LostPassword";
import SetNewPassword from "views/login/SetNewPassword";

var loginRoutes = [
	{
		path: "/login",
		name: "Login",
		component: Login
	},
	{
		path: "/lostPassword",
		name: "LostPassword",
		component: LostPassword
	},
	{
		path: "/setNewPassword",
		name: "setNewPassword",
		component: SetNewPassword
	}
];

export default loginRoutes;
