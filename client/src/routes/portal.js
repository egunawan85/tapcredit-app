import Dashboard from "views/portal/Dashboard";
import Withdraw from "views/portal/Withdraw";

var portalRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard
  },
  {
    path: "/withdraw",
    name: "Withdraw",
    component: Withdraw
  }
];
export default portalRoutes;
