import Portal from "layouts/Portal";
import Login from "layouts/Login";
import PortalRoute from "components/PortalRoute";
import LoggedOutRoute from "components/LoggedOutRoute";
import { Route } from "react-router-dom";

var indexRoutes = [
	{ path: "/portal", name: "Portal", routeType: PortalRoute, component: Portal },
	{ path: "/login", name: "Login", routeType: LoggedOutRoute, component: Login }
];

export default indexRoutes;
