import "materialize-css/dist/js/materialize.min.js";
import "materialize-css/dist/css/materialize.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import "assets/index.css";
import store from "store";

import indexRoutes from "./routes/index";

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<div>
				<Switch>
					{indexRoutes.map((prop, key) => {
						const RouteType = prop.routeType;
						return <RouteType path={prop.path} component={prop.component} key={key} />;
					})}
				</Switch>
			</div>
		</BrowserRouter>
	</Provider>,
	document.querySelector("#root")
);
