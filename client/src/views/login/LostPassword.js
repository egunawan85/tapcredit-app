import React, { Component } from "react";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import { resetPassword } from "modules/auth";

import "./LostPassword.css";
import logo from "assets/images/tc_logo1.png";

class LostPassword extends Component {
	constructor() {
		super();
		this.state = {
			emailError: false,
			emailErrorMsg: "",
			emailValue: "",
			success: false
		};
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.handleEmailChange = this.handleEmailChange.bind(this);
	}

	async submitEmail() {
		await this.props.resetPassword(this.state.emailValue);
		this.props.reset.found
			? await this.setState({
					success: true,
					emailValue: ""
			  })
			: await this.setState({
					emailError: true,
					emailErrorMsg: this.props.reset.message,
					emailValue: ""
			  });
	}

	async handleEmailChange(e) {
		await this.setState({
			emailValue: e.target.value
		});
	}

	async handleKeyPress(e) {
		if (e.key === "Enter") {
			await this.submitEmail();
		}
	}

	render() {
		const emailError = this.state.emailError ? true : false;

		return (
			<div className="lostPasswordContainer">
				<div className="lostPasswordPanel">
					<img src={logo} className="mainLogo" alt="tapcredit" />
					<div className="heading">
						<h2>Lupa Kata Sandi?</h2>
						{this.state.success ? (
							<div>
								<p className="successMessage">Silakan cek email untuk tautan mereset kata sandi</p>
								<div className="backButton">
									<a className="waves-effect waves-light btn" href="/login/login">
										Kembali
									</a>
								</div>
							</div>
						) : (
							<div>
								<div className="lostPasswordInputs">
									<p>Masukkan email Anda</p>
									<TextField
										error={emailError}
										id="outlined-email-input"
										label="Email"
										className="email"
										type="email"
										name="email"
										autoComplete="email"
										margin="normal"
										variant="outlined"
										onKeyPress={this.handleKeyPress}
										onChange={this.handleEmailChange}
										InputLabelProps={{
											FormLabelClasses: {
												root: "textFieldRoot",
												focused: "textFieldFocused",
												error: "error"
											}
										}}
									/>
									<p className="errorMsg">{this.state.emailErrorMsg}</p>
								</div>
								<div className="lostPasswordButton">
									<a className="waves-effect waves-light btn" onClick={e => this.submitEmail()}>
										Kirim email
									</a>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ reset }) {
	return { reset: reset };
}

export default connect(
	mapStateToProps,
	{ resetPassword }
)(LostPassword);
