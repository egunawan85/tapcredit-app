import React, { Component } from "react";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import { authenticateToken, setNewPassword } from "modules/auth";

import "./SetNewPassword.css";
import logo from "assets/images/tc_logo1.png";

class SetNewPassword extends Component {
	constructor() {
		super();
		this.state = {
			passwordError: false,
			passwordErrorMsg: "",
			passwordValue: "",
			isAuthenticated: false,
			success: false,
			token: ""
		};
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
	}

	async componentDidMount() {
		await this.setState({ token: this.props.location.pathname.split("/").slice(-1)[0] });
		await this.authenticateToken();
	}

	async authenticateToken() {
		await this.props.authenticateToken(this.state.token);
		this.props.auth.validToken
			? await this.setState({ isAuthenticated: true })
			: this.props.history.push("/login/lostPassword");
	}

	async handlePasswordChange(e) {
		await this.setState({
			passwordValue: e.target.value
		});
	}

	async handleKeyPress(e) {
		if (e.key === "Enter") {
			await this.submitPassword();
		}
	}

	async submitPassword() {
		await this.props.setNewPassword(this.state.token, this.state.passwordValue);
		console.log(this.props.auth);
		this.props.auth.newPassword.result
			? await this.setState({
					success: true,
					passwordValue: ""
			  })
			: await this.setState({
					passwordError: true,
					passwordErrorMsg: this.props.auth.newPassword.message,
					passwordValue: ""
			  });
	}

	createErrorMsg() {
		return Array.isArray(this.state.passwordErrorMsg) ? (
			this.state.passwordErrorMsg.map((prop, key) => {
				let msg;
				switch (prop) {
					case "The password must be at least 8 characters long.":
						msg = "Kata sandi harus mengandung minimal 8 karakter";
						break;
					case "The password must contain at least one uppercase letter.":
						msg = "Kata sandi harus mengandung satu huruf besar";
						break;
					case "The password must contain at least one number.":
						msg = "Kata sandi harus mengandung satu huruf besar";
						break;
					case "The password must contain at least one special character.":
						msg = "Kata sandi harus mengandung satu karakter khusus";
						break;
					case "The password must contain at least one lowercase letter.":
						msg = "Kata sandi harus mengandung satu huruf kecil";
						break;
					default:
						msg = prop;
						break;
				}
				return <p className="errorMsg">{msg}</p>;
			})
		) : (
			<p className="errorMsg">{this.state.passwordErrorMsg}</p>
		);
	}

	render() {
		const passwordError = this.state.passwordError ? true : false;
		if (!this.state.isAuthenticated) return null;
		return (
			<div className="setNewPasswordContainer">
				<div className="setNewPasswordPanel">
					<img src={logo} className="mainLogo" alt="tapcredit" />
					<div className="heading">
						<h2>Kata Sandi Baru</h2>
						{this.state.success ? (
							<div>
								<p className="successMessage">Kata Sandi Anda telah diganti, silakan coba masuk.</p>
								<div className="backButton">
									<a className="waves-effect waves-light btn" href="/login/login">
										Masuk
									</a>
								</div>
							</div>
						) : (
							<div>
								<div className="setNewPasswordInputs">
									<p>Masukkan kata sandi untuk Akun TapCredit Anda</p>
									<TextField
										error={passwordError}
										id="outlined-password-input"
										label="Kata sandi"
										className="password"
										type="password"
										autoComplete="current-password"
										margin="normal"
										variant="outlined"
										onChange={this.handlePasswordChange}
										onKeyPress={this.handleKeyPress}
										InputLabelProps={{
											FormLabelClasses: {
												root: "textFieldRoot",
												focused: "textFieldFocused",
												error: "error"
											}
										}}
									/>
									{this.createErrorMsg()}
								</div>
								<div className="setNewPasswordButton">
									<a className="waves-effect waves-light btn" onClick={e => this.submitPassword()}>
										Reset Kata Sandi
									</a>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ auth }) {
	return { auth: auth };
}

export default connect(
	mapStateToProps,
	{ authenticateToken, setNewPassword }
)(SetNewPassword);
