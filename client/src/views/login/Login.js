import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import { loginUser, fetchUser } from "modules/auth";
import parsePath from "parse-path";

import "./Login.css";
import "views/Login.css";
import logo from "assets/images/tc_logo1.png";

class Login extends Component {
	constructor() {
		super();
		this.state = {
			loginError: false,
			loginErrorMsg: "",
			emailValue: "",
			passwordValue: ""
		};
		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);
	}

	async handleEmailChange(e) {
		await this.setState({
			emailValue: e.target.value
		});
	}

	async handlePasswordChange(e) {
		await this.setState({
			passwordValue: e.target.value
		});
	}

	async handleKeyPress(e) {
		if (e.key === "Enter") {
			await this.submitLogin();
		}
	}

	async submitLogin() {
		await this.props.loginUser(this.state.emailValue, this.state.passwordValue);
		if (!this.props.login.authorized) {
			await this.setState({
				loginError: true,
				loginErrorMsg:
					this.props.login.message == "Missing credentials"
						? "Email atau kata sandi belum diisi"
						: this.props.login.message
			});
		}
		if (this.props.login.authorized) {
			await this.props.fetchUser();
			this.routeLogin();
		}
	}

	routeLogin() {
		const url = parsePath(window.location.href);
		const route = (url, path) => {
			return url.port
				? (window.location = `${url.protocol}://${url.resource}:${url.port + path}`)
				: (window.location = `${url.protocol}://${url.resource + path}`);
		};
		return route(url, "/api/router");
	}

	render() {
		const loginError = this.state.loginError ? true : false;
		const { classes } = this.props;
		return (
			<div className="loginContainer">
				<div className="loginPanel">
					<img src={logo} className="mainLogo" alt="tapcredit" />
					<div className="heading">
						<h2>Masuk</h2>
						<p>dengan Akun Anda</p>
						<div className="loginInputs">
							<TextField
								error={loginError}
								id="outlined-email-input"
								label="Email"
								className="email"
								type="email"
								name="email"
								autoComplete="email"
								margin="normal"
								variant="outlined"
								onChange={this.handleEmailChange}
								onKeyPress={this.handleKeyPress}
								InputLabelProps={{
									FormLabelClasses: {
										root: "textFieldRoot",
										focused: "textFieldFocused",
										error: "error"
									}
								}}
							/>
							<TextField
								error={loginError}
								id="outlined-password-input"
								label="Kata sandi"
								className="password"
								type="password"
								autoComplete="current-password"
								margin="normal"
								variant="outlined"
								onChange={this.handlePasswordChange}
								onKeyPress={this.handleKeyPress}
								InputLabelProps={{
									FormLabelClasses: {
										root: "textFieldRoot",
										focused: "textFieldFocused",
										error: "error"
									}
								}}
							/>
							<p className="errorMsg">{this.state.loginErrorMsg}</p>
						</div>
						<div className="lostPassword">
							<a href="/login/lostpassword">Lupa sandi?</a>
						</div>
						<div className="loginButton">
							<a className="waves-effect waves-light btn" onClick={e => this.submitLogin()}>
								Masuk
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ login, auth }) {
	return { login: login, auth: auth };
}

export default connect(
	mapStateToProps,
	{ loginUser, fetchUser }
)(Login);
