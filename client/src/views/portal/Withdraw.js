import React, { Component } from "react";
import { connect } from "react-redux";
import { getAccountDetails } from "modules/accounts/accountDetails";
import { setSelectedAmount, setPlan } from "modules/withdraw";

import WithdrawPanel from "./withdraw/WithdrawPanel";
import SchedulePanel from "./withdraw/SchedulePanel";

import "../Portal.css";

class Withdraw extends Component {
	constructor() {
		super();
		this.state = {
			loading: true
		};
		this.changeSelectedAmount = this.changeSelectedAmount.bind(this);
		this.refreshInvalidAmount = this.refreshInvalidAmount.bind(this);
		this.changeSelectedPlan = this.changeSelectedPlan.bind(this);
	}

	async componentDidUpdate() {
		if (this.props.selectedAccount && this.state.loading) {
			await this.props.getAccountDetails(this.props.selectedAccount);
			await this.props.setPlan(this.props.accountDetails.availablePlans[0]);
			await this.props.setSelectedAmount(this.props.accountDetails.availableCredit);
			await this.setState({ invalidAmount: false, loading: false });
		}
	}

	async changeSelectedAmount(selectedAmount) {
		await this.props.setSelectedAmount(parseInt(selectedAmount));
	}

	async refreshInvalidAmount() {
		await this.setState({ invalidAmount: false });
	}

	async changeSelectedPlan(e) {
		if (!this.state.loading) {
			const match = this.props.accountDetails.availablePlans.find(
				plan => plan.durationMonths === parseInt(e.target.value)
			);
			if (match) await this.props.setPlan(match);
		}
	}

	render() {
		if (this.state.loading || !this.props.accountDetails) return null;
		return (
			<div className="container">
				<div className="withdraw">
					<div className="widgetsContainer">
						<div className="widgetsMain">
							<WithdrawPanel
								selectAmount={this.changeSelectedAmount}
								invalidAmount={this.state.invalidAmount}
								refresh={this.refreshInvalidAmount}
								selectPlan={this.changeSelectedPlan}
							/>
							<SchedulePanel branch="main" />
						</div>
						<div className="widgetsSide" />
						<div className="widgetsExt">
							<SchedulePanel branch="ext" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ selectedAccount, accountDetails, selectedAmount, plan }) {
	return {
		selectedAccount: selectedAccount,
		accountDetails: accountDetails,
		selectedAmount: selectedAmount,
		plan: plan
	};
}

export default connect(
	mapStateToProps,
	{ getAccountDetails, setSelectedAmount, setPlan }
)(Withdraw);
