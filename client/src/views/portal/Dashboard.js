import React, { Component } from "react";
import { connect } from "react-redux";
import { getAccountDetails } from "modules/accounts/accountDetails";

import WelcomePanel from "./dashboard/WelcomePanel";
import SummaryPanel from "./dashboard/SummaryPanel";
import ConnectPanel from "./dashboard/ConnectPanel";
import QuestionsPanel from "./dashboard/QuestionsPanel";

import "../Portal.css";

class Dashboard extends Component {
	state = {
		loading: false,
		accountDetails: null,
		selectedAccount: null
	};

	componentDidMount() {
		this.setState({ loading: true });
	}

	async componentDidUpdate() {
		if (this.props.selectedAccount && this.state.loading) {
			await this.props.getAccountDetails(this.props.selectedAccount);
			this.setState({ loading: false, accountDetails: this.props.accountDetails });
		}
	}

	render() {
		if (this.state.loading || !this.state.accountDetails) return null;
		return (
			<div className="container">
				<div className="dashboard">
					<div className="widgetsContainer">
						<div className="widgetsMain">
							<WelcomePanel />
							<ConnectPanel />
						</div>
						<div className="widgetsSide">
							<SummaryPanel />
							<QuestionsPanel />
						</div>
						<div className="widgetsExt">
							<ConnectPanel />
							<QuestionsPanel />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ selectedAccount, accountDetails }) {
	return { selectedAccount: selectedAccount, accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	{ getAccountDetails }
)(Dashboard);
