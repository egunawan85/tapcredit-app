import React, { Component } from "react";

import "./Questions.css";

class Questions extends Component {
	render() {
		return (
			<div className="questions">
				<ul className="collapsible">
					<li>
						<div className="collapsible-header">Bagaimana cara kerja pinjaman?</div>
						<div className="collapsible-body">
							<span>
								TapCredit menyederhanakan pembayaran Anda melalui metode pembayaran
								online. Kami akan mengingatkan Anda dan Anda dapat melakukan
								pembayaran di portal TapCredit.
							</span>
						</div>
					</li>
					<li>
						<div className="collapsible-header">Bagaimana perhitungan biaya saya?</div>
						<div className="collapsible-body">
							<span>
								Saat Anda mengirimkan pengajuan, biaya pinjaman berkisar antara 0.5
								– 2.5% dari jumlah pinjaman yang dinilai. Setiap bulannya, Anda
								membayarkan 1/3 total jumlah pinjaman (untuk pinjaman 3 bulan) atau
								1/6 dari jumlah pinjaman (untuk pinjaman 6 bulan) ditambah dengan
								biaya bulanan.
							</span>
						</div>
					</li>
					<li>
						<div className="collapsible-header">
							Bagaimana cara saya mengubah lokasi deposit?
						</div>
						<div className="collapsible-body">
							<span>
								Untuk tujuan keamanan, akun pembayaran yang dihubungkan saat
								pengajuan akan diatur sebagai akun pembayaran utama Anda. Dana akan
								dideposit ke akun ini. Untuk mengubahnya, hubungi kami di
								support@tapcredit.id. Kalau akun pembayaran belum ada, kami akan
								hubungi Anda.
							</span>
						</div>
					</li>
					<li>
						<div className="collapsible-header">Apakah ada penalti prapembayaran?</div>
						<div className="collapsible-body">
							<span>
								TapCredit tidak membebankan penalti pembayaran awal. Anda hanya
								perlu membayar biaya bulanan saat memiliki saldo pinjaman yang belum
								terbayarkan. Jadi, semakin cepat Anda melunasi pinjaman TapCredit,
								semakin banyak uang yang Anda hemat untuk menghindari biaya bulanan
								di masa mendatang.
							</span>
						</div>
					</li>
					<li>
						<div className="collapsible-header">
							Bisakah jumlah kredit maksimal TapCredit saya meningkat atau menurun?
						</div>
						<div className="collapsible-body">
							<span>
								Akun TapCredit dievaluasi secara rutin, sehingga dapat menyebabkan
								perubahan jumlah kredit maksimal dan/atau biaya Anda. Selama proses
								ini, kami mempertimbangkan beragam faktor, termasuk status akun,
								pemanfaatan akun, kinerja bisnis, dan ketentuan saat ini. Pastikan
								Anda memasukkan layanan-layanan dengan transaksi pendapatan bisnis
								yang tinggi agar mendapatkan batas kredit tertinggi.
							</span>
						</div>
					</li>
				</ul>
			</div>
		);
	}
}

export default Questions;
