import React, { Component } from "react";

import "./WelcomePanel.css";
import WelcomeWithdraw from "./welcomePanel/WelcomeWithdraw";

class WelcomePanel extends Component {
	render() {
		return (
			<div className="welcomePanel">
				<WelcomeWithdraw />
			</div>
		);
	}
}

export default WelcomePanel;
