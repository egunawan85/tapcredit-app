import React, { Component } from "react";
import M from "materialize-css/dist/js/materialize.min.js";

import "./ConnectPanel.css";
import bcaLogo from "assets/images/providers/logo-bca.png";
import mandiriLogo from "assets/images/providers/logo-mandiri.png";
import bsLogo from "assets/images/providers/logo-bs.png";

class ConnectPanel extends Component {
	async componentDidMount() {
		this.initCollapsible();
	}

	initCollapsible() {
		const elems = document.querySelectorAll(".collapsible");
		M.Collapsible.init(elems, { accordion: true });
	}

	render() {
		return (
			<div className="connectPanel">
				<h1>Tertarik dengan jumlah kredit yang lebih tinggi?</h1>
				<p>Masukkan layanan lain untuk memberikan kami gambaran yang lebih baik tentang bisnis Anda</p>
				<ul className="collapsible">
					<li>
						<div className="collapsible-header">
							<i className="material-icons">arrow_drop_down</i>Lihat semua layanan bisnis
						</div>
						<div className="collapsible-body">
							<span>
								<div className="providers_container active">
									<div className="providers_list">
										<h1>Bank</h1>
										<ul className="banks">
											<li className="provider">
												<a href="/channel/bca?returnPath=portal&amp;provider=bank">
													<div className="provider_logo">
														<div className="fix" />
														<img alt="service-logo" src={bcaLogo} />
													</div>
												</a>
											</li>
											<li className="provider">
												<a href="/channel/mandiri?returnPath=portal&amp;provider=bank">
													<div className="provider_logo">
														<div className="fix" />
														<img alt="service-logo" src={mandiriLogo} />
													</div>
												</a>
											</li>
										</ul>
										<h1>Jasa Lain</h1>
										<ul className="business_services">
											<li className="provider">
												<a href="/channel/bridestory?returnPath=portal&amp;provider=service">
													<div className="provider_logo">
														<div className="fix" />
														<img alt="service-logo" src={bsLogo} />
													</div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</span>
						</div>
					</li>
				</ul>
			</div>
		);
	}
}

export default ConnectPanel;
