import React, { Component } from "react";
import { connect } from "react-redux";
import { currencyFormat } from "utils";

import "./WelcomeWithdraw.css";
import SelectAmount from "components/SelectAmount";

class WelcomeWithdraw extends Component {
	constructor() {
		super();
		this.state = {
			availableCredit: null,
			selectedAmount: null,
			isLoading: true,
			minimumAmount: 1000000,
			invalidAmount: false,
			currency: null
		};
		this.changeSelectedAmount = this.changeSelectedAmount.bind(this);
		this.refreshInvalidAmount = this.refreshInvalidAmount.bind(this);
	}

	async componentDidMount() {
		await this.setState({
			availableCredit: this.props.accountDetails.availableCredit,
			selectedAmount: this.props.accountDetails.availableCredit,
			currency: this.props.accountDetails.details.currency
		});
		await this.setState({ isLoading: false });
	}

	async changeSelectedAmount(selectedAmount) {
		await this.setState({ selectedAmount: parseInt(selectedAmount) });
	}

	async refreshInvalidAmount() {
		await this.setState({ invalidAmount: false });
	}

	async submitAmount() {
		if (this.state.selectedAmount < this.state.minimumAmount) {
			this.setState({ invalidAmount: true });
		}
		if (this.state.selectedAmount > this.state.availableCredit) {
			this.setState({ invalidAmount: true });
		}
	}

	render() {
		if (this.state.isLoading) return null;
		return (
			<div className="welcomeWithdraw">
				<p className="greeting">Selamat datang kembali, Edward Gunawan!</p>
				<div className="row">
					<div className="col s7">
						<h1 className="howMuch">Berapa yang Anda inginkan hari ini?</h1>
						<SelectAmount
							selectAmount={this.changeSelectedAmount}
							availableCredit={this.state.availableCredit}
							minimumAmount={this.state.minimumAmount}
							invalidAmount={this.state.invalidAmount}
							currency={this.state.currency}
							refresh={this.refreshInvalidAmount}
						/>
					</div>
					<div className="col s5">
						<p className="fundsLeft right-align">
							Dana yang tersedia:{" "}
							{currencyFormat(this.state.currency, this.state.availableCredit)}
						</p>
						<button
							className="btn waves-effect waves-light right-align"
							type="submit"
							name="action"
							onClick={e => this.submitAmount()}
						>
							Submit
							<i className="material-icons right">send</i>
						</button>
					</div>
					<p className="notes">
						*Jumlah kredit maksimum bergantung pada tinjauan dan perubahan berkala,
						termasuk penurunan, kenaikan, dan penghapusan jumlah kredit maksimum.
					</p>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(WelcomeWithdraw);
