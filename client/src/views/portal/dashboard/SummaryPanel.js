import React, { Component } from "react";
import { Doughnut } from "react-chartjs-2";
import M from "materialize-css/dist/js/materialize.min.js";
import { connect } from "react-redux";
import { currencyFormat } from "utils";

import "./SummaryPanel.css";

class SummaryPanel extends Component {
	constructor() {
		super();
		this.state = {
			availableCredit: null,
			balanceOutstanding: null,
			nextPaymentDue: null,
			nextStatementDate: null,
			minimumDue: null,
			isLoading: true,
			currency: null
		};
	}

	async componentDidMount() {
		await this.setState({
			availableCredit: this.props.accountDetails.availableCredit,
			balanceOutstanding: this.props.accountDetails.balanceOutstanding,
			nextPaymentDue: this.props.accountDetails.nextPaymentDue,
			nextStatementDate: this.props.accountDetails.nextStatementDate,
			minimumDue: this.props.accountDetails.minimumDue,
			currency: this.props.accountDetails.details.currency
		});
		await this.setState({ isLoading: false });
	}

	initCollapsible() {
		const elems = document.querySelectorAll(".collapsible");
		M.Collapsible.init(elems, { accordion: true });
	}

	render() {
		if (this.state.isLoading) return null;
		this.initCollapsible();
		return (
			<div className="summaryPanel">
				<ul className="collapsible">
					<li>
						<div className="collapsible-header">
							<i className="material-icons">arrow_drop_down</i>Lihat Detil Akun
						</div>
						<div className="collapsible-body">
							<span>
								<div>
									<div className="summary">
										<h1 className=".center-align">Dana yang digunakan</h1>
										<Doughnut
											data={{
												datasets: [
													{
														data: [
															this.state.balanceOutstanding,
															this.state.availableCredit
														],
														backgroundColor: ["#137396", "#d2d3d4"]
													}
												]
											}}
											options={{
												cutoutPercentage: 75,
												rotation: 0.5 * Math.PI,
												tooltips: { enabled: false },
												hover: { mode: null }
											}}
										/>
										<h1>
											{Math.floor(
												(this.state.balanceOutstanding /
													(this.state.balanceOutstanding +
														this.state.availableCredit)) *
													100
											)}%
										</h1>
										<ul className="collection with-header">
											<li className="collection-item">
												<div>
													Jumlah Pinjaman<span className="secondary-content">
														{currencyFormat(
															this.state.currency,
															this.state.balanceOutstanding
														)}
													</span>
												</div>
											</li>
											<li className="collection-item">
												<div>
													E-Statement Selanjutnya<span className="secondary-content">
														{this.state.nextStatementDate}
													</span>
												</div>
											</li>
											<li className="collection-item">
												<div>
													Jatuh Tempo Selanjutnya<span className="secondary-content">
														{this.state.nextPaymentDue}
													</span>
												</div>
											</li>
											<li className="collection-item">
												<div>
													Pembayaran Minimal<span className="secondary-content">
														{currencyFormat(
															this.state.currency,
															this.state.minimumDue
														)}
													</span>
												</div>
											</li>
											<div className="makePayment">
												<a href="/payments" className="right-align">
													Lakukan pembayaran
												</a>
											</div>
										</ul>
									</div>
								</div>
							</span>
						</div>
					</li>
				</ul>
			</div>
		);
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(SummaryPanel);
