import React, { Component } from "react";

import "./QuestionsPanel.css";
import Questions from "./questionsPanel/Questions";

class QuestionsPanel extends Component {
	render() {
		return (
			<div className="questionsPanel">
				<h1>Pertanyaan Umum</h1>
				<Questions />
			</div>
		);
	}
}

export default QuestionsPanel;


