import React, { Component } from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import { connect } from "react-redux";
import { currencyFormat } from "utils";

import "./WithdrawPanel.css";
import SelectAmount from "components/SelectAmount";

class WithdrawPanel extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: true
		};
	}

	async componentDidMount() {
		await this.setState({ isLoading: false });
	}

	initModal() {
		const elems = document.querySelectorAll(".modal");
		M.Modal.init(elems);
	}

	createRadio() {
		let radio = [];
		this.props.accountDetails.availablePlans.forEach((plan, key) => {
			radio.push(
				<p key={key}>
					<label>
						<input
							key={key}
							value={plan.durationMonths}
							className="with-gap"
							onChange={e => this.props.selectPlan(e)}
							name="group1"
							type="radio"
							defaultChecked={key === 0 ? true : false}
						/>
						<span>Selama {plan.durationMonths} bulan</span>
					</label>
				</p>
			);
		});

		return radio;
	}

	render() {
		if (this.state.isLoading) return null;
		this.initModal();
		return (
			<div className="withdrawPanel">
				<div className="row">
					<div className="col s7">
						<h1>Pilih Pinjaman Anda</h1>
					</div>
					<div className="col s5">
						<a className="accountDetailsLink modal-trigger" href="#accountDetails">
							Rincian Akun
						</a>
					</div>
				</div>
				<div className="row">
					<div className="col s6">
						<SelectAmount
							selectAmount={this.props.selectAmount}
							invalidAmount={this.props.invalidAmount}
							selectPlan={this.props.selectPlan}
							refresh={this.props.refresh}
							minimumAmount={1000000}
						/>
					</div>
					<div className="col s6 selectPlan">{this.createRadio()}</div>
				</div>
				<div id="accountDetails" className="modal bottom-sheet">
					<div className="modal-content">
						<h4>Modal Header</h4>
						<p>A bunch of text</p>
					</div>
					<div className="modal-footer">
						<a href="#!" className="modal-close waves-effect waves-green btn-flat">
							Agree
						</a>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(WithdrawPanel);
