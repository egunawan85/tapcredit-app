import React, { Component } from "react";
import { connect } from "react-redux";
import M from "materialize-css/dist/js/materialize.min.js";

import "./ScheduleWithdraw.css";
import PaymentSchedule from "components/PaymentSchedule";

class ScheduleWithdraw extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: true
		};
	}

	async componentDidMount() {
		await this.setState({ isLoading: false });
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			this.props.plan != nextProps.plan ||
			this.props.selectedAmount != nextProps.selectedAmount
		) {
			return false;
		} else {
			return true;
		}
	}

	initTabs() {
		const elems = document.querySelectorAll(".tabs");
		M.Tabs.init(elems, {});
	}

	render() {
		this.initTabs();
		return (
			<div className="scheduleWithdraw">
				<div className="row toggle">
					<div className="col s6">
						<ul className="tabs">
							<li className="tab col s6">
								<a href={`#allLoans_${this.props.branch}`}>Semua pinjaman</a>
							</li>
							<li className="tab col s6">
								<a href={`#newLoan_${this.props.branch}`}>Pinjaman baru</a>
							</li>
						</ul>
					</div>
					<div id={`allLoans_${this.props.branch}`} className="col s12">
						<PaymentSchedule type={"all"} />
					</div>
					<div id={`newLoan_${this.props.branch}`} className="col s12">
						<PaymentSchedule type={"new"} />
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps({ selectedAmount, plan }) {
	return {
		selectedAmount: selectedAmount,
		plan: plan
	};
}

export default connect(
	mapStateToProps,
	null
)(ScheduleWithdraw);
