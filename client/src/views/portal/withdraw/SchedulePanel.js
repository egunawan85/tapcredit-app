import React, { Component } from "react";
import { connect } from "react-redux";

import "./SchedulePanel.css";
import ScheduleWithdraw from "./schedulePanel/ScheduleWithdraw";

class SchedulePanel extends Component {
	render() {
		return (
			<div className="schedulePanel">
				<h1>Tinjau Jadwal Pembayaran</h1>
				<p>
					Pinjaman akan dibayar selama {this.props.plan.durationMonths} bulan. Tidak ada
					penalti pembayaran awal, sehingga Anda dapat menghemat uang jika membayar
					seluruh pinjaman lebih awal.
				</p>
				<ScheduleWithdraw branch={this.props.branch} />
			</div>
		);
	}
}

function mapStateToProps({ plan }) {
	return {
		plan: plan
	};
}

export default connect(
	mapStateToProps,
	null
)(SchedulePanel);
