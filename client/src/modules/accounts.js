import axios from "axios";
import accountDetailsReducer from "./accounts/accountDetails";

//types
const GET_ACCOUNTS = "get_accounts";
const SELECT_ACCOUNT = "select_account";

//reducers
const allAccountsReducer = (state = null, action) => {
	switch (action.type) {
		case GET_ACCOUNTS:
			return action.payload || false;
		default:
			return state;
	}
};

const selectAccountReducer = (state = null, action) => {
	switch (action.type) {
		case SELECT_ACCOUNT:
			return action.payload || false;
		default:
			return state;
	}
};

const accountsReducer = {
	accounts: allAccountsReducer,
	selectedAccount: selectAccountReducer
};

export default Object.assign({}, accountsReducer, accountDetailsReducer);

//actions
export const getAccounts = () => async dispatch => {
	const res = await axios.get("/api/accounts");
	dispatch({ type: GET_ACCOUNTS, payload: res.data });
	return res;
};

export const selectAccount = accountID => async dispatch => {
	dispatch({ type: SELECT_ACCOUNT, payload: accountID });
};

//enum
export const accountType = {
	CASH_REVOLVING: 0,
	CASH_SINGLE: 1,
	INSTAL_REVOLVING: 1,
	INSTAL_SINGLE: 1
};
