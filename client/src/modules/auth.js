import axios from "axios";

//types
const FETCH_USER = "fetch_user";
const LOGIN_USER = "login_user";
const RESET_PASSWORD = "reset_password";
const AUTHENTICATE_TOKEN = "authenticate_token";
const NEW_PASSWORD = "new_password";

//reducers
const reducer = (state = null, action) => {
	switch (action.type) {
		case FETCH_USER:
			return action.payload || false;
		case AUTHENTICATE_TOKEN:
			return action.payload || false;
		case NEW_PASSWORD:
			return action.payload || false;
		default:
			return state;
	}
};

const loginReducer = (state = null, action) => {
	switch (action.type) {
		case LOGIN_USER:
			return action.payload || false;
		default:
			return state;
	}
};

const resetPasswordReducer = (state = null, action) => {
	switch (action.type) {
		case RESET_PASSWORD:
			return action.payload || false;
		default:
			return state;
	}
};

const authReducer = {
	auth: reducer,
	login: loginReducer,
	reset: resetPasswordReducer
};

export default authReducer;

//actions
export const fetchUser = () => async dispatch => {
	const res = await axios.get("/api/current_user");
	dispatch({ type: FETCH_USER, payload: res.data });
};

export const loginUser = (email, password) => async dispatch => {
	const res = await axios.post("/api/login", {
		username: email,
		password: password
	});
	dispatch({ type: LOGIN_USER, payload: res.data });
};

export const resetPassword = email => async dispatch => {
	const res = await axios.post("/api/resetPassword", {
		email: email
	});
	console.log(res.data);
	dispatch({ type: RESET_PASSWORD, payload: res.data });
};

export const authenticateToken = token => async dispatch => {
	const res = await axios.get(`/api/validToken/${token}`);
	dispatch({ type: AUTHENTICATE_TOKEN, payload: res.data });
};

export const setNewPassword = (token, password) => async dispatch => {
	const res = await axios.post(`/api/setNewPassword/${token}`, {
		password: password
	});
	dispatch({ type: NEW_PASSWORD, payload: res.data });
};
