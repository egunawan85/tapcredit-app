import axios from "axios";
//types
const GET_ACCOUNT_DETAILS = "get_accounts_details";

//reducers
const accountDetailsReducer = (state = null, action) => {
	switch (action.type) {
		case GET_ACCOUNT_DETAILS:
			return action.payload || false;
		default:
			return state;
	}
};

export default {
	accountDetails: accountDetailsReducer
};

//actions
export const getAccountDetails = accountID => async dispatch => {
	const res = await axios.get(`/api/accounts/${accountID}/accountDetails`);
	dispatch({ type: GET_ACCOUNT_DETAILS, payload: res.data });
	return res;
};
