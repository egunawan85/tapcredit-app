import axios from "axios";

//types
const GET_SCHEDULE_ALL = "get_schedule_all";
const GET_SCHEDULE_SIM = "get_schedule_sim";

//reducers
const scheduleAllReducer = (state = null, action) => {
	switch (action.type) {
		case GET_SCHEDULE_ALL:
			return action.payload || false;
		default:
			return state;
	}
};

const scheduleSimReducer = (state = null, action) => {
	switch (action.type) {
		case GET_SCHEDULE_SIM:
			return action.payload || false;
		default:
			return state;
	}
};

const schedulesReducer = {
	scheduleAll: scheduleAllReducer,
	scheduleSim: scheduleSimReducer
};

export default schedulesReducer;

//actions
export const getScheduleAll = accountID => async dispatch => {
	const res = await axios.get(`/api/paymentSchedule/${accountID}/all`);
	dispatch({ type: GET_SCHEDULE_ALL, payload: res.data });
	return res;
};

export const getScheduleSim = (amount, planID) => async dispatch => {
	const res = await axios.get(`/api/paymentSchedule/new/${planID}/${amount}/`);
	dispatch({ type: GET_SCHEDULE_SIM, payload: res.data });
	return res;
};
