import axios from "axios";

//types
const SET_SELECTED_AMOUNT = "SET_SELECTED_AMOUNT";
const SET_PLAN = "SET_PLAN";

//reducers
const withdrawAmountReducer = (state = null, action) => {
	switch (action.type) {
		case SET_SELECTED_AMOUNT:
			return action.payload || false;
		default:
			return state;
	}
};

const withdrawPlanReducer = (state = null, action) => {
	switch (action.type) {
		case SET_PLAN:
			return action.payload || false;
		default:
			return state;
	}
};

const withdrawReducer = {
	selectedAmount: withdrawAmountReducer,
	plan: withdrawPlanReducer
};

export default withdrawReducer;

//actions
export const setSelectedAmount = selectedAmount => async dispatch => {
	dispatch({ type: SET_SELECTED_AMOUNT, payload: selectedAmount });
	return selectedAmount;
};

export const setPlan = plan => async dispatch => {
	dispatch({ type: SET_PLAN, payload: plan });
	return plan;
};
