import axios from "axios";

//types
const FETCH_SNAPTOKEN = "fetch_snaptoken";

//reducers
const reducer = (state = null, action) => {
	switch (action.type) {
		case FETCH_SNAPTOKEN:
			return action.payload || false;
		default:
			return state;
	}
};

const snapReducer = {
	snapReducer: reducer
};

export default snapReducer;

//actions
export const fetchSnapToken = () => async dispatch => {
	const res = await axios.get("/api/snaptoken");
	dispatch({ type: FETCH_SNAPTOKEN, payload: res.data });
};
