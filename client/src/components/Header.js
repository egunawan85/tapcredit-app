import React, { Component } from "react";
import { connect } from "react-redux";
import { verifyAccount } from "utils";
import { getAccounts, selectAccount } from "modules/accounts";
import M from "materialize-css/dist/js/materialize.min.js";
import "./Header.css";

import Menu from "./header/Menu";

import logo from "assets/images/tc_logo1.png";

class Header extends Component {
	async componentDidMount() {
		await this.initAccount();
		await this.initSidenav();
	}

	async initAccount() {
		await this.props.getAccounts();
		if (!localStorage.selectedAccount) {
			this.selectAccount(this.props.accounts[0].accountID);
		} else {
			this.selectAccount(localStorage.selectedAccount);
		}
	}

	async selectAccount(accountID) {
		const verified = await verifyAccount(accountID);
		if (verified) {
			localStorage.setItem("selectedAccount", accountID);
			this.props.selectAccount(accountID);
		} else {
			localStorage.clear();
			this.props.selectAccount(null);
		}
	}

	initSidenav() {
		const elem = document.querySelector(".sidenav");
		M.Sidenav.init(elem);
	}

	render() {
		return (
			<header>
				<nav>
					<div className="nav-wrapper">
						<a href="/" className="brand-logo">
							<img src={logo} className="main-logo" alt="tapcredit" />
						</a>
						<a data-target="mobile-demo" className="sidenav-trigger">
							<i className="material-icons">menu</i>
						</a>
						<ul id="nav-mobile" className="right hide-on-med-and-down">
							<li>
								<a href="/portal/dashboard">Dasbor</a>
							</li>
							<li>
								<a href="/portal/withdraw">Pinjaman</a>
							</li>
							<li>
								<a href="/portal/payments">Pembayaran</a>
							</li>
							<li>
								<a href="/portal/activities">Aktivitas</a>
							</li>
							<li>
								<a href="/api/logout">Keluar</a>
							</li>
						</ul>
					</div>
				</nav>
				<ul className="sidenav" id="mobile-demo">
					<Menu />
				</ul>
			</header>
		);
	}
}

function mapStateToProps({ accounts, selectedAccount }) {
	return { accounts: accounts, selectedAccount: selectedAccount };
}

export default connect(
	mapStateToProps,
	{ getAccounts, selectAccount }
)(Header);
