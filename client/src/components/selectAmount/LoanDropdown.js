import React, { Component } from "react";
import { connect } from "react-redux";
import { currencyFormat } from "utils";
import M from "materialize-css/dist/js/materialize.min.js";

import "./LoanDropdown.css";

class LoanDropdown extends Component {
	async componentDidMount() {
		this.initDropdown();
	}

	initDropdown() {
		var elems = document.querySelectorAll("select");
		M.FormSelect.init(elems, {});
	}

	createDropdown() {
		let select = [];
		let options = [];
		let x = this.props.accountDetails.availableCredit;
		if (x <= 0) {
			options.push(
				<option key="not_available" value="">
					Dana belum tersedia
				</option>
			);
		} else {
			while (x > 0) {
				options.push(
					<option key={x} value={x}>
						{currencyFormat(this.props.accountDetails.currency, x)}
					</option>
				);
				x -= this.props.minimumAmount;
				x = Math.ceil(x / this.props.minimumAmount) * this.props.minimumAmount;
			}
		}
		select.push(
			<select
				key="dropdown"
				className={this.props.className}
				onChange={e => this.handleChange(e)}
			>
				{options}
			</select>
		);
		return select;
	}

	async handleChange(event) {
		await this.props.selectAmount(event.target.value);
	}

	render() {
		return <div>{this.createDropdown()}</div>;
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(LoanDropdown);
