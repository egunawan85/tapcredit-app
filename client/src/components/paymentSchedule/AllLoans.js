import React, { Component } from "react";
import { connect } from "react-redux";
import { getScheduleAll } from "modules/schedules";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";

import "./AllLoans.css";

class AllLoans extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: true
		};
	}

	shouldComponentUpdate() {
		return true;
	}

	async componentDidMount() {
		await this.props.getScheduleAll(this.props.selectedAccount);
		this.setState({ isLoading: false });
	}

	render() {
		if (this.state.isLoading) return null;
		return (
			<div className="allLoans">
				<BootstrapTable data={this.props.scheduleAll.data} striped>
					<TableHeaderColumn dataField="dateDue" isKey>
						Tanggal
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="principalRepayment"
						dataFormat={this.props.handleCurrency}
					>
						Pokok Pinjaman
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="interestDue"
						dataFormat={this.props.handleCurrency}
					>
						Biaya
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="totalPayment"
						dataFormat={this.props.handleCurrency}
					>
						Total Jatuh Tempo
					</TableHeaderColumn>
				</BootstrapTable>
			</div>
		);
	}
}

function mapStateToProps({ scheduleAll, selectedAccount, selectedAmount, plan }) {
	return {
		scheduleAll: scheduleAll,
		selectedAccount: selectedAccount,
		selectedAmount: selectedAmount,
		plan: plan
	};
}

export default connect(
	mapStateToProps,
	{ getScheduleAll }
)(AllLoans);
