import React, { Component } from "react";
import { connect } from "react-redux";
import { getScheduleSim } from "modules/schedules";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
// import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";

import "./NewLoan.css";

class NewLoan extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: true
		};
	}

	shouldComponentUpdate() {
		return true;
	}

	async componentDidMount() {
		await this.props.getScheduleSim(this.props.selectedAmount, this.props.plan.planID);
		this.setState({ isLoading: false });
	}

	async componentDidUpdate(prevProps) {
		if (
			this.props.plan !== prevProps.plan ||
			this.props.selectedAmount !== prevProps.selectedAmount
		) {
			await this.props.getScheduleSim(this.props.selectedAmount, this.props.plan.planID);
		}
	}

	render() {
		if (this.state.isLoading) return null;
		return (
			<div className="newLoans">
				<BootstrapTable data={this.props.scheduleSim.data.payments}>
					<TableHeaderColumn dataField="dateDue" isKey>
						Tanggal
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="principalRepayment"
						dataFormat={this.props.handleCurrency}
					>
						Pokok Pinjaman
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="interestDue"
						dataFormat={this.props.handleCurrency}
					>
						Biaya
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="totalPayment"
						dataFormat={this.props.handleCurrency}
					>
						Total Jatuh Tempo
					</TableHeaderColumn>
				</BootstrapTable>
			</div>
		);
	}
}

function mapStateToProps({ scheduleSim, selectedAmount, plan }) {
	return { scheduleSim: scheduleSim, selectedAmount: selectedAmount, plan: plan };
}

export default connect(
	mapStateToProps,
	{ getScheduleSim }
)(NewLoan);
