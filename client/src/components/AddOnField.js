import React, { Component } from "react";
import { connect } from "react-redux";
import NumberFormat from "react-number-format";
import { currencyFormat } from "utils";

import "./AddOnField.css";

class AddOnField extends Component {
	handleChange(event) {
		this.props.selectAmount(event.value);
		if (this.props.invalidAmount) {
			this.props.refresh();
		}
	}

	render() {
		return (
			<div
				className={
					this.props.className +
					" addOnField" +
					(this.props.invalidAmount ? " invalid" : "")
				}
			>
				<span className="addOn">
					<span className="fa fa-idr" />
				</span>
				<NumberFormat
					className={"addOnInput" + (this.props.invalidAmount ? " invalid" : "")}
					onValueChange={e => this.handleChange(e)}
					thousandSeparator={"."}
					decimalSeparator={","}
				/>
				<span
					className="helper-text right-align"
					data-error={`Masukkan nilai di antara ${currencyFormat(
						this.props.accountDetails.currency,
						this.props.minimumAmount
					)} dan ${currencyFormat(
						this.props.accountDetails.currency,
						this.props.accountDetails.availableCredit
					)}`}
					data-success="right"
				/>
			</div>
		);
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(AddOnField);
