import React, { Component } from "react";
import { connect } from "react-redux";
import { currencyFormat } from "utils";

import "./PaymentSchedule.css";
import AllLoans from "./paymentSchedule/AllLoans";
import NewLoan from "./paymentSchedule/NewLoan";

class paymentSchedule extends Component {
	constructor() {
		super();
		this.handleCurrency = this.handleCurrency.bind(this);
	}
	handleCurrency(cell, row) {
		return currencyFormat(this.props.accountDetails.details.currency, cell);
	}

	getSchedule() {
		if (this.props.type == "all") {
			return <AllLoans handleCurrency={this.handleCurrency} />;
		}
		if (this.props.type == "new") {
			return <NewLoan handleCurrency={this.handleCurrency} />;
		}
	}

	render() {
		return <div className="paymentSchedule">{this.getSchedule()}</div>;
	}
}

function mapStateToProps({ accountDetails }) {
	return {
		accountDetails: accountDetails
	};
}

export default connect(
	mapStateToProps,
	null
)(paymentSchedule);
