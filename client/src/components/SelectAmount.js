import React, { Component } from "react";
import { connect } from "react-redux";

import "./SelectAmount.css";
import AddOnField from "components/AddOnField.js";
import LoanDropdown from "./selectAmount/LoanDropdown.js";

class SelectAmount extends Component {
	constructor() {
		super();
		this.state = { checked: false };
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange() {
		this.setState({
			checked: !this.state.checked
		});
	}

	render() {
		return (
			<div className="selectAmount input-field col s12">
				{this.state.checked ? (
					<AddOnField
						selectAmount={this.props.selectAmount}
						className="exactAmount"
						invalidAmount={this.props.invalidAmount}
						refresh={this.props.refresh}
						minimumAmount={this.props.minimumAmount}
					/>
				) : (
					<LoanDropdown
						selectAmount={this.props.selectAmount}
						minimumAmount={this.props.minimumAmount}
						className="dropdownAmount"
					/>
				)}
				<p>
					<label>
						<input
							type="checkbox"
							className="filled-in"
							checked={this.state.checked}
							onChange={this.handleChange}
						/>
						<span>Masukkan jumlahnya</span>
					</label>
				</p>
			</div>
		);
	}
}

function mapStateToProps({ accountDetails }) {
	return { accountDetails: accountDetails };
}

export default connect(
	mapStateToProps,
	null
)(SelectAmount);
