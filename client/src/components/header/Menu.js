import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import "./Menu.css";

class Menu extends Component {
	componentDidMount() {}

	render() {
		return (
			<div className="collection mobileMenu">
				<NavLink className="collection-item" to="/dashboard" activeClassName="active">
					Dasbor
				</NavLink>
				<NavLink className="collection-item" to="/withdraw" activeClassName="active">
					Pinjaman
				</NavLink>
				<NavLink className="collection-item" to="/payments" activeClassName="active">
					Pembayaran
				</NavLink>
				<NavLink className="collection-item" to="/activities" activeClassName="active">
					Aktivitas
				</NavLink>
				<NavLink className="collection-item" to="/logout" activeClassName="active">
					Keluar
				</NavLink>
			</div>
		);
	}
}

export default Menu;
