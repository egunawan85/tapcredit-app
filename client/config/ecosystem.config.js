module.exports = {
  apps: [
    {
      name: "TapCredit.Client",
      script: "node_modules/react-scripts/scripts/start.js",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
