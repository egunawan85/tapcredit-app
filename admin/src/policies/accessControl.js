//can set interest for simulation

const ac = {
  agent: {
    static: ["register/customer:visit", "applications:read", "applications:update", "applications:delete-before-submit"]
  },
  accountOfficer: {
    static: [
      "register/customer:visit",
      "register/agent:visit",
      "applications:read",
      "applications:update",
      "applications:delete",
      "simulation:update"
    ]
  },
  admin: {
    static: [
      "register/customer:visit",
      "register/agent:visit",
      "register/team:visit",
      "applications:read",
      "applications:update",
      "applications:delete",
      "applications:approve",
      "simulation:update"
    ]
  },
  superAdmin: {
    static: [
      "register/customer:visit",
      "register/agent:visit",
      "register/team:visit",
      "applications:read",
      "applications:update",
      "applications:delete",
      "applications:approve",
      "simulation:update"
    ]
  }
};

export default ac;
