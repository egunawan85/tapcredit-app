//Simulation page/container
import React, { Component } from "react";
// react component that creates a form divided into multiple steps
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import NumberFormat from "react-number-format";
import Select from "react-select";
import Can from "components/Can/Can.jsx";
import { connect } from "react-redux";

import { fetchUser } from "modules/auth";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SimulationTable from "components/Tables/SimulationTable.js";

import "./simulation.css";

class Simulation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			amount: null,
			interest: { value: 0.1, label: "10%" },
			editInterest: false,
			duration: { value: 12, label: "12 bulan" },
			editDuration: false,
			loading: true
		};
		this.editInterest = this.editInterest.bind(this);
		this.editDuration = this.editDuration.bind(this);
	}

	async componentDidMount() {
		this.props.fetchUser();
		this.setState({ loading: false });
		console.log(this.props);
	}

	interestOptions() {
		const interests = [];
		let i, interest;
		for (i = 0; i < 21; i++) {
			interest = { value: i / 100, label: `${i}%` };
			interests.push(interest);
		}
		return interests;
	}

	editInterest() {
		this.setState({ editInterest: true });
	}

	editDuration() {
		this.setState({ editDuration: true });
	}

	render() {
		if (this.state.loading) return null;
		return (
			<div className="main-content simulation">
				<Grid fluid>
					<Row>
						<Col md={10} mdOffset={1}>
							<Card
								wizard
								id="simulation"
								textCenter
								title="Simulasi"
								category="Untuk Cicilan Sewa"
								content={
									<div>
										<h5 className="text-center">
											Masukkan jumlah sewa untuk lihat jadwal pembayaran
										</h5>
										<Row>
											<Col md={10} mdOffset={1}>
												<FormGroup>
													<ControlLabel>Jumlah Sewa</ControlLabel>
													<NumberFormat
														type="text"
														name="amount"
														placeholder="ex: 50000000"
														value={this.state.amount}
														className="form-control amount"
														thousandSeparator={"."}
														decimalSeparator={","}
														prefix="Rp. "
														onValueChange={values =>
															this.setState({ amount: values.value })
														}
														isAllowed={values => {
															const { formattedValue, floatValue } = values;
															return floatValue <= 500000000 || formattedValue === "";
														}}
													/>
												</FormGroup>
											</Col>
										</Row>
										<Row>
											<Col xs={6} md={5} mdOffset={1}>
												<FormGroup>
													<ControlLabel>Biaya Bunga</ControlLabel>
													{this.state.editInterest ? (
														<Select
															name="interest"
															value={this.state.interest}
															className="interest"
															options={this.interestOptions()}
															onChange={value => this.setState({ interest: value })}
														/>
													) : (
														<div className="interestWrapper">
															<p>{`${this.state.interest.value * 100}%`}</p>
															<Can
																role={this.props.auth.mainRole}
																perform="simulation:update"
																yes={() => (
																	<i
																		className="fa fa-edit"
																		onClick={this.editInterest}
																	/>
																)}
															/>
														</div>
													)}
												</FormGroup>
											</Col>
											<Col xs={6} md={5}>
												<FormGroup>
													<ControlLabel>Periode</ControlLabel>
													{this.state.editDuration ? (
														<Select
															name="duration"
															value={this.state.duration}
															className="duration"
															options={[
																{ value: "12", label: "12 bulan" },
																{ value: "6", label: "6 bulan" }
															]}
															onChange={value => this.setState({ duration: value })}
														/>
													) : (
														<div className="durationWrapper">
															<p>{`${this.state.duration.value} bulan`}</p>
															<Can
																role={this.props.auth.mainRole}
																perform="simulation:update"
																yes={() => (
																	<i
																		className="fa fa-edit"
																		onClick={this.editDuration}
																	/>
																)}
															/>
														</div>
													)}
												</FormGroup>
											</Col>
										</Row>
										{this.state.amount ? (
											<Row>
												<Col md={10} mdOffset={1}>
													<SimulationTable
														amount={this.state.amount}
														duration={this.state.duration.value}
														interest={this.state.interest.value}
													/>
												</Col>
											</Row>
										) : null}
									</div>
								}
							/>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

function mapStateToProps({ auth }) {
	return { auth: auth };
}

export default connect(
	mapStateToProps,
	{ fetchUser }
)(Simulation);
