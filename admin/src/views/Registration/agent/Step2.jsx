import React, { Component } from "react";
// react component that creates a form divided into multiple steps
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import NumberFormat from "react-number-format";
import Select from "react-select";

import Card from "components/Card/Card.jsx";
import { provinceOptions } from "variables/Variables.jsx";

class Step2 extends Component {
  constructor(props) {
    super(props);
    const application = this.props.application;
    this.state = {
      applicationID: application.applicationID || null,
      ktp: application.ktp || "",
      ktpError: null,
      dateOfBirth: application.dateOfBirth || "",
      dateOfBirthError: null,
      address: application.address || "",
      addressError: null,
      city: application.city || "",
      cityError: null,
      province: application.province || "",
      provinceError: null,
      postalCode: application.postalCode || "",
      postalCodeError: null,
      viewOnly: application.status ? true : false,
      loading: true
    };
  }

  async componentDidMount() {
    if (!this.props.application.applicationID) {
      window.location.reload();
    }
    await this.setState({ loading: false });
  }

  async isValidated() {
    if (!this.state.viewOnly) {
      this.state.ktp == false || this.state.ktp.toString().length < 16
        ? await this.setState({
            ktpError: <small className="text-danger">Nomor KTP harus di isi.</small>
          })
        : await this.setState({ ktpError: null });
      this.state.dateOfBirth == false || this.state.dateOfBirth.toString().length < 8
        ? await this.setState({
            dateOfBirthError: <small className="text-danger">Tanggal lahir harus di isi.</small>
          })
        : await this.setState({ dateOfBirthError: null });
      this.state.address == false
        ? await this.setState({
            addressError: <small className="text-danger">Alamat harus di isi.</small>
          })
        : await this.setState({ addressError: null });
      this.state.city == false
        ? await this.setState({
            cityError: <small className="text-danger">Kota harus di isi.</small>
          })
        : await this.setState({ cityError: null });
      this.state.province == false
        ? await this.setState({
            provinceError: <small className="text-danger">Propinsi harus di isi.</small>
          })
        : await this.setState({ provinceError: null });
      this.state.postalCode == false || this.state.postalCode.toString().length < 5
        ? await this.setState({
            postalCodeError: <small className="text-danger">Kode pos harus di isi.</small>
          })
        : await this.setState({ postalCodeError: null });
      if (
        this.state.ktpError ||
        this.state.dateOfBirthError ||
        this.state.addressError ||
        this.state.cityError ||
        this.state.provinceError ||
        this.state.postalCodeError
      ) {
        return false;
      } else {
        return await this.submit();
      }
    }
    return true;
  }

  async submit() {
    return await this.props.submitApplication(
      {
        applicationID: this.props.application.applicationID,
        ownerUserID: this.props.application.ownerUserID,
        applicantUserID: this.props.application.applicantUserID,
        applicationFormID: 2,
        step: 2,
        status: "pending"
      },
      [
        {
          usermeta: true,
          fieldKey: "ktp",
          fieldValue: this.state.ktp
        },
        {
          usermeta: true,
          fieldKey: "dateOfBirth",
          fieldValue: this.state.dateOfBirth
        },
        {
          usermeta: true,
          fieldKey: "address",
          fieldValue: this.state.address
        },
        {
          usermeta: true,
          fieldKey: "city",
          fieldValue: this.state.city
        },
        {
          usermeta: true,
          fieldKey: "province",
          fieldValue: this.state.province
        },
        {
          usermeta: true,
          fieldKey: "postalCode",
          fieldValue: this.state.postalCode
        }
      ]
    );
  }

  render() {
    if (this.state.loading) return null;
    return (
      <div className="wizard-step step-2">
        <h5 className="text-center">Ceritakan sedikit mengenai agen Anda</h5>
        <div>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Nomor KTP <span className="text-danger">*</span>
                </ControlLabel>
                <NumberFormat
                  type="text"
                  name="ktp"
                  placeholder="ex: 130130151723"
                  value={this.state.ktp}
                  disabled={this.state.viewOnly}
                  format="################"
                  mask="_"
                  className={`form-control ${this.state.ktpError && "error"}`}
                  onValueChange={values => this.setState({ ktp: values.value })}
                />
                {this.state.ktpError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Tanggal lahir <span className="text-danger">*</span>
                </ControlLabel>
                <NumberFormat
                  type="text"
                  name="date_of_birth"
                  placeholder="ex: 20/10/1985"
                  disabled={this.state.viewOnly}
                  value={this.state.dateOfBirth}
                  format="##/##/####"
                  mask="_"
                  className={`form-control ${this.state.dateOfBirthError && "error"}`}
                  onValueChange={values => this.setState({ dateOfBirth: values.formattedValue })}
                />
                {this.state.dateOfBirthError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Alamat <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="address"
                  name="address"
                  placeholder="ex: Jalan Pondok Indah"
                  disabled={this.state.viewOnly}
                  value={this.state.address}
                  className={this.state.addressError && "error"}
                  onChange={event => this.setState({ address: event.target.value })}
                />
                {this.state.addressError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={5} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Kota <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="city"
                  name="city"
                  placeholder="ex: Jakarta"
                  disabled={this.state.viewOnly}
                  value={this.state.city}
                  className={this.state.cityError && "error"}
                  onChange={event => this.setState({ city: event.target.value })}
                />
                {this.state.cityError}
              </FormGroup>
            </Col>
            <Col md={5}>
              <FormGroup>
                <ControlLabel>
                  Propinsi <span className="text-danger">*</span>
                </ControlLabel>
                <Select
                  placeholder="Pilih satu"
                  name="province"
                  value={this.state.province}
                  disabled={this.state.viewOnly}
                  className={this.state.provinceError && "error"}
                  options={provinceOptions}
                  error
                  onChange={value => this.setState({ province: value ? value.value : null })}
                />
                {this.state.provinceError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Kode Pos <span className="text-danger">*</span>
                </ControlLabel>
                <NumberFormat
                  type="text"
                  name="postal_code"
                  placeholder="ex: 12190"
                  disabled={this.state.viewOnly}
                  value={this.state.postalCode}
                  format="#####"
                  mask="_"
                  className={`form-control ${this.state.postalCodeError && "error"}`}
                  onValueChange={values => this.setState({ postalCode: values.value })}
                />
                {this.state.postalCodeError}
              </FormGroup>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Step2;
