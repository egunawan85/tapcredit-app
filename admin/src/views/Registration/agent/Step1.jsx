import React, { Component } from "react";
// react component that creates a form divided into multiple steps
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import generator from "generate-password";

import Card from "components/Card/Card.jsx";

class Step1 extends Component {
  constructor(props) {
    super(props);
    const application = this.props.application;
    this.state = {
      applicationID: application.applicationID || null,
      email: application.email || "",
      emailError: null,
      firstName: application.firstName || "",
      firstNameError: null,
      lastName: application.lastName || "",
      lastNameError: null,
      mobileNumber: application.mobileNumber || "",
      mobileError: null,
      role: "agent",
      success: false,
      locked: false,
      loading: true
    };
  }

  async componentDidMount() {
    if (this.state.applicationID) await this.setState({ locked: true });
    await this.props.fetchUser();
    await this.setState({ loading: false });
  }

  async isValidated() {
    if (!this.state.locked) {
      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      re.test(this.state.email) === false
        ? await this.setState({
            emailError: (
              <small className="text-danger">
                Email harus di isi dengan format <i>john@doe.com</i>.
              </small>
            )
          })
        : await this.setState({ emailError: null });
      this.state.firstName == false
        ? await this.setState({
            firstNameError: <small className="text-danger">Nama depan harus di isi.</small>
          })
        : await this.setState({ firstNameError: null });
      this.state.lastName == false
        ? await this.setState({
            lastNameError: <small className="text-danger">Nama keluarga harus di isi.</small>
          })
        : await this.setState({ lastNameError: null });
      this.state.mobileNumber == false
        ? await this.setState({
            mobileError: <small className="text-danger">Nomor telpon pribadi harus di isi.</small>
          })
        : await this.setState({ mobileError: null });
      const submit =
        this.state.emailError || this.state.firstNameError || this.state.lastNameError || this.state.mobileError
          ? false
          : this.submit();
      return submit;
    }
    return true;
  }

  async submit() {
    const register = await this.register();
    if (register) {
      return await this.submitApplication();
    } else {
      return false;
    }
  }

  async register() {
    const { email, firstName, lastName, mobileNumber, role } = this.state;
    const password = generator.generate({
      length: 10,
      numbers: true
    });

    await this.props.registerUser(
      email,
      password,
      role,
      {
        firstName: firstName,
        lastName: lastName,
        mobileNumber: mobileNumber
      },
      false
    );
    if (this.props.register.userID) {
      return true;
    } else {
      await this.setState({ emailError: <small className="text-danger">{this.props.register.message}</small> });
      return false;
    }
  }

  async submitApplication() {
    await this.props.submitApplication(
      {
        applicationID: null,
        ownerUserID: this.props.auth.userID,
        applicantUserID: this.props.register.userID,
        applicationFormID: 2,
        step: 1,
        status: "pending"
      },
      [
        {
          usermeta: false,
          fieldKey: "firstName",
          fieldValue: this.state.firstName
        },

        {
          usermeta: false,
          fieldKey: "lastName",
          fieldValue: this.state.lastName
        },
        {
          usermeta: false,
          fieldKey: "email",
          fieldValue: this.state.email
        },
        {
          usermeta: false,
          fieldKey: "mobileNumber",
          fieldValue: this.state.mobileNumber
        }
      ]
    );
  }

  render() {
    if (this.state.loading) return null;
    console.log(this.state);
    return (
      <div className="wizard-step step-1">
        <h5 className="text-center">Buatkan akun untuk agen Anda</h5>
        <div>
          <Row>
            <Col md={5} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Nama Depan <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="text"
                  name="first_name"
                  placeholder="ex: Nikola"
                  disabled={this.state.locked}
                  value={this.state.firstName}
                  className={this.state.firstNameError && "error"}
                  onChange={event => this.setState({ firstName: event.target.value })}
                />
                {this.state.firstNameError}
              </FormGroup>
            </Col>
            <Col md={5}>
              <FormGroup>
                <ControlLabel>
                  Nama Keluarga <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="text"
                  name="last_name"
                  placeholder="ex: Tesla"
                  disabled={this.state.locked}
                  value={this.state.lastName}
                  className={this.state.lastNameError && "error"}
                  onChange={event => this.setState({ lastName: event.target.value })}
                />
                {this.state.lastNameError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Email <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="email"
                  name="email"
                  placeholder="ex: hello@tapcredit.id"
                  disabled={this.state.locked}
                  value={this.state.email}
                  className={this.state.emailError && "error"}
                  onChange={event => this.setState({ email: event.target.value })}
                />
                {this.state.emailError}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10} mdOffset={1}>
              <FormGroup>
                <ControlLabel>
                  Nomor Telpon <span className="text-danger">*</span>
                </ControlLabel>
                <FormControl
                  type="mobile"
                  name="mobile"
                  placeholder="ex: +62811888879"
                  disabled={this.state.locked}
                  value={this.state.mobileNumber}
                  className={this.state.mobileError && "error"}
                  onChange={event => this.setState({ mobileNumber: event.target.value })}
                />
                {this.state.mobileError}
              </FormGroup>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Step1;
