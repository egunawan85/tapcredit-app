import React, { Component } from "react";
// react component that creates a form divided into multiple steps
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import Button from "components/CustomButton/CustomButton.jsx";
import NumberFormat from "react-number-format";
import Select from "react-select";

import Card from "components/Card/Card.jsx";

class Step4 extends Component {
  constructor(props) {
    super(props);
    const application = this.props.application;
    this.state = {
      applicationID: application.applicationID || null,
      income: application.income || "",
      incomeError: null,
      question1: application.question1 || "",
      question1Error: null,
      question2: application.question2 || "",
      question2Error: null,
      question3: application.question3 || "",
      question3Error: null,
      loading: true,
      success: false,
      successError: null,
      viewOnly: application.status ? true : false
    };
  }

  async componentDidMount() {
    if (!this.props.application.applicationID) {
      window.location.reload();
    }
    await this.setState({ loading: false });
  }

  redirect() {
    return this.props.history.push("/dashboard");
  }

  async isValidated() {
    if (!this.state.viewOnly) {
      this.state.question1 == false
        ? await this.setState({
            question1Error: <small className="text-danger">Pertanyaan ini harus di jawab.</small>
          })
        : await this.setState({ question1Error: null });
      this.state.question2 == false
        ? await this.setState({
            question2Error: <small className="text-danger">Pertanyaan ini harus di jawab.</small>
          })
        : await this.setState({ question2Error: null });
      this.state.question3 == false
        ? await this.setState({
            question3Error: <small className="text-danger">Pertanyaan ini harus di jawab.</small>
          })
        : await this.setState({ question3Error: null });
      if (this.state.question1Error || this.state.question2Error || this.state.question3Error) {
        return false;
      } else {
        await this.submit();
        await this.props.setFirstPassword(this.props.application.email);
        this.props.application.error
          ? await this.setState({ success: false, successError: true })
          : await this.setState({ success: true, successError: null });
      }
    }
    return true;
  }

  async submit() {
    return await this.props.submitApplication(
      {
        applicationID: this.props.application.applicationID,
        ownerUserID: this.props.application.ownerUserID,
        applicantUserID: this.props.application.applicantUserID,
        applicationFormID: 3,
        step: 2,
        status: "completed"
      },
      [
        {
          usermeta: false,
          fieldKey: "income",
          fieldValue: this.state.income
        },
        {
          usermeta: false,
          fieldKey: "question1",
          fieldValue: this.state.question1
        },
        {
          usermeta: false,
          fieldKey: "question2",
          fieldValue: this.state.question2
        },
        {
          usermeta: false,
          fieldKey: "question3",
          fieldValue: this.state.question3
        }
      ]
    );
  }

  render() {
    if (this.state.loading) return null;
    return (
      <div className={`wizard-step step-3 ${this.state.success && "success"}`}>
        {!this.state.success ? (
          <h5 className="text-center">Hampir selesai! Sedikit informasi tambahan</h5>
        ) : (
          <div className="check-icon text-center">
            <i className="pe-7s-check" />
          </div>
        )}
        {this.state.success ? (
          <div>
            <h2 className="successMessage text-center text-space">
              Selesai!<p>
                <small>Penyewa akan segera dapat email</small>
              </p>
            </h2>
            <Button
              fill
              round
              className="btn btn-prev btn-info btn-fill btn-round pull-right btn-wd next"
              id="back-button"
              onClick={e => this.redirect()}
            >
              Kembali
            </Button>
          </div>
        ) : (
          <div>
            <div>
              <Row>
                <Col md={10} mdOffset={1}>
                  <FormGroup>
                    <ControlLabel>
                      Penghasilan per bulan <span className="text-danger">*</span>
                    </ControlLabel>
                    <NumberFormat
                      type="text"
                      name="income"
                      placeholder="ex: 50000000"
                      disabled={this.state.viewOnly}
                      value={this.state.income}
                      thousandSeparator={"."}
                      decimalSeparator={","}
                      prefix="Rp. "
                      className={`form-control ${this.state.incomeError && "error"}`}
                      onValueChange={values => this.setState({ income: values.value })}
                    />
                    {this.state.incomeError}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md={10} mdOffset={1}>
                  <FormGroup>
                    <ControlLabel>
                      Apa Anda siap mengirim bukti penghasilan? <span className="text-danger">*</span>
                    </ControlLabel>
                    <Select
                      name="question1"
                      value={this.state.question1}
                      className={this.state.question1Error && "error"}
                      disabled={this.state.viewOnly}
                      options={[
                        { value: "iya", label: "Iya" },
                        { value: "tidak", label: "Tidak (kemungkinan disetujui turun)" }
                      ]}
                      onChange={value => this.setState({ question1: value ? value.value : null })}
                    />
                    {this.state.question1Error}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md={10} mdOffset={1}>
                  <FormGroup>
                    <ControlLabel>
                      Apa Anda siap mengirim akun Facebook? <span className="text-danger">*</span>
                    </ControlLabel>
                    <Select
                      name="question2"
                      disabled={this.state.viewOnly}
                      value={this.state.question2}
                      className={this.state.question2Error && "error"}
                      options={[
                        { value: "iya", label: "Iya" },
                        { value: "tidak", label: "Tidak (kemungkinan disetujui turun)" }
                      ]}
                      onChange={value => this.setState({ question2: value ? value.value : null })}
                    />
                    {this.state.question2Error}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md={10} mdOffset={1}>
                  <FormGroup>
                    <ControlLabel>
                      Apa Anda siap mengirim akun Instagram? <span className="text-danger">*</span>
                    </ControlLabel>
                    <Select
                      name="question3"
                      disabled={this.state.viewOnly}
                      value={this.state.question3}
                      className={this.state.question3Error && "error"}
                      options={[
                        { value: "iya", label: "Iya" },
                        { value: "tidak", label: "Tidak (kemungkinan disetujui turun)" }
                      ]}
                      onChange={value => this.setState({ question3: value ? value.value : null })}
                    />
                    {this.state.question3Error}
                  </FormGroup>
                </Col>
              </Row>
            </div>
            <div className="wizard-finish-button">
              <Button
                bsStyle="info"
                fill
                wd
                onClick={event => (this.state.viewOnly ? this.redirect() : this.isValidated())}
                pullRight
              >
                {this.state.viewOnly ? "Ke Dasbor" : "Selesai"}
              </Button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Step4;
