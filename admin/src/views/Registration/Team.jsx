import React, { Component } from "react";
// react component that creates a form divided into multiple steps
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { connect } from "react-redux";
import Select from "react-select";
import { registerUser } from "modules/auth";
import generator from "generate-password";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import "./team.css";

class Team extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      emailError: null,
      firstName: "",
      firstNameError: null,
      lastName: "",
      lastNameError: null,
      role: "",
      roleError: null,
      success: false
    };
  }
  async isValidated() {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    re.test(this.state.email) === false
      ? await this.setState({
          emailError: (
            <small className="text-danger">
              Email harus di isi dengan format <i>john@doe.com</i>.
            </small>
          )
        })
      : await this.setState({ emailError: null });
    this.state.firstName == false
      ? await this.setState({
          firstNameError: <small className="text-danger">Nama depan harus di isi.</small>
        })
      : await this.setState({ firstNameError: null });
    this.state.lastName == false
      ? await this.setState({
          lastNameError: <small className="text-danger">Nama keluarga harus di isi.</small>
        })
      : await this.setState({ lastNameError: null });
    this.state.role == false
      ? await this.setState({
          roleError: <small className="text-danger">Pilih salah satu tipe akun.</small>
        })
      : await this.setState({ roleError: null });
    return this.state.emailError || this.state.firstNameError || this.state.lastNameError || this.state.roleError
      ? false
      : true;
  }

  async submit() {
    if (await this.isValidated()) {
      const { email, firstName, lastName, role } = this.state;
      const password = generator.generate({
        length: 10,
        numbers: true
      });
      const result = await this.props.registerUser(
        email,
        password,
        role.value,
        {
          firstName: firstName,
          lastName: lastName
        },
        true
      );
      await this.setState({ success: true });
    } else {
      await this.setState({ success: false });
    }
  }

  redirect() {
    return this.props.history.push("/dashboard");
  }

  render() {
    return (
      <div className="main-content registerTeam">
        <Grid fluid>
          <Row>
            <Col md={8} mdOffset={2}>
              <Card
                wizard
                id="wizardCard"
                textCenter
                title="Pendaftaran"
                category="Anggota Tim TapCredit"
                content={
                  <div className="wizard-step">
                    {!this.state.success ? (
                      <h5 className="text-center">Ceritakan sedikit mengenai anggota tim baru</h5>
                    ) : (
                      <h5 className="text-center">Registrasi berhasil -- Silakan kembali ke dasbor.</h5>
                    )}
                    {this.state.success ? (
                      <div className="check-icon text-center">
                        <i className="pe-7s-check" />
                      </div>
                    ) : (
                      <div>
                        <Row>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Nama Depan <span className="text-danger">*</span>
                              </ControlLabel>
                              <FormControl
                                type="text"
                                name="first_name"
                                placeholder="ex: Nikola"
                                className={this.state.firstNameError && "error"}
                                onChange={event => this.setState({ firstName: event.target.value })}
                              />
                              {this.state.firstNameError}
                            </FormGroup>
                          </Col>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Nama Keluarga <span className="text-danger">*</span>
                              </ControlLabel>
                              <FormControl
                                type="text"
                                name="last_name"
                                placeholder="ex: Tesla"
                                className={this.state.lastNameError && "error"}
                                onChange={event => this.setState({ lastName: event.target.value })}
                              />
                              {this.state.lastNameError}
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={10} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Email <span className="text-danger">*</span>
                              </ControlLabel>
                              <FormControl
                                type="email"
                                name="email"
                                placeholder="ex: hello@tapcredit.id"
                                className={this.state.emailError && "error"}
                                onChange={event => this.setState({ email: event.target.value })}
                              />
                              {this.state.emailError}
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={10} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Tipe Akun <span className="text-danger">*</span>
                              </ControlLabel>
                              <Select
                                placeholder="Pilih satu"
                                name="Tipe Akun"
                                value={this.state.role}
                                className={this.state.roleError && "error"}
                                options={[
                                  { value: "admin", label: "Admin" },
                                  { value: "accountOfficer", label: "Account Officer" }
                                ]}
                                onChange={value => this.setState({ role: value ? value : null })}
                              />
                              {this.state.roleError}
                            </FormGroup>
                          </Col>
                        </Row>
                      </div>
                    )}
                    <Row>
                      <Col md={10} mdOffset={1}>
                        {!this.state.success ? (
                          <Button
                            fill
                            round
                            className="btn btn-prev btn-info btn-fill btn-round pull-right btn-wd next"
                            id="next-button"
                            onClick={e => this.submit()}
                          >
                            Daftar
                          </Button>
                        ) : (
                          <Button
                            fill
                            round
                            className="btn btn-prev btn-info btn-fill btn-round pull-right btn-wd back"
                            id="next-button"
                            onClick={e => this.redirect()}
                          >
                            Kembali
                          </Button>
                        )}
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default connect(
  null,
  { registerUser }
)(Team);
