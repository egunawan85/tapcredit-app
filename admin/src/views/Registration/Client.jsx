import React, { Component } from "react";
import StepZilla from "react-stepzilla";
import { Grid, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import { registerUser, fetchUser, setFirstPassword, fetchByEmail } from "modules/auth";
import { submitApplication, fetchApplication } from "modules/applications";

import Step1 from "./client/Step1.jsx";
import Step2 from "./client/Step2.jsx";
import Step3 from "./client/Step3.jsx";
import Step4 from "./client/Step4.jsx";

import "./client.css";

class Client extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			applicationID: this.props.match.params.applicationID ? this.props.match.params.applicationID : 0,
			applicationFormID: 3
		};
	}

	async componentDidMount() {
		await this.fetchApplication();
		await this.setState({ loading: false });
	}

	async fetchApplication() {
		await this.props.fetchApplication(this.state.applicationID);
		if (this.props.application.applicationFormID != this.state.applicationFormID) {
			return this.props.history.push("/register/customer");
		}
	}

	render() {
		if (this.state.loading) return null;
		const steps = [
			{ name: "Buat Akun", component: <Step1 {...this.props} /> },
			{ name: "Info Properti", component: <Step2 {...this.props} /> },
			{ name: "Info Penyewa", component: <Step3 {...this.props} /> },
			{ name: "Tambahan", component: <Step4 {...this.props} /> }
		];
		console.log(this.props);
		return (
			<div className="main-content register-client">
				<Grid fluid>
					<Row>
						<Col md={8} mdOffset={2}>
							<Card
								wizard
								id="wizardCard"
								textCenter
								title="Pengajuan"
								category="Cicilan Sewa untuk Pelanggan"
								content={
									<StepZilla
										steps={steps}
										stepsNavigation={false}
										startAtStep={
											!this.props.application.status && !this.props.application.approval
												? this.props.application.step
												: 0
										}
										nextButtonCls="btn btn-prev btn-info btn-fill pull-right btn-wd"
										backButtonCls="btn btn-next btn-default btn-fill pull-left btn-wd"
										nextButtonText="Lanjut"
										backButtonText="Kembali"
									/>
								}
							/>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

function mapStateToProps({ auth, register, application, reset }) {
	return { auth: auth, register: register, application: application, reset: reset };
}

export default connect(
	mapStateToProps,
	{ registerUser, submitApplication, fetchUser, fetchApplication, setFirstPassword, fetchByEmail }
)(Client);
