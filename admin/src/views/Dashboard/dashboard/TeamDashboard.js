//stats Calculated within component from this.props.applicationList
import React, { Component } from "react";
import { Grid, Col, Row } from "react-bootstrap";
// react component used to create charts
import ChartistGraph from "react-chartist";
// react components used to create a SVG / Vector map
import { VectorMap } from "react-jvectormap";
import { connect } from "react-redux";
import moment from "moment-timezone";

import { fetchAllUsers } from "modules/users";
import { currencyFormat } from "utils";

import Card from "components/Card/Card.jsx";
import StatsCard from "components/Card/StatsCard.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import ApplicationList from "components/Tables/ApplicationList.jsx";
import FloatingButton from "components/CustomButton/FloatingButton";

import "./teamDashboard.css";

class TeamDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      applicationsApplied: this.applicationsApplied().length,
      appliedList: this.applicationsApplied(),
      applicationsApproved: this.applicationsApproved().length,
      approvedList: this.applicationsApproved(),
      agentsRegistered: null,
      agentsList: null,
      loading: true
    };
    this.testFunction = this.testFunction.bind(this);
  }

  async componentDidMount() {
    await this.props.fetchAllUsers();
    await this.setState({
      loading: false,
      agentsRegistered: this.agentsRegistered().length,
      agentsList: this.agentsRegistered()
    });
  }

  testFunction() {
    console.log("it works!");
  }

  agentsRegistered() {
    const allUsers = this.props.users;
    const allAgents = allUsers.filter(user => user.mainRole == "client");
    return allAgents;
  }

  applicationsApplied() {
    const applicationList = this.props.applicationList;
    const pendingList = applicationList.filter(application => application.status == 1);
    return pendingList;
  }

  applicationsApproved() {
    const applicationList = this.props.applicationList;
    const approvedList = applicationList.filter(application => application.approval == 2);
    return approvedList;
  }

  redirect() {
    return this.props.history.push("/register/agent");
  }

  render() {
    if (this.state.loading) return null;
    return (
      <div className="main-content teamDashboard">
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-users text-warning" />}
                statsText="Agen Terdaftar"
                statsValue={this.state.agentsRegistered}
                statsIcon={this.state.agentsRegistered > 0 ? <i className="fa fa-refresh" /> : ""}
                statsIconText={
                  this.state.agentsRegistered > 0 ? (
                    moment(this.state.agentsList[0].updatedAt).format("DD/MM/YYYY")
                  ) : (
                    <Button bsSize="xs" simple wd onClick={e => this.redirect()}>
                      Mulai Sekarang!
                    </Button>
                  )
                }
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-note2 text-warning" />}
                statsText="Cicilan Diajukan"
                statsValue={this.state.applicationsApplied}
                statsIcon={<i className="fa fa-calendar-o" />}
                statsIconText={
                  this.state.applicationsApplied > 0
                    ? moment(this.state.appliedList[0].updatedAt).format("DD/MM/YYYY")
                    : "--"
                }
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-home text-success" />}
                statsText="Cicilan Disetujui"
                statsValue={this.state.applicationsApproved}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText={
                  this.state.applicationsApproved > 0
                    ? moment(this.state.approvedList[0].updatedAt).format("DD/MM/YYYY")
                    : "--"
                }
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-cash text-danger" />}
                statsText="Cicilan Dibayar"
                statsValue={currencyFormat(0, 0)}
                statsIcon={<i className="fa fa-clock-o" />}
                statsIconText="--"
              />
            </Col>
          </Row>
        </Grid>
        <ApplicationList testFunction={this.testFunction} />
      </div>
    );
  }
}

function mapStateToProps({ users }) {
  return { users: users };
}

export default connect(
  mapStateToProps,
  { fetchAllUsers }
)(TeamDashboard);
