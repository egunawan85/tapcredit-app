//stats Calculated within component from this.props.applicationList
import React, { Component } from "react";
import { Grid, Col, Row } from "react-bootstrap";
// react component used to create charts
import ChartistGraph from "react-chartist";
// react components used to create a SVG / Vector map
import { VectorMap } from "react-jvectormap";
import moment from "moment-timezone";

import Card from "components/Card/Card.jsx";
import StatsCard from "components/Card/StatsCard.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import ApplicationList from "components/Tables/ApplicationList.jsx";

import { currencyFormat } from "utils";

import "./agentDashboard.css";

import {
  dataPie,
  dataSales,
  optionsSales,
  responsiveSales,
  dataBar,
  optionsBar,
  responsiveBar,
  table_data
} from "variables/Variables.jsx";

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};

class AgentDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      applicationsApplied: this.applicationsApplied().length,
      appliedList: this.applicationsApplied(),
      applicationsApproved: this.applicationsApproved().length,
      approvedList: this.applicationsApproved()
    };
  }

  createTableData() {
    var tableRows = [];
    for (var i = 0; i < table_data.length; i++) {
      tableRows.push(
        <tr key={i}>
          <td>
            <div className="flag">
              <img src={table_data[i].flag} alt="us_flag" />
            </div>
          </td>
          <td>{table_data[i].country}</td>
          <td className="text-right">{table_data[i].count}</td>
          <td className="text-right">{table_data[i].percentage}</td>
        </tr>
      );
    }
    return tableRows;
  }

  redirect() {
    return this.props.history.push("/register/customer");
  }

  applicationsApplied() {
    const applicationList = this.props.applicationList || [];
    const appliedList = applicationList.filter(application => application.status == 1);
    return appliedList;
  }

  applicationsApproved() {
    const applicationList = this.props.applicationList || [];
    const approvedList = applicationList.filter(application => application.approval == 2);
    return approvedList;
  }

  render() {
    return (
      <div className="main-content agentDashboard">
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-note2 text-warning" />}
                statsText="Cicilan Diajukan"
                statsValue={this.state.applicationsApplied}
                statsIcon={this.state.appliedList.length > 0 ? <i className="fa fa-refresh" /> : ""}
                statsIconText={
                  this.state.appliedList.length > 0 ? (
                    moment(this.state.appliedList[0].updatedAt).format("DD/MM/YYYY")
                  ) : (
                    <Button bsSize="xs" simple wd onClick={e => this.redirect()}>
                      Mulai Sekarang!
                    </Button>
                  )
                }
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-home text-success" />}
                statsText="Cicilan Disetujui"
                statsValue={this.state.applicationsApproved}
                statsIcon={<i className="fa fa-calendar-o" />}
                statsIconText={
                  this.state.applicationsApproved.length > 0
                    ? moment(this.state.applicationsApproved[0].updatedAt).format("DD/MM/YYYY")
                    : "--"
                }
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-like2 text-danger" />}
                statsText="Cicilan Dibayar"
                statsValue={currencyFormat(0, 0)}
                statsIcon={<i className="fa fa-clock-o" />}
                statsIconText="--"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-cash text-info" />}
                statsText="Komisi Agen"
                statsValue={currencyFormat(0, 0)}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="--"
              />
            </Col>
          </Row>
        </Grid>
        <ApplicationList />
      </div>
    );
  }
}

export default AgentDashboard;
