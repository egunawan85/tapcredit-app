//stats Calculated within component from this.props.applicationList
import React, { Component } from "react";
import { connect } from "react-redux";

import { fetchApplicationList } from "modules/applications";
import { fetchUser } from "modules/auth";

import AgentDashboard from "./dashboard/AgentDashboard";
import TeamDashboard from "./dashboard/TeamDashboard";
import "./dashboard.css";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await this.props.fetchUser();
    this.props.auth.mainRole == "agent"
      ? await this.props.fetchApplicationList(this.props.auth.userID, { applicationFormID: 3 })
      : await this.props.fetchApplicationList(null, { applicationFormID: 3 });
    await this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) return null;
    return (
      <div>
        {this.props.auth.mainRole == "agent" ? <AgentDashboard {...this.props} /> : <TeamDashboard {...this.props} />}
      </div>
    );
  }
}

function mapStateToProps({ applicationList, auth }) {
  return { applicationList: applicationList, auth: auth };
}

export default connect(
  mapStateToProps,
  { fetchApplicationList, fetchUser }
)(Dashboard);
