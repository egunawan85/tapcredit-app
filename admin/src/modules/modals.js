/** Constants */
const SHOW_MODAL = "SHOW_MODAL";
const HIDE_MODAL = "HIDE_MODAL";

/** Action-creators */
export const loadModal = (modalType, modalProps = null) => {
	return {
		type: SHOW_MODAL,
		modalType: modalType,
		modalProps: modalProps
	};
};

export const hideModal = () => {
	return {
		type: HIDE_MODAL
	};
};

/** Initial State */
const initialModalState = {
	modalType: null,
	modalProps: {}
};

/** Modal reducer */
const modalTypeReducer = (state = null, action) => {
	switch (action.type) {
		case SHOW_MODAL:
			return action.modalType;
		case HIDE_MODAL:
			return null;
		default:
			return state;
	}
};

const modalPropsReducer = (state = null, action) => {
	switch (action.type) {
		case SHOW_MODAL:
			return action.modalProps;
		default:
			return modalTypeReducer;
	}
};

const modalReducer = {
	modalType: modalTypeReducer,
	modalProps: modalPropsReducer
};

export default modalReducer;
