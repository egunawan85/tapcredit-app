//reducer that reorgs data structure

import axios from "axios";

//types
const FETCH_APPLICATION = "fetch_application";
const FETCH_APPLICATION_LIST = "fetch_application_list";

//reducers
const reducer = (state = null, action) => {
	switch (action.type) {
		case FETCH_APPLICATION:
			return action.payload || false;
		default:
			return state;
	}
};

const applicationListReducer = (state = null, action) => {
	switch (action.type) {
		case FETCH_APPLICATION_LIST:
			return action.payload || false;
		default:
			return state;
	}
};

const applicationReducer = {
	application: reducer,
	applicationList: applicationListReducer
};

export default applicationReducer;

//actions
export const fetchApplication = applicationID => async dispatch => {
	const res = applicationID ? await axios.get(`/api/applications/read/${applicationID}`) : { data: {} };
	console.log(res.data);
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const fetchApplicationList = (ownedApplications = null, conditions = null) => async dispatch => {
	const res = await axios.post("/api/applications/read/", {
		ownedApplications: ownedApplications,
		conditions: conditions
	});
	dispatch({ type: FETCH_APPLICATION_LIST, payload: res.data });
};

export const createApplication = (ownerUserID, applicantUserID, applicationFormID) => async dispatch => {
	const res = await axios.post("/api/applications/create", {
		ownerUserID: ownerUserID,
		applicantUserID: applicantUserID,
		applicationFormID: applicationFormID
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const setField = (fieldKey, fieldValue) => async dispatch => {
	const res = await axios.post("/api/applications/update/:applicationID/fields", {
		fieldKey: fieldKey,
		fieldValue: fieldValue
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const updateStep = step => async dispatch => {
	const res = await axios.post("/api/applications/update/:applicationID/step", {
		step: step
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const setApplicationStatus = status => async dispatch => {
	const res = await axios.post("/api/applications/update/:applicationID/status", {
		status: status
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const setApplicationApproval = (applicationID, approval) => async dispatch => {
	const res = await axios.post(`/api/applications/update/${applicationID}/approval`, {
		approval: approval
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const submitApplication = (metas, fields) => async dispatch => {
	const res = await axios.post("/api/applications/submit", {
		metas: metas,
		fields: fields
	});
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const deleteApplication = applicationID => async dispatch => {
	const res = await axios.get(`/api/applications/delete/${applicationID}`);
	dispatch({ type: FETCH_APPLICATION, payload: res.data });
};

export const APPROVAL_STATUS = {
	PENDING: 0,
	PASS: 1,
	APPROVED: 2
};
