//fetch_all_users copy from auth and applications
import axios from "axios";

//types
const FETCH_ALL_USERS = "fetch_all_users";

//reducers
const reducer = (state = null, action) => {
	switch (action.type) {
		case FETCH_ALL_USERS:
			return action.payload || false;
		default:
			return state;
	}
};

const usersReducer = {
	users: reducer
};

export default usersReducer;

//actions
export const fetchAllUsers = () => async dispatch => {
	const res = await axios.get("/api/users/read/");
	dispatch({ type: FETCH_ALL_USERS, payload: res.data });
};
