import moment from "moment-timezone";

export const currencyFormat = (currency, num, symbol = true) => {
	const USD = 1;
	let prefix, separator, decimal;
	switch (currency) {
		case USD:
			prefix = "$";
			separator = ",";
			decimal = true;
			break;
		default:
			prefix = "Rp. ";
			separator = ".";
			decimal = false;
			break;
	}
	if (symbol == null) prefix = "";
	if (num || num === 0) {
		return decimal
			? prefix +
					num
						.toFixed(2)
						.toString()
						.replace(/\B(?=(\d{3})+(?!\d))/g, separator)
			: prefix + num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
	}
};

export const now = moment().tz("Asia/Jakarta");
