import React from "react";
import { connect } from "react-redux";

/** Modal Components */
import ApprovalModal from "./dialogs/ApprovalModal";
import PassModal from "./dialogs/PassModal";
import DeleteModal from "./dialogs/DeleteModal";

const MODAL_COMPONENTS = {
  APPROVAL_MODAL: ApprovalModal,
  PASS_MODAL: PassModal,
  DELETE_MODAL: DeleteModal
};

const ModalContainer = props => {
  if (!props.modalType) {
    return null;
  }
  console.log(props);
  const SpecificModal = MODAL_COMPONENTS[props.modalType];
  return <SpecificModal {...props.modalProps} />;
};

const mapStateToProps = state => {
  return {
    modalType: state.modalType,
    modalProps: state.modalProps
  };
};

export default connect(mapStateToProps)(ModalContainer);
