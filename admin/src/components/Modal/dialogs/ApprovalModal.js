import React, { Component } from "react";
import { connect } from "react-redux";
import { hideModal } from "modules/modals";
import { setApplicationApproval } from "modules/applications";

import SweetAlert from "react-bootstrap-sweetalert";
import Modal from "../Modal";

class ApprovalModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      show: false
    };
    this.onClose = this.onClose.bind(this);
  }

  componentDidMount() {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Apakah Anda yakin?"
          onConfirm={() => this.successApproval()}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Iya, menyetujui cicilan"
          cancelBtnText="Batal"
          hideOverlay={true}
          showCancel
        >
          Anda akan menyetujui cicilan ini
        </SweetAlert>
      )
    });
  }

  async successApproval(applicationID, approval) {
    await this.props.setApplicationApproval(applicationID, "approved");
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Selesai!"
          onConfirm={() => {
            this.onClose();
            window.location.reload();
          }}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
        >
          Cicilan telah disetujui
        </SweetAlert>
      )
    });
  }

  onClose() {
    this.props.hideModal();
  }

  render() {
    return <Modal onClose={this.onClose}>{this.state.alert}</Modal>;
  }
}

export default connect(
  null,
  { hideModal, setApplicationApproval }
)(ApprovalModal);
