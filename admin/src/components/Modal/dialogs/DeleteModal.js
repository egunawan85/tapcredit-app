import React, { Component } from "react";
import { connect } from "react-redux";
import { hideModal } from "modules/modals";
import { deleteApplication } from "modules/applications";

import SweetAlert from "react-bootstrap-sweetalert";
import Modal from "../Modal";

class ApprovalModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      show: false
    };
    this.onClose = this.onClose.bind(this);
  }

  componentDidMount() {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Apa Anda yakin?"
          onConfirm={() => this.successDelete(this.props.applicationID)}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Iya, saya ingin hapus!"
          cancelBtnText="Batal"
          showCancel
        >
          Pengajuan ini akan di hapus!
        </SweetAlert>
      )
    });
  }

  async successDelete(applicationID) {
    await this.props.deleteApplication(applicationID);
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => {
            this.onClose();
            window.location.reload();
          }}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
        >
          Pengajuan ini telah di hapus
        </SweetAlert>
      )
    });
  }

  onClose() {
    this.props.hideModal();
  }

  render() {
    return <Modal onClose={this.onClose}>{this.state.alert}</Modal>;
  }
}

export default connect(
  null,
  { hideModal, deleteApplication }
)(ApprovalModal);
