import React, { Component } from "react";
import { connect } from "react-redux";
import { hideModal } from "modules/modals";
import { setApplicationApproval } from "modules/applications";

import SweetAlert from "react-bootstrap-sweetalert";
import Modal from "../Modal";

class PassModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      show: false
    };
    this.onClose = this.onClose.bind(this);
  }

  componentDidMount() {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Apakah Anda yakin?"
          onConfirm={() => this.successPass(this.props.applicationID)}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Iya, menolak cicilan"
          cancelBtnText="Batal"
          showCancel
        >
          Anda akan menolak cicilan ini
        </SweetAlert>
      )
    });
  }

  async successPass(applicationID, approval) {
    await this.props.setApplicationApproval(applicationID, "pass");
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Selesai!"
          onConfirm={() => {
            this.onClose();
            window.location.reload();
          }}
          onCancel={() => this.onClose()}
          confirmBtnBsStyle="info"
        >
          "Cicilan telah ditolak"
        </SweetAlert>
      )
    });
  }

  onClose() {
    this.props.hideModal();
  }

  render() {
    return <Modal onClose={this.onClose}>{this.state.alert}</Modal>;
  }
}

export default connect(
  null,
  { hideModal, setApplicationApproval }
)(PassModal);
