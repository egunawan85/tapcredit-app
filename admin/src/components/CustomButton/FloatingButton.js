import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Icon from "@material-ui/core/Icon";
import DeleteIcon from "@material-ui/icons/Delete";
import NavigationIcon from "@material-ui/icons/Navigation";
import _ from "lodash";

import "./floatingButton.css";

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "40px",
    right: "20px",
    minHeight: "28px",
    "z-index": "1000000",
    "background-color": "#25d366"
  },
  fa: {
    width: "100%"
  }
});

class FloatingActionButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrolling: false
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.handleNoScroll = this.handleNoScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    window.addEventListener(
      "scroll",
      _.debounce(() => {
        this.handleNoScroll();
      }, 50)
    );
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
    window.removeEventListener(
      "scroll",
      _.debounce(() => {
        this.handleNoScroll();
      }, 50)
    );
  }

  handleScroll() {
    console.log("go");
    this.setState({ scrolling: true });
  }

  handleNoScroll() {
    console.log("stop");
    this.setState({ scrolling: false });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <a target="_blank" href={"https://wa.me/628111737688"}>
          <Fab
            color="primary"
            aria-label="Add"
            className={`${classes.fab} ${this.state.scrolling ? "scroll" : "noscroll"}`}
          >
            <i className={`fa fa-whatsapp ${classes.fa} ${this.state.scrolling ? "scroll" : "noscroll"}`} />
          </Fab>
        </a>
      </div>
    );
  }
}

FloatingActionButtons.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FloatingActionButtons);
