import React, { Component } from "react";

import "./welcomeCard.css";

export class WelcomeCard extends Component {
  render() {
    return (
      <div className="card card-stats">
        <div className="content">
          <div className="row">
            <div className="col-xs-9">
              <div className="heading">
                <p>Welcome, Edward!</p>
                <h5>Apply your customers</h5>
              </div>
            </div>
            <div className="col-xs-3">
              <div className="icon-big text-center icon-warning">{this.props.bigIcon}</div>
            </div>
            <div className="col-xs-12">
              <p className="text-muted">
                We are here to help you and your customers. Incentive them today with some amazing loans from TapCredit
              </p>
            </div>
          </div>
        </div>
        <div className="footer">
          <hr />
          <div className="apply">Ajukan Pinjaman</div>
        </div>
      </div>
    );
  }
}

export default WelcomeCard;
