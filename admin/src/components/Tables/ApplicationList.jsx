import React, { Component } from "react";
import { Link } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import SweetAlert from "react-bootstrap-sweetalert";
import ReactWindowResizeListener from "window-resize-listener-react";

import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";

import { Grid, Row, Col, OverlayTrigger, Tooltip } from "react-bootstrap";
import { connect } from "react-redux";

import { currencyFormat } from "utils";
import { fetchApplicationList, deleteApplication } from "modules/applications";
import { fetchUser } from "modules/auth";

import Card from "components/Card/Card.jsx";
import ListActions from "./applicationList/ListActions";

import "./applicationList.css";

const { SearchBar } = Search;

class ApplicationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      width: null,
      height: null,
      columns: [
        {
          dataField: "email",
          text: "Email",
          sort: true
        },
        {
          dataField: "name",
          text: "Nama",
          sort: true
        },
        {
          dataField: "amount",
          text: "Jumlah Sewa",
          headerAlign: "left",
          align: "left",
          sort: true
        },
        {
          dataField: "status",
          text: "Status",
          headerAlign: "left",
          align: "left",
          sort: true
        },
        {
          dataField: "action",
          text: "Action",
          headerAlign: "right",
          align: "right"
        }
      ],
      data: null
    };
    this.hideAlert = this.hideAlert.bind(this);
    this.successDelete = this.successDelete.bind(this);
    this.resizeHandler = this.resizeHandler.bind(this);
  }

  resizeHandler(event) {
    this.setState({ width: window.innerWidth });
  }

  async componentDidMount() {
    this.resizeHandler();
    await this.props.fetchUser();
    await this.props.fetchApplicationList;
    await this.setData();
    await this.setState({ loading: false });
  }

  async deleteApplication(applicationID) {
    await this.props.deleteApplication(applicationID);
  }

  async setData() {
    console.log(this.props.applicationList);
    await this.setState({
      data: this.props.applicationList.map((prop, key) => {
        return {
          id: key,
          email: prop.email,
          name: `${prop.firstName} ${prop.lastName}`,
          amount: currencyFormat("idr", prop.amount) || "--",
          status: this.statusToString(prop.status, prop.approval),
          applicationID: prop.applicationID,
          action: <ListActions prop={prop} />
        };
      })
    });
  }

  warningWithConfirmMessage(applicationID) {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.successDelete(applicationID)}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Yes, delete it!"
          cancelBtnText="Cancel"
          showCancel
        >
          You will not be able to recover this imaginary file!
        </SweetAlert>
      )
    });
  }

  async successDelete(applicationID) {
    await this.deleteApplication(applicationID);
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
        >
          Your imaginary file has been deleted.
        </SweetAlert>
      )
    });
  }
  hideAlert() {
    this.setState({
      alert: null
    });
    window.location.reload();
  }

  statusToString(status, approval) {
    if (approval == 2) {
      return "Disetujui";
    }
    if (approval == 1) {
      return "Ditolak";
    }
    if (!approval && status) {
      return "Sedang di proses";
    }
    if (!approval && !status) {
      return "Belum selesai";
    }
  }
  paginationOptions() {
    const options = {
      paginationSize: 4,
      pageStartIndex: 0
    };
    return options;
  }

  expandRow() {
    return {
      renderer: row => (
        <div>
          <ListActions {...this.props} prop={row} expandRow={true} />
          <p>
            Name:<span className="detail">{row.name}</span>
          </p>
          <p>
            Email:<span className="detail">{row.email}</span>
          </p>
          <p>
            Amount:<span className="detail">{row.amount}</span>
          </p>
          <p>
            Status:<span className="detail">{row.status}</span>
          </p>
        </div>
      ),
      showExpandColumn: true,
      expandColumnPosition: "right",
      expandHeaderColumnRenderer: ({ isAnyExpands }) => {
        if (isAnyExpands) {
          return <b>-</b>;
        }
        return <b>+</b>;
      },
      expandColumnRenderer: ({ expanded }) => {
        if (expanded) {
          return <i className="fa fa-sort-up" />;
        }
        return <i className="fa fa-sort-down" />;
      }
    };
  }

  render() {
    if (this.state.loading) return null;
    return (
      <Grid fluid>
        <ReactWindowResizeListener onResize={this.resizeHandler} />
        <Row>
          <Col md={12} className="applicationList">
            <Card
              title="Application List"
              category="View and track your applications here"
              content={
                <ToolkitProvider keyField="id" data={this.state.data} columns={this.state.columns} search>
                  {props => (
                    <div>
                      <div className="searchWrapper">
                        <SearchBar {...props.searchProps} />
                        <i className="pe-7s-search searchIcon" />
                      </div>
                      <BootstrapTable
                        bordered={false}
                        striped
                        pagination={paginationFactory(this.paginationOptions())}
                        expandRow={this.state.width <= 767 ? this.expandRow() : {}}
                        {...props.baseProps}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              }
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps({ applicationList, application, auth }) {
  return { applicationList: applicationList, application: application, auth: auth };
}

export default connect(
  mapStateToProps,
  { fetchApplicationList, deleteApplication, fetchUser }
)(ApplicationList);
