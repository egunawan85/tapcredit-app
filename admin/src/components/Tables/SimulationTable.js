//payment schedule table

import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ReactWindowResizeListener from "window-resize-listener-react";
import { Grid, Row, Col } from "react-bootstrap";
import { currencyFormat } from "utils";
import Card from "components/Card/Card.jsx";

import "./simulationTable.css";

class SimulationTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			width: null,
			height: null,
			amount: this.props.amount,
			duration: this.props.duration,
			interest: this.props.interest,
			columns: [
				{
					dataField: "month",
					text: "Bulan ke-"
				},
				{
					dataField: "principal",
					text: "Pokok cicilan"
				},
				{
					dataField: "interestRate",
					text: "Bunga"
				},
				{
					dataField: "interestFee",
					text: "Biaya"
				},
				{
					dataField: "totalPayment",
					text: "Total Jatuh Tempo"
				}
			],
			data: null
		};
	}

	calculateData(amount, duration, interest) {
		const payments = { schedule: [], totals: {} };
		const monthsUpFront = duration / 6;
		let i,
			payment,
			month,
			monthlyAmount,
			principal,
			interestRate,
			interestFee,
			totalPayment,
			totalPrincipal = 0,
			totalInterestFee = 0,
			grandTotal = 0;
		for (i = 0; i < duration; i++) {
			month = i + 1;
			monthlyAmount = amount / duration;
			principal = i < duration - monthsUpFront ? monthlyAmount : 0;
			interestRate = i < duration - monthsUpFront ? (interest * 100) / (duration - monthsUpFront) : 0;
			interestFee = i < duration - monthsUpFront ? (amount * interest) / (duration - monthsUpFront) : 0;
			totalPayment = principal + interestFee;
			totalPrincipal += principal;
			totalInterestFee += interestFee;
			grandTotal += totalPayment;
			payment = {
				month: month,
				principal: Math.round(principal),
				interestRate: Math.round(interestRate * 10) / 10 + "%",
				interestFee: Math.round(interestFee),
				totalPayment: Math.round(totalPayment)
			};
			payments.schedule.push(payment);
		}
		payments.totals = {
			...payments.totals,
			...{
				totalPrincipal: totalPrincipal,
				totalInterestFee: totalInterestFee,
				grandTotal: grandTotal
			}
		};
		return payments;
	}

	setData(payments) {
		return payments.schedule.map((payment, key) => {
			return {
				id: key,
				month: payment.month,
				principal: currencyFormat("idr", payment.principal),
				interestRate: payment.interestRate,
				interestFee: currencyFormat("idr", payment.interestFee),
				totalPayment: currencyFormat("idr", payment.totalPayment)
			};
		});
	}

	expandRow() {
		return {
			renderer: row => (
				<div>
					<p>
						Bulan ke-:<span className="detail">{row.month}</span>
					</p>
					<p>
						Pokok cicilan:<span className="detail">{row.principal}</span>
					</p>
					<p>
						Bunga:<span className="detail">{row.interestRate}</span>
					</p>
					<p>
						Biaya:<span className="detail">{row.interestFee}</span>
					</p>
					<p>
						Total Jatuh Tempo:<span className="detail">{row.totalPayment}</span>
					</p>
				</div>
			),
			showExpandColumn: true,
			expandColumnPosition: "right",
			expandHeaderColumnRenderer: ({ isAnyExpands }) => {
				if (isAnyExpands) {
					return <b>-</b>;
				}
				return <b>+</b>;
			},
			expandColumnRenderer: ({ expanded }) => {
				if (expanded) {
					return <i className="fa fa-sort-up" />;
				}
				return <i className="fa fa-sort-down" />;
			}
		};
	}

	render() {
		const payments = this.calculateData(this.props.amount, this.props.duration, this.props.interest);
		const data = this.setData(payments);
		console.log(this.props);
		return (
			<Grid fluid>
				<ReactWindowResizeListener onResize={this.resizeHandler} />
				<Row>
					<Col md={12} className="simulationTable">
						<Card
							title="Jadwal Pembayaran"
							content={
								<div>
									<BootstrapTable
										keyField="id"
										data={data}
										columns={this.state.columns}
										bordered={false}
										striped
										expandRow={this.state.width <= 1200 ? this.expandRow() : {}}
									/>
									<div className="totals">
										<p>
											Semua Cicilan<span>:</span>{" "}
										</p>
										<p>
											{currencyFormat("idr", Math.round(payments.totals.totalPrincipal))}
											<span>|</span>
										</p>
										<p>
											Biaya<span>:</span>{" "}
										</p>
										<p>
											{currencyFormat("idr", Math.round(payments.totals.totalInterestFee))}
											<span>|</span>
										</p>
										<p>
											Cicilan + Biaya<span>:</span>{" "}
										</p>
										<p>{currencyFormat("idr", Math.round(payments.totals.grandTotal))}</p>
									</div>
									<div className="notes">
										<p>Catatan:</p>
										<p>
											Pembayaran sebesar{" "}
											{currencyFormat(
												"idr",
												Math.round(this.props.amount / this.props.duration) *
													(this.props.duration / 6)
											)}{" "}
											dan deposit sebesar{" "}
											{currencyFormat("idr", Math.round(this.props.amount / this.props.duration))}{" "}
											sebelum tenant masuk.
										</p>
										<p>
											Deposit sebesar{" "}
											{currencyFormat("idr", Math.round(this.props.amount / this.props.duration))}{" "}
											dikembalikan pada bulan terakhir.
										</p>
									</div>
								</div>
							}
						/>
					</Col>
				</Row>
			</Grid>
		);
	}
}

export default SimulationTable;
