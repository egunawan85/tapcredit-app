import React, { Component } from "react";
import { Link } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
import Can from "components/Can/Can.jsx";
import { connect } from "react-redux";

import { APPROVAL_MODAL, PASS_MODAL, DELETE_MODAL } from "components/Modal/dialogs/modalTypes";
import { fetchUser } from "modules/auth";
import { loadModal } from "modules/modals";
import { setApplicationApproval, deleteApplication, APPROVAL_STATUS } from "modules/applications";

import "./listActions.css";

class ListActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prevPosition: null,
      loading: true,
      alert: null,
      show: false
    };
    this.hideAlert = this.hideAlert.bind(this);
    this.successDelete = this.successDelete.bind(this);
    this.warningWithConfirmMessage = this.warningWithConfirmMessage.bind(this);
    this.approvalConfirmMessage = this.approvalConfirmMessage.bind(this);
    this.showApprovalDialog = this.showApprovalDialog.bind(this);
    this.showPassDialog = this.showPassDialog.bind(this);
    this.showDeleteDialog = this.showDeleteDialog.bind(this);
  }

  async componentDidMount() {
    await this.setState({
      loading: false
    });
  }

  showApprovalDialog() {
    const modalProps = { applicationID: this.props.prop.applicationID };

    this.props.loadModal(APPROVAL_MODAL, modalProps);
  }

  showPassDialog() {
    const modalProps = { applicationID: this.props.prop.applicationID };

    this.props.loadModal(PASS_MODAL, modalProps);
  }

  showDeleteDialog() {
    const modalProps = { applicationID: this.props.prop.applicationID };

    this.props.loadModal(DELETE_MODAL, modalProps);
  }

  async deleteApplication(applicationID) {
    await this.props.deleteApplication(applicationID);
    window.location.reload();
  }

  async warningWithConfirmMessage(applicationID) {
    await this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.successDelete(applicationID)}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Yes, delete it!"
          cancelBtnText="Cancel"
          showCancel
        >
          You will not be able to recover this imaginary file!
        </SweetAlert>
      )
    });
  }

  async approvalConfirmMessage(applicationID, approval = true) {
    await this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Apakah Anda yakin?"
          onConfirm={() => this.successApproval(applicationID, approval)}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText={approval ? "Iya, menyetujui cicilan" : "Iya, menolak cicilan"}
          cancelBtnText="Batal"
          showCancel
        >
          {approval ? "Anda akan menyetujui cicilan ini" : "Anda akan menolak cicilan ini"}
        </SweetAlert>
      )
    });
  }

  async successApproval(applicationID, approval) {
    approval
      ? await this.props.setApplicationApproval(applicationID, "approved")
      : await this.props.setApplicationApproval(applicationID, "pass");
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Selesai!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
        >
          {approval ? "Cicilan telah disetujui" : "Cicilan telah ditolak"}
        </SweetAlert>
      )
    });
  }

  async successDelete(applicationID) {
    await this.deleteApplication(applicationID);
    await this.setState({
      alert: (
        <SweetAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
        >
          Pengajuan ini telah di hapus
        </SweetAlert>
      )
    });
  }

  async hideAlert() {
    await this.setState({
      alert: null
    });
  }

  statusToString(status, approval) {
    if (approval == 2) {
      return "Disetujui";
    }
    if (approval == 1) {
      return "Ditolak";
    }
    if (!approval && status) {
      return "Sedang di proses";
    }
    if (!approval && !status) {
      return "Belum selesai";
    }
  }

  render() {
    if (this.state.loading) return null;
    const prop = this.props.prop;
    const status = this.props.expandRow ? prop.status : this.statusToString(prop.status, prop.approval);
    return (
      <div className="listActions">
        {this.state.alert}
        {console.log(prop)}
        {console.log(status == "Belum selesai")}
        {console.log(status)}
        <Can
          role={this.props.auth.mainRole}
          perform="applications:update"
          yes={() =>
            status == "Belum selesai" ? (
              <Link className="action" to={`/register/customer/${prop.applicationID}`}>
                ubah
              </Link>
            ) : (
              <Link className="action" to={`/register/customer/${prop.applicationID}`}>
                lihat
              </Link>
            )
          }
        />
        <Can
          role={this.props.auth.mainRole}
          perform="applications:approve"
          yes={() => {
            console.log(status);
            return status == "Sedang di proses" ? (
              <span className="action" onClick={this.showPassDialog}>
                tolak
              </span>
            ) : null;
          }}
        />
        <Can
          role={this.props.auth.mainRole}
          perform="applications:approve"
          yes={() => {
            return status == "Sedang di proses" ? (
              <span className="action" onClick={this.showApprovalDialog}>
                setuju
              </span>
            ) : null;
          }}
        />
        <Can
          role={this.props.auth.mainRole}
          perform="applications:delete-before-submit"
          yes={() => {
            return status == "Belum selesai" ? (
              <span className="action delete" onClick={this.showDeleteDialog}>
                hapus
              </span>
            ) : null;
          }}
        />
        <Can
          role={this.props.auth.mainRole}
          perform="applications:delete"
          yes={() => {
            return (
              <span className="action delete" onClick={this.showDeleteDialog}>
                hapus
              </span>
            );
          }}
        />
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth: auth };
}

const mapDispatchToProps = dispatch => ({
  loadModal: (modalType, modalProps) => dispatch(loadModal(modalType, modalProps)),
  fetchUser: () => dispatch(fetchUser()),
  setApplicationApproval: (applicationID, approval) => dispatch(setApplicationApproval(applicationID, approval)),
  deleteApplication: applicationID => dispatch(deleteApplication(applicationID))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListActions);
