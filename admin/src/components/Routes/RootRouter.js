import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchUser } from "modules/auth";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Modal from "components/Modal/Modal";

import indexRoutes from "routes/index.jsx";

class RootRouter extends Component {
	state = {
		loaded: false
	};

	async componentDidMount() {
		await this.props.fetchUser();
	}

	render() {
		if (!this.props.auth) return null;
		return (
			<BrowserRouter basename={this.props.auth.mainRole === "agent" ? "agent" : "admin"}>
				<Switch>
					{indexRoutes.map((prop, key) => {
						const RouteType = prop.routeType;
						return <RouteType path={prop.path} component={prop.component} key={key} />;
					})}
				</Switch>
			</BrowserRouter>
		);
	}
}

function mapStateToProps({ auth }) {
	return { auth: auth };
}

export default connect(
	mapStateToProps,
	{ fetchUser }
)(RootRouter);
