import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { fetchUser } from "modules/auth";
import parsePath from "parse-path";

class AdminRoute extends React.Component {
	state = {
		loaded: false,
		isAuthenticated: false
	};

	async componentDidMount() {
		await this.props.fetchUser();
		this.authenticate();
	}

	async authenticate() {
		this.props.auth.mainRole === "superAdmin" ||
		this.props.auth.mainRole === "admin" ||
		this.props.auth.mainRole === "accountOfficer" ||
		this.props.auth.mainRole === "agent"
			? await this.setState({ isAuthenticated: true, loaded: true })
			: await this.setState({ isAuthenticated: false, loaded: true });
	}

	render() {
		const { component: Component, ...rest } = this.props;
		const { isAuthenticated, loaded } = this.state;
		if (!loaded) return null;
		if (isAuthenticated) {
			return (
				<Route
					{...rest}
					render={props => {
						return <Component {...props} />;
					}}
				/>
			);
		} else {
			const url = parsePath(window.location.href);
			const route = (url, path) => {
				return url.port
					? (window.location = `${url.protocol}://${url.resource}:${url.port + path}`)
					: (window.location = `${url.protocol}://${url.resource + path}`);
			};
			route(url, "/api/router");
		}
	}
}

function mapStateToProps({ auth }) {
	return { auth: auth };
}

export default connect(
	mapStateToProps,
	{ fetchUser }
)(AdminRoute);
