import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import { combineReducers } from "redux";

import authReducer from "modules/auth";
import usersReducer from "modules/users";
import applicationReducer from "modules/applications";
import modalReducer from "modules/modals";

const allReducers = Object.assign({}, authReducer, applicationReducer, usersReducer, modalReducer);
const rootReducer = combineReducers(allReducers);

const store = createStore(rootReducer, {}, applyMiddleware(reduxThunk));

export default store;
