import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import store from "store";

import indexRoutes from "./routes/index.jsx";
import RootRouter from "components/Routes/RootRouter";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/sass/light-bootstrap-dashboard.css";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";
import "./assets/css/index.css";

ReactDOM.render(
	<Provider store={store}>
		<RootRouter />
	</Provider>,
	document.getElementById("root")
);
