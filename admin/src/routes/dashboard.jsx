//simulation object

import Dashboard from "views/Dashboard/Dashboard.jsx";
import Buttons from "views/Components/Buttons.jsx";
import GridSystem from "views/Components/GridSystem.jsx";
import Panels from "views/Components/Panels.jsx";
import SweetAlert from "views/Components/SweetAlertPage.jsx";
import Notifications from "views/Components/Notifications.jsx";
import Icons from "views/Components/Icons.jsx";
import Typography from "views/Components/Typography.jsx";
import RegularForms from "views/Forms/RegularForms.jsx";
import ExtendedForms from "views/Forms/ExtendedForms.jsx";
import ValidationForms from "views/Forms/ValidationForms.jsx";
import Wizard from "views/Forms/Wizard/Wizard.jsx";
import RegularTables from "views/Tables/RegularTables.jsx";
import ExtendedTables from "views/Tables/ExtendedTables.jsx";
import ReactTables from "views/Tables/ReactTables.jsx";
import GoogleMaps from "views/Maps/GoogleMaps.jsx";
import FullScreenMap from "views/Maps/FullScreenMap.jsx";
import VectorMap from "views/Maps/VectorMap.jsx";
import Charts from "views/Charts/Charts.jsx";
import Calendar from "views/Calendar/Calendar.jsx";
import UserPage from "views/Pages/UserPage.jsx";
import Team from "views/Registration/Team.jsx";
import Agent from "views/Registration/Agent.jsx";
import Client from "views/Registration/Client.jsx";
import Simulation from "views/Simulation/Simulation";

import pagesRoutes from "./pages.jsx";

var pages = [{ path: "/user-page", name: "User Page", mini: "UP", component: UserPage }].concat(pagesRoutes);

var dashboardRoutes = [
	{
		path: "/dashboard",
		name: "Dasbor",
		icon: "pe-7s-graph",
		component: Dashboard
	},
	{
		collapse: true,
		path: "/register",
		name: "Pendaftaran",
		state: "openComponents",
		icon: "pe-7s-plugin",
		views: [
			{
				path: "/register/team",
				name: "Tim",
				mini: "T",
				sidebar: true,
				permission: "register/team:visit",
				component: Team
			},
			{
				path: "/register/agent/:applicationID",
				name: "Agen",
				mini: "A",
				sidebar: false,
				permission: "register/agent:visit",
				component: Agent
			},
			{
				path: "/register/agent",
				name: "Agen",
				mini: "A",
				sidebar: true,
				permission: "register/agent:visit",
				component: Agent
			},
			{
				path: "/register/customer/:applicationID",
				name: "Pelanggan",
				mini: "P",
				sidebar: false,
				permission: "register/customer:visit",
				component: Client
			},
			{
				path: "/register/customer",
				name: "Pelanggan",
				mini: "P",
				sidebar: true,
				permission: "register/customer:visit",
				component: Client
			}
		]
	},
	{
		path: "/simulation",
		name: "Simulasi",
		icon: "pe-7s-graph",
		component: Simulation
	},
	{ redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashboardRoutes;
