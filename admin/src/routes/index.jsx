import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import AdminRoute from "components/Routes/AdminRoute";

var indexRoutes = [{ path: "/", name: "Admin", routeType: AdminRoute, component: Dashboard }];

export default indexRoutes;
