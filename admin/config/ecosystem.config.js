module.exports = {
  apps: [
    {
      name: "TapCredit.Admin",
      script: "node_modules/react-scripts/scripts/start.js",
      env: {
        NODE_ENV: "development",
        PORT: 3002
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
