const moment = require("moment-timezone");
const now = moment().tz("Asia/Jakarta");

const utils = {
	currentDate: () => {
		const time = moment(now);
		return time.format("YYYY-MM-DD");
	},

	currentDateTime: () => {
		const time = moment(now);
		return time.format("YYYY-MM-DD H:m:s");
	},

	currentTimestamp: () => {
		const time = moment(now);
		return time.format("X");
	}
};

module.exports.utils = utils;
module.exports.now = now;
