const db = require("@config/lib/sequelize");

db.users.sync({ force: false });
db.usermeta.sync({ force: false });
db.applications.sync({ force: false });
db.applicationFields.sync({ force: false });
db.applicationForms.sync({ force: false });
db.accounts.sync({ force: false });
db.creditLines.sync({ force: false });
db.transactions.sync({ force: false });
db.loans.sync({ force: false });
db.fees.sync({ force: false });
db.repayments.sync({ force: false });
db.interestFees.sync({ force: false });
db.overpayments.sync({ force: false });
db.payments.sync({ force: false });
db.latePaymentPlans.sync({ force: false });
db.paymentPlans.sync({ force: false });
db.paymentPlanDetails.sync({ force: false });
db.ledger.sync({ force: false });
db.ledgerDetails.sync({ force: false });

// Adds initial data to some tables, only for first time!
// db.latePaymentPlans.initialData();
// db.paymentPlans.initialData();
// db.paymentPlanDetails.initialData();
// db.applicationForms.initialData();
