const { snapToken } = require("@modules/snap/controllers/snap");
const requireLogin = require("@middlewares/requireLogin");

module.exports = app => {
	app.get("/api/snaptoken", requireLogin, (req, res) => {
		snapToken().then(result => res.send(result.data.token));
	});
};
