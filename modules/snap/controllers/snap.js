const axios = require("axios");
const keys = require("@config/keys");

const buffer = new Buffer.from(keys.midtransServerKey + ":");

const token = async () => {
	try {
		return await axios({
			method: "post",
			url: keys.midtransSnapURI,
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
				Authorization: "Basic " + buffer.toString("base64")
			},
			data: {
				transaction_details: {
					order_id: "ORDER-101",
					gross_amount: 10000
				}
			}
		});
	} catch (error) {
		console.error(error);
	}
};

module.exports.snapToken = token;
