const { utils } = require("@src/utils");

module.exports = (sequelize, Sequelize) => {
  const Usermeta = sequelize.define(
    "usermeta",
    {
      umetaID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      userID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      metaKey: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      metaValue: {
        type: Sequelize.TEXT("long"),
        allowNull: true
      }
    },
    {
      paranoid: true,
      indexes: [{ unique: true, fields: ["userID", "metaKey"] }]
    }
  );

  Usermeta.createMeta = async (userID, metaKey, metaValue) => {
    try {
      const results = await Usermeta.create({
        userID: userID,
        metaKey: metaKey,
        metaValue: metaValue
      });
      return results;
    } catch (e) {
      return e;
    }
  };

  Usermeta.getMeta = async (userID, metaKey) => {
    const results = await sequelize.query(
      `SELECT MAX(CASE usermeta.metaKey WHEN '${metaKey}' THEN usermeta.metaValue ELSE '' END) AS '${metaKey}' FROM usermeta WHERE userID = '${userID}'`,
      { type: sequelize.QueryTypes.SELECT }
    );
    return results[0][metaKey] ? results[0][metaKey] : null;
  };

  Usermeta.updateMeta = async (userID, metaKey, metaValue) => {
    const results = await Usermeta.update(
      { metaValue: metaValue },
      {
        where: { userID: userID, metaKey: metaKey }
      }
    );
    return results;
  };

  Usermeta.deleteMeta = async (userID, metaKey) => {
    try {
      const results = await Usermeta.destroy({
        where: { userID: userID, metaKey: metaKey }
      });
      return results;
    } catch (e) {
      return "deletion error";
    }
  };

  return Usermeta;
};
