module.exports = (sequelize, Sequelize) => {
  const Users = sequelize.define("users", {
    userID: {
      type: Sequelize.BIGINT(20),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userLogin: {
      type: Sequelize.STRING(60),
      allowNull: false,
      unique: "compositeIndex"
    },
    userPass: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    userNicename: {
      type: Sequelize.STRING(50),
      allowNull: false,
      unique: "compositeIndex"
    },
    userEmail: {
      type: Sequelize.STRING(100),
      allowNull: false,
      unique: "compositeIndex"
    },
    userRoles: {
      type: Sequelize.STRING(100),
      allowNull: false
    }
  });

  Users.getUser = async (key, id) => {
    let type;
    switch (key) {
      case "user":
        type = "userID";
        break;
      case "email":
        type = "userEmail";
        break;
      case "nicename":
        type = "userNicename";
        break;
    }
    const results = await Users.findOne({
      where: {
        [type]: id
      },
      raw: true
    });
    if (results) {
      results.userRoles = JSON.parse(results.userRoles);
      results.mainRole = await Users.mainRole(results.userRoles);
    }
    return results;
  };

  Users.getAllUsers = async () => {
    const results = await Users.findAll({
      raw: true,
      order: [["updatedAt", "DESC"]]
    });
    const users = [];
    if (results) {
      for (let result of results) {
        let user = await Users.getMetas(result);
        user.userRoles = JSON.parse(result.userRoles);
        user.mainRole = await Users.mainRole(user.userRoles);
        users.push(user);
      }
    }
    return users;
  };

  Users.updateUser = async (state, conditions) => {
    const results = await Users.update(state, {
      where: conditions
    });
    return results;
  };

  Users.deleteUser = async userID => {
    try {
      const results = await Users.destroy({
        where: { userID: userID }
      });
      return results;
    } catch (e) {
      return "deletion error";
    }
  };

  Users.metas = async userID => {
    const user = await Users.getUser("user", userID);
    if (user) {
      return await Users.getMetas(user);
    } else {
      console.log("No user found");
      return null;
    }
  };

  Users.getMetas = async user => {
    let usermetas = user;
    const results = await sequelize.query(`SELECT * FROM usermeta WHERE userID = ${user.userID}`, {
      type: sequelize.QueryTypes.SELECT
    });
    for (let usermeta of results) {
      usermetas = { ...usermetas, [usermeta.metaKey]: usermeta.metaValue };
    }
    return usermetas;
  };

  Users.mainRole = async roles => {
    const superAdmin = roles.find(role => role == "superAdmin");
    if (superAdmin) {
      return superAdmin;
    }
    const admin = roles.find(role => role == "admin");
    if (admin) {
      return admin;
    }
    const accountOfficer = roles.find(role => role == "accountOfficer");
    if (accountOfficer) {
      return accountOfficer;
    }
    const agent = roles.find(role => role == "agent");
    if (agent) {
      return agent;
    }
    const client = roles.find(role => role == "client");
    if (client) {
      return client;
    }
  };

  return Users;
};
