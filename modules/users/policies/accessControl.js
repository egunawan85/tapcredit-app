const AccessControl = require("accesscontrol");
const ac = new AccessControl();

ac.grant("client").readOwn("user");
ac.grant("agent")
	.extend("client")
	.createAny("user");
ac.grant("accountOfficer").extend("client");
ac.grant("admin").extend("accountOfficer");
ac.grant("superAdmin")
	.extend("admin")
	.updateAny("user")
	.readAny("user")
	.deleteAny("user");

ac.grant("client").updateOwn("profile");
ac.grant("agent").extend("client");
ac.grant("accountOfficer").extend("agent");
ac.grant("admin")
	.extend("accountOfficer")
	.updateAny("profile");
ac.grant("superAdmin").extend("admin");

module.exports = ac;

//superAdmin
//admin
//accountOfficer
//agent
//client
