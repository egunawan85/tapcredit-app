const db = require("@config/lib/sequelize");
const { utils } = require("@src/utils.js");
const Mailer = require("@services/Mailer");
const axios = require("axios");
const crypto = require("crypto");
const bCrypt = require("bcrypt-nodejs");
const owasp = require("owasp-password-strength-test");

const resetPasswordEmailContent = require("@services/emailTemplates/resetPassword");
const setFirstPasswordEmailContent = require("@services/emailTemplates/setFirstPassword");

owasp.config({
	allowPassphrases: true,
	maxLength: 128,
	minLength: 8,
	minPhraseLength: 20,
	minOptionalTestsToPass: 3
});

const userFn = {
	createSetPasswordToken: async userID => {
		try {
			return await db.sequelize.transaction(async t => {
				const token = await crypto.randomBytes(20).toString("hex");
				const result1 = await userFn.setMeta(userID, "setPasswordToken", token);
				const result2 = await userFn.setMeta(
					userID,
					"setPasswordTokenExpires",
					parseInt(utils.currentTimestamp()) + 3600
				);
				return token;
			});
		} catch (err) {
			return err;
		}
	},

	sendPasswordEmail: async (userID, token, req, type) => {
		const user = await db.users.getUser("user", userID);
		const url = `${req.protocol}://${req.headers.host}/login/setNewPassword/${token}`;
		const mailer =
			type === "reset"
				? new Mailer(user.userEmail, "Reset Password", resetPasswordEmailContent(url))
				: new Mailer(user.userEmail, "Set Your Password", setFirstPasswordEmailContent(url));
		mailer.send();
	},

	confirmValidToken: async token => {
		const found = await db.usermeta.findOne({
			where: {
				metaKey: "setPasswordToken",
				metaValue: token
			}
		});
		if (found) {
			const expiry = await db.usermeta.getMeta(found.userID, "setPasswordTokenExpires");
			return expiry >= utils.currentTimestamp() ? found.userID : false;
		} else {
			return false;
		}
	},

	setNewPassword: async (password, token) => {
		const userID = await userFn.confirmValidToken(token);
		if (!userID) {
			return { result: false, data: null, message: "Token tidak valid atau telah kadaluarsa" };
		}

		const passwordStrength = owasp.test(password);
		if (!passwordStrength.strong) {
			return { result: false, data: passwordStrength.errors, message: passwordStrength.errors };
		}

		const userPassword = bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
		try {
			const result = await db.sequelize.transaction(async t => {
				const result = await db.users.updateUser({ userPass: userPassword }, { userID: userID });
				const removeToken = await db.usermeta.updateMeta(userID, "PasswordToken", null);
				const removeTokenExpiry = await db.usermeta.updateMeta(userID, "setPasswordTokenExpires", null);
				return result;
			});
			console.log(result);
			if (result) {
				return { result: true, data: result, message: "Kata Sandi Anda telah diganti" };
			}
		} catch (err) {
			return { result: false, data: err, message: "Ada kesalahan yang terjadi" };
		}
	},

	setMeta: async (userID, metaKey, metaValue) => {
		const create = await db.usermeta.createMeta(userID, metaKey, metaValue);
		if (create.errors) {
			const update = await db.usermeta.updateMeta(userID, metaKey, metaValue);
			return update;
		} else {
			return create;
		}
	},

	setRole: async (userID, newRole) => {
		if (newRole) {
			const userRoles = (await db.users.getUser("user", userID)).userRoles;
			const found = userRoles.find(role => role == newRole);
			return found ? true : userFn.addRole(userID, newRole);
		} else {
			return false;
		}
	},

	addRole: async (userID, newRole) => {
		const userRoles = (await db.users.getUser("user", userID)).userRoles;
		userRoles.push(newRole);
		const newUserRoles = `["${userRoles.join('", "')}"]`;
		return await db.users.updateUser({ userRoles: newUserRoles.toString() }, { userID: userID });
	},

	removeRole: async (userID, role) => {
		const userRoles = (await db.users.getUser("user", userID)).userRoles;
		const removed = userRoles.filter(r => r !== role);
		const newUserRoles = `["${removed.join('", "')}"]`;
		return await db.users.updateUser({ userRoles: newUserRoles.toString() }, { userID: userID });
	}
};

module.exports = userFn;
