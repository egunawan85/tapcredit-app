const passport = require("@config/lib/passport");
const requireLogin = require("@middlewares/requireLogin.js");
const path = require("path");
const db = require("@config/lib/sequelize");
const User = require("../lib/User");
const user = new User();

module.exports = app => {
	app.get("/login", (req, res) => {
		res.sendFile(path.join(__dirname, "../../../", "login.html"));
	});

	app.post(
		"/api/login",
		passport.authenticate("local-signin", {
			failureRedirect: "/api/login/failure",
			failureMessage: true
		}),
		(req, res) => {
			res.send({ authorized: true });
		}
	);

	app.get("/api/login/failure", async (req, res) => {
		res.send({ authorized: false, message: req.session.messages[req.session.messages.length - 1] });
	});

	app.get("/api/logout", (req, res) => {
		req.logout();
		res.redirect("/login/login");
	});

	app.get("/api/current_user", (req, res) => {
		res.status(200).send(req.user);
	});

	app.post("/api/resetPassword", async (req, res) => {
		const found = await user.initialize("email", req.body.email);
		if (found) {
			const data = await user.resetPassword(req);
			res.send({ found: true });
		} else {
			res.send({ found: null, message: "Tidak dapat menemukan Akun TapCredit Anda" });
		}
	});

	app.post("/api/setFirstPassword", async (req, res) => {
		const found = await user.initialize("email", req.body.email);
		if (found) {
			const data = await user.setFirstPassword(req);
			res.send({ found: true });
		} else {
			res.send({ found: null, message: "Tidak dapat menemukan Akun TapCredit Anda" });
		}
	});

	app.get("/api/validToken/:token", async (req, res) => {
		const valid = await User.confirmValidToken(req.params.token);
		res.send({ validToken: valid });
	});

	app.post("/api/setNewPassword/:token", async (req, res) => {
		const result = await User.setNewPassword(req.body.password, req.params.token);
		res.send({ newPassword: result });
	});

	app.get("/api/router", (req, res) => {
		if (req.user) {
			if (
				req.user.userRoles.includes("superAdmin") ||
				req.user.userRoles.includes("admin") ||
				req.user.userRoles.includes("accountOfficer")
			) {
				res.redirect("/admin/dashboard");
			} else if (req.user.userRoles.includes("agent")) {
				res.redirect("/agent/dashboard");
			} else if (req.user.userRoles.includes("client")) {
				res.redirect("/portal/dashboard");
			}
		} else {
			res.redirect("/login/login");
		}
	});
};
