//fetchAllUsers

const passport = require("@config/lib/passport");
const db = require("@config/lib/sequelize");
const ac = require("../policies/accessControl");

const requireLogin = require("@middlewares/requireLogin");
const User = require("../lib/User");
const user = new User();

module.exports = app => {
	app.get("/api/users/read", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).readAny("user")) {
			const results = await db.users.getAllUsers();
			res.send(results);
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.get("/api/user/read/:userID", requireLogin, async (req, res) => {
		const permission =
			req.user.userID === req.params.userID
				? ac.can(req.user.mainRole).readOwn("user")
				: ac.can(req.user.mainRole).readAny("user");

		if (permission.granted) {
			await user.initialize("user", req.params.userID);
			res.send(user);
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.get("/api/user/read/email/:email", requireLogin, async (req, res) => {
		const permission =
			req.user.userID === req.params.userID
				? ac.can(req.user.mainRole).readOwn("user")
				: ac.can(req.user.mainRole).readAny("user");

		if (permission.granted) {
			await user.initialize("email", req.params.email);
			res.send(user);
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.get("/api/user/delete/:userID", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).deleteAny("user")) {
			const remove = await db.users.deleteUser(req.params.userID);
			res.send({ data: remove });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/user/update/:userID/role", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).updateAny("user")) {
			await user.initialize("user", req.params.userID);
			res.send(await user.setRole(req.body.role));
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/user/update/:userID/role/delete", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).updateAny("user")) {
			await user.initialize("user", req.params.userID);
			res.send(await user.removeRole(req.body.role));
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/profile/update/:userID/", async (req, res) => {
		const permission =
			req.user.userID === req.params.userID
				? ac.can(req.user.mainRole).updateOwn("profile")
				: ac.can(req.user.mainRole).updateAny("profile");

		if (permission.granted) {
			res.send(await User.setProfile(req.params.userID, req.body.profile));
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post(
		"/api/user/create",
		requireLogin,
		async (req, res, next) => {
			if (ac.can(req.user.mainRole).createAny("user")) {
				next();
			} else {
				res.status(401).send({ error: "Unauthorized" });
			}
		},
		passport.authenticate("local-signup", {
			failureRedirect: "/api/user/create/failure",
			failureMessage: true
		}),
		async (req, res) => {
			if (req.body.passwordEmail) {
				await user.initialize("email", req.body.username);
				await user.setFirstPassword(req);
			}
			await user.initialize("email", req.body.username);
			res.send(user);
		}
	);

	app.get("/api/user/create/failure", async (req, res) => {
		console.log("here4");
		console.log(req.user);
		res.send({ authorized: false, message: req.session.messages[req.session.messages.length - 1] });
	});
};
