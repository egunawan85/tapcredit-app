//fetch all users
const userFn = require("../src/userFn");
const bCrypt = require("bcrypt-nodejs");
const db = require("@config/lib/sequelize");

class User {
	async initialize(key, id) {
		await this.resetUser();
		const user = await db.users.getUser(key, id);
		return user ? await this.fetchUser(user.userID) : null;
	}

	async refreshUser() {
		await this.resetUser();
		return this.fetchUser(this.userID);
	}

	async resetUser() {
		Object.keys(this).forEach(meta => {
			delete this[meta];
		});
	}

	async fetchUser(userID) {
		const user = await db.users.metas(userID);

		if (user) {
			Object.keys(user).forEach(meta => {
				this[meta] = user[meta];
			});
			return this;
		} else {
			await this.resetUser();
			return "No user found";
		}
	}

	async deleteUser() {
		const result = await db.users.deleteUser(this.userID);
		await this.refreshUser();
		return result;
	}

	async setMeta(metaKey, metaValue) {
		const result = userFn.setMeta(this.userID, metaKey, metaValue);
		await this.refreshUser();
		return result;
	}

	async deleteMeta(metaKey) {
		const result = await db.usermeta.deleteMeta(this.userID, metaKey);
		await this.refreshUser();
		return result;
	}

	async setRole(role) {
		return await userFn.setRole(this.userID, role);
	}

	async removeRole(role) {
		return await userFn.removeRole(this.userID, role);
	}

	async setFirstPassword(req) {
		const token = await userFn.createSetPasswordToken(this.userID);
		const result = await userFn.sendPasswordEmail(this.userID, token, req, "first");
		return result;
	}

	async resetPassword(req) {
		const token = await userFn.createSetPasswordToken(this.userID);
		const result = await userFn.sendPasswordEmail(this.userID, token, req, "reset");
		return result;
	}

	static async setNewPassword(password, token) {
		const result = await userFn.setNewPassword(password, token);
		return result;
	}

	static async confirmValidToken(token) {
		return await userFn.confirmValidToken(token);
	}

	static async setProfile(userID, profile) {
		try {
			const firstname = await userFn.setMeta(userID, "firstName", profile.firstName);
			const lastName = await userFn.setMeta(userID, "lastName", profile.lastName);
			return true;
		} catch (err) {
			return err;
		}
	}
}

module.exports = User;
