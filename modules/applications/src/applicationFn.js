const db = require("@config/lib/sequelize");
const { APPROVAL_STATUS, APPLICATION_STATUS } = require("../models/enum");
const User = require("@modules/users/lib/User");
const user = new User();

const applicationFn = {
	createApplication: async (ownerUserID, applicantUserID, applicationFormID) => {
		const applicationForm = await db.applicationForms.getForm(applicationFormID);
		return applicationForm
			? await db.applications.createApplication(
					ownerUserID,
					applicantUserID,
					applicationFormID,
					`${applicationForm.multiStep ? 0 : 1}`,
					`${applicationForm.requireApproval ? 0 : 1}`
			  )
			: null;
	},

	getApplication: async applicationID => {
		return await db.applications.getApplication(applicationID);
	},

	setApplicationApproval: async (applicationID, approval) => {
		let type;
		switch (approval) {
			case "pending":
				type = APPROVAL_STATUS.PENDING;
				break;
			case "pass":
				type = APPROVAL_STATUS.PASS;
				break;
			case "approved":
				type = APPROVAL_STATUS.APPROVED;
				break;
			default:
				type = null;
		}
		return Number.isInteger(type)
			? await db.applications.updateApplication({ approval: type }, { applicationID: applicationID })
			: null;
	},

	setApplicationStatus: async (applicationID, status) => {
		let type;
		switch (status) {
			case "pending":
				type = APPLICATION_STATUS.PENDING;
				break;
			case "completed":
				type = APPLICATION_STATUS.COMPLETED;
				break;
			default:
				type = null;
		}
		return Number.isInteger(type)
			? await db.applications.updateApplication({ status: type }, { applicationID: applicationID })
			: null;
	},

	setApplicationField: async (applicationID, fieldKey, fieldValue) => {
		const create = await db.applicationFields.createField(applicationID, fieldKey, fieldValue);
		if (create.errors) {
			const update = await db.applicationFields.updateField(applicationID, fieldKey, fieldValue);
			return update;
		} else {
			return create;
		}
	},

	submitApplication: async (metas, fields) => {
		const application = !metas.applicationID
			? await applicationFn.createApplication(metas.ownerUserID, metas.applicantUserID, metas.applicationFormID)
			: await applicationFn.getApplication(metas.applicationID);
		if (application.status == APPLICATION_STATUS.PENDING) {
			for (const field of fields) {
				await applicationFn.setApplicationField(application.applicationID, field.fieldKey, field.fieldValue);
			}

			await db.applications.updateApplication({ step: metas.step }, { applicationID: application.applicationID });
			await applicationFn.setApplicationStatus(application.applicationID, metas.status);

			await user.initialize("user", metas.applicantUserID);

			for (const field of fields) {
				if (field.usermeta) await user.setMeta(field.fieldKey, field.fieldValue);
			}
		} else {
			return { error: "Application already submitted" };
		}

		return application;
	}
};

module.exports = applicationFn;
