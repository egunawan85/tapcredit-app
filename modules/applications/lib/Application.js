const db = require("@config/lib/sequelize");
const applicationFn = require("../src/applicationFn");

class Application {
	async initialize(applicationID) {
		await this.resetApplication();
		return await this.fetchApplication(applicationID);
	}

	async fetchApplication(applicationID) {
		const application = await applicationFn.getApplication(applicationID);
		if (application) {
			Object.keys(application).forEach(field => {
				this[field] = application[field];
			});
			return this;
		} else {
			Object.keys(this).forEach(field => {
				delete this[field];
			});
			return "No application found";
		}
	}

	async refreshApplication() {
		await this.resetApplication();
		return await this.fetchApplication(this.applicationID);
	}

	async resetApplication() {
		Object.keys(this).forEach(field => {
			delete this[field];
		});
	}

	async updateApplication(state) {
		const result = await db.applications.updateApplication(state, { applicationID: this.applicationID });
		await this.refreshApplication();
		return result;
	}

	async setApplicationApproval(approval) {
		const result = await applicationFn.setApplicationApproval(this.applicationID, approval);
		await this.refreshApplication();
		return result;
	}

	async setApplicationStatus(status) {
		const result = await applicationFn.setApplicationStatus(this.applicationID, status);
		await this.refreshApplication();
		return result;
	}

	async deleteApplication() {
		const result = await db.applications.deleteApplication(this.applicationID);
		await this.refreshApplication();
		return result;
	}

	async setField(fieldKey, fieldValue) {
		const result = await applicationFn.setApplicationField(this.applicationID, fieldKey, fieldValue);
		await this.refreshApplication();
		return result;
	}

	static async createApplication(ownerUserID, applicantUserID, applicationFormID) {
		return await applicationFn.createApplication(ownerUserID, applicantUserID, applicationFormID);
	}

	static async submitApplication(metas, fields) {
		return await applicationFn.submitApplication(metas, fields);
	}

	static async fetchApplicationList(conditions = null) {
		return await db.applications.getApplicationList(conditions);
	}
}

module.exports = Application;
