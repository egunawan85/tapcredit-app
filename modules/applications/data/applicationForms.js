module.exports = [
	{
		applicationFormID: 1,
		path: "/registration/team",
		title: "Create Team Member",
		description: "Create a new user for internal team member",
		multiStep: true,
		requireApproval: false
	},
	{
		applicationFormID: 2,
		path: "/registration/agent",
		title: "Register New Agent",
		description: "Create a new agent account",
		multiStep: true,
		requireApproval: true
	},
	{
		applicationFormID: 3,
		path: "/registration/customer",
		title: "Apply New Customer",
		description: "Create new customer account and apply for installment loan",
		multiStep: true,
		requireApproval: true
	}
];
