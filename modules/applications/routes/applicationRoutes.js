//get all applications
//get owned applications

const db = require("@config/lib/sequelize");
const requireLogin = require("@middlewares/requireLogin.js");
const Application = require("../lib/Application");
const { APPLICATION_STATUS, APPROVAL_STATUS } = require("../models/enum");
const application = new Application();
const axios = require("axios");
const ac = require("../policies/accessControl");

const moment = require("moment-timezone");

module.exports = app => {
	app.get("/api/applications/dev", async (req, res) => {
		const results = {};
		res.send({ data: results });
	});

	app.post("/api/applications/create", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).createAny("application")) {
			const application = await Application.createApplication(
				req.body.ownerUserID,
				req.body.applicantUserID,
				req.body.applicationFormID
			);
			application ? res.send(application) : res.send({ error: "Error when creating application" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/submit", requireLogin, async (req, res) => {
		if (ac.can(req.user.mainRole).createAny("application")) {
			const newApplication = await Application.submitApplication(req.body.metas, req.body.fields);
			await application.initialize(newApplication.applicationID);
			console.log(application);
			application ? res.send(application) : res.send({ error: "Error when submitting application" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/read/", requireLogin, async (req, res) => {
		const permission = req.body.ownedApplications
			? ac.can(req.user.mainRole).readOwn("application")
			: ac.can(req.user.mainRole).readAny("application");

		if (permission.granted) {
			const applicationList = req.body.ownedApplications
				? await Application.fetchApplicationList({ ownerUserID: req.user.userID, ...req.body.conditions })
				: await Application.fetchApplicationList(req.body.conditions);
			res.send(applicationList);
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.get("/api/applications/read/:applicationID", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID || req.user.userID === application.applicantUserID
				? ac.can(req.user.mainRole).readOwn("application")
				: ac.can(req.user.mainRole).readAny("application");

		if (permission.granted) {
			console.log(application);

			res.send(application);
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/update/:applicationID/fields", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID
				? ac.can(req.user.mainRole).updateOwn("application")
				: ac.can(req.user.mainRole).updateAny("application");

		if (permission.granted) {
			const setField = await application.setField(req.body.fieldKey, req.body.fieldValue);
			setField ? res.send(application) : res.send({ error: "Set application field failed" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/update/:applicationID/step", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID
				? ac.can(req.user.mainRole).updateOwn("application")
				: ac.can(req.user.mainRole).updateAny("application");

		if (permission.granted) {
			const update = await application.updateApplication({ step: req.body.step });
			update ? res.send(application) : res.send({ error: "Update failed" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/update/:applicationID/status", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID
				? ac.can(req.user.mainRole).updateOwn("application")
				: ac.can(req.user.mainRole).updateAny("application");

		if (permission.granted) {
			const update = await application.setApplicationStatus(req.body.status);
			update ? res.send(application) : res.send({ error: "Update failed" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.post("/api/applications/update/:applicationID/approval", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID
				? ac.can(req.user.mainRole).updateOwn("application")
				: ac.can(req.user.mainRole).updateAny("application");

		console.log(req.body.approval);
		if (permission.granted) {
			const update = await application.setApplicationApproval(req.body.approval);
			update ? res.send(application) : res.send({ error: "Update failed" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});

	app.get("/api/applications/delete/:applicationID", requireLogin, async (req, res) => {
		await application.initialize(req.params.applicationID);
		const permission =
			req.user.userID === application.ownerUserID
				? ac.can(req.user.mainRole).deleteOwn("application")
				: ac.can(req.user.mainRole).deleteAny("application");

		if (permission.granted) {
			const destroy = await application.deleteApplication();
			destroy ? res.send(application) : res.send({ error: "Delete failed" });
		} else {
			res.status(401).send({ error: "Unauthorized" });
		}
	});
};
