module.exports.APPLICATION_STATUS = {
	PENDING: 0,
	COMPLETED: 1
};
module.exports.APPROVAL_STATUS = {
	PENDING: 0,
	PASS: 1,
	APPROVED: 2
};
