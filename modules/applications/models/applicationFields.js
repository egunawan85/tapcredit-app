module.exports = (sequelize, Sequelize) => {
	const ApplicationFields = sequelize.define(
		"applicationFields",
		{
			applicationFieldID: {
				type: Sequelize.BIGINT(20),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			applicationID: {
				type: Sequelize.BIGINT(20),
				allowNull: false
			},
			fieldKey: {
				type: Sequelize.STRING(255),
				allowNull: false
			},
			fieldValue: {
				type: Sequelize.TEXT("long"),
				allowNull: true
			}
		},
		{
			paranoid: true,
			indexes: [{ unique: true, fields: ["applicationID", "fieldKey"] }]
		}
	);

	ApplicationFields.getFields = async applicationID => {
		return await sequelize.query(`SELECT * FROM applicationFields WHERE applicationID = ${applicationID}`, {
			type: sequelize.QueryTypes.SELECT
		});
	};

	ApplicationFields.createField = async (applicationID, fieldKey, fieldValue) => {
		try {
			return await ApplicationFields.create({
				applicationID: applicationID,
				fieldKey: fieldKey,
				fieldValue: fieldValue
			});
		} catch (e) {
			return e;
		}
	};

	ApplicationFields.updateField = async (applicationID, fieldKey, fieldValue) => {
		return await ApplicationFields.update(
			{ fieldValue: fieldValue },
			{
				where: { applicationID: applicationID, fieldKey: fieldKey }
			}
		);
	};

	return ApplicationFields;
};
