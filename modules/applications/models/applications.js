module.exports = (sequelize, Sequelize) => {
	const Applications = sequelize.define(
		"applications",
		{
			applicationID: {
				type: Sequelize.BIGINT(20),
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			ownerUserID: {
				type: Sequelize.BIGINT(20),
				allowNull: false
			},
			applicantUserID: {
				type: Sequelize.BIGINT(20),
				allowNull: false
			},
			applicationFormID: {
				type: Sequelize.BIGINT(20),
				allowNull: false
			},
			step: {
				type: Sequelize.TINYINT,
				allowNull: false
			},
			status: {
				type: Sequelize.TINYINT,
				allowNull: false
			},
			approval: {
				type: Sequelize.TINYINT,
				allowNull: true
			}
		},
		{
			paranoid: true
		}
	);

	Applications.createApplication = async (ownerUserID, applicantUserID, applicationFormID, status, approval) => {
		return await Applications.create({
			ownerUserID: ownerUserID,
			applicantUserID: applicantUserID,
			applicationFormID: applicationFormID,
			step: 0,
			status: status,
			approval: approval
		});
	};

	Applications.getApplication = async applicationID => {
		let application = await Applications.findOne({
			where: {
				applicationID: applicationID
			},
			raw: true
		});
		return application ? await Applications.getFields(application) : null;
	};

	Applications.getApplicationList = async (conditions = null) => {
		const options = conditions
			? {
					where: conditions,
					raw: true,
					order: [["updatedAt", "DESC"]]
			  }
			: { raw: true, order: [["updatedAt", "DESC"]] };
		let applications = await Applications.findAll(options);
		const applicationList = [];
		if (applications) {
			for (let application of applications) {
				applicationList.push(await Applications.getFields(application));
			}
			return applicationList;
		} else {
			console.log("No applications found");
			return null;
		}
	};

	Applications.getFields = async application => {
		const results = await sequelize.query(
			`SELECT * FROM applicationFields WHERE applicationID = ${application.applicationID}`,
			{
				type: sequelize.QueryTypes.SELECT
			}
		);
		for (let fields of results) {
			application = { ...application, [fields.fieldKey]: fields.fieldValue };
		}
		return application;
	};

	Applications.updateApplication = async (state, conditions) => {
		return await Applications.update(state, {
			where: conditions
		});
	};

	Applications.deleteApplication = async applicationID => {
		try {
			const results = await Applications.destroy({
				where: { applicationID: applicationID }
			});
			return results;
		} catch (e) {
			return "deletion error";
		}
	};

	return Applications;
};
