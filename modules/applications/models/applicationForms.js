const applicationForms = require("../data/applicationForms");

module.exports = (sequelize, Sequelize) => {
	const ApplicationForms = sequelize.define("applicationForms", {
		applicationFormID: {
			type: Sequelize.BIGINT(20),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		path: {
			type: Sequelize.STRING(255),
			allowNull: false,
			unique: "compositeIndex"
		},
		title: {
			type: Sequelize.STRING(255),
			allowNull: false
		},
		description: {
			type: Sequelize.TEXT("long"),
			allowNull: true
		},
		multiStep: {
			type: Sequelize.BOOLEAN,
			allowNull: false
		},
		requireApproval: {
			type: Sequelize.BOOLEAN,
			allowNull: false
		}
	});

	ApplicationForms.getForm = async applicationFormID => {
		return await ApplicationForms.findOne({
			where: { applicationFormID: applicationFormID }
		});
	};

	ApplicationForms.initialData = async (key, id) => {
		applicationForms.forEach(applicationForm => {
			ApplicationForms.create(applicationForm)
				.then(result => {
					console.log("Application form data inserted succesfully.");
				})
				.catch(err => {
					console.error("error inserting data");
				});
		});
	};

	return ApplicationForms;
};
