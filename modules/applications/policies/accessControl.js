const AccessControl = require("accesscontrol");
const ac = new AccessControl();

ac.grant("client").readOwn("application");
ac.grant("agent")
	.extend("client")
	.createAny("application")
	.updateOwn("application")
	.deleteOwn("application");
ac.grant("accountOfficer")
	.extend("agent")
	.readAny("application");
ac.grant("admin")
	.extend("accountOfficer")
	.updateAny("application")
	.deleteAny("application");
ac.grant("superAdmin").extend("admin");

module.exports = ac;
