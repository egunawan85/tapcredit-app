const { utils } = require("@src/utils");
const db = require("@config/lib/sequelize");
const citizen = require("../lib/citizen");
const citizenFn = require("../src/citizenFn");
const Torque = require("@modules/torque/lib/Torque");
const tapUtils = require("@modules/torque/src/tapUtils");

module.exports = app => {
	app.get("/api/accounts", async (req, res) => {
		const results = await citizen.accounts(req.user.userID);
		res.send(results);
	});

	app.get("/api/accounts/:accountID/verifyAccount", async (req, res) => {
		const found = await citizen.verifyAccount(req.user.userID, req.params.accountID);
		if (found) {
			res.send(true);
		} else {
			res.send(false);
		}
	});

	app.get("/api/accounts/:accountID/accountDetails", async (req, res) => {
		const found = await citizen.verifyAccount(req.user.userID, req.params.accountID);
		if (found) {
			const paymentSchedule = await Torque.paymentSchedule(req.params.accountID, true);
			res.send({
				availableCredit: await citizen.availableCredit(req.params.accountID),
				details: (await db.accounts.getAccounts("account", req.params.accountID))[0],
				balanceOutstanding: await Torque.balanceOutstanding(req.params.accountID),
				paymentSchedule: paymentSchedule,
				nextStatementDate: await tapUtils.nextStatementDate(
					paymentSchedule[Object.keys(paymentSchedule)[0]].dateDue
				),
				nextPaymentDue: await Torque.nextPaymentDue(req.params.accountID),
				minimumDue: await Torque.minimumDue(
					req.params.accountID,
					await Torque.nextPaymentDue(req.params.accountID)
				),
				availablePlans: await citizen.availablePlans(req.params.accountID)
			});
		} else {
			res.send("Not Authorized");
		}
	});
};
