const { CREDIT_LINE_STATUS, CREDIT_LINE_TYPE } = require("./enum");
const { utils } = require("@src/utils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const CreditLines = sequelize.define(
    "creditLines",
    {
      creditLineID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      planID: {
        type: Sequelize.JSON,
        allowNull: true
      },
      type: {
        type: Sequelize.TINYINT,
        allowNull: false
      },
      amount: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      expiryDate: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID"] }]
    }
  );

  CreditLines.createLine = async (accountID, amount, expiryDate, type, planID) => {
    const results = await CreditLines.create({
      accountID: accountID,
      type: type,
      amount: amount,
      expiryDate: expiryDate,
      status: CREDIT_LINE_STATUS.OPEN,
      planID: planID
    });
    return results;
  };

  CreditLines.getLine = async (key, id) => {
    let type;
    switch (key) {
      case "account":
        type = "accountID";
        break;
      case "creditLine":
        type = "creditLineID";
        break;
    }

    const results = await CreditLines.findAll({
      where: {
        [type]: id
      }
    });
    return results;
  };

  return CreditLines;
};
