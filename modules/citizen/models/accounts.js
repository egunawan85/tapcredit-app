const { ACCOUNT_STATUS } = require("./enum");
const { utils } = require("@src/utils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Accounts = sequelize.define(
    "accounts",
    {
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      userID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      currency: {
        type: Sequelize.TINYINT,
        allowNull: false
      },
      platform: {
        type: Sequelize.BIGINT(20),
        allowNull: true
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["userID"] }]
    }
  );

  Accounts.createAccount = async (userID, currency, platform) => {
    const results = await Accounts.create({
      userID: userID,
      currency: currency,
      platform: platform,
      status: ACCOUNT_STATUS.OPEN
    });
    return results;
  };

  Accounts.getAccounts = async (key, id) => {
    let type;
    switch (key) {
      case "user":
        type = "userID";
        break;
      case "account":
        type = "accountID";
        break;
      case "platform":
        type = "platform";
        break;
    }
    const result = await Accounts.findAll({
      attributes: {
        include: [
          [sequelize.literal("creditLines.creditLineID"), "creditLineID"],
          [sequelize.literal("creditLines.type"), "type"],
          [sequelize.literal("creditLines.amount"), "amount"],
          [sequelize.literal("creditLines.expiryDate"), "expiryDate"],
          [sequelize.literal("creditLines.planID"), "planID"]
        ]
      },
      include: [
        {
          model: sequelize.models.creditLines,
          attributes: [],
          required: false
        }
      ],
      where: {
        [type]: id
      }
    });
    return result;
  };

  return Accounts;
};
