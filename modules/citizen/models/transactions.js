const { utils } = require("@src/utils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Transactions = sequelize.define(
    "transactions",
    {
      transactionID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      orderID: {
        type: Sequelize.STRING(50),
        allowNull: true
      },
      transactionDesc: {
        type: Sequelize.STRING(200),
        allowNull: true
      },
      amount: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID"] }]
    }
  );

  Transactions.createTransaction = async (accountID, orderID, transactionDesc, amount, status) => {
    const results = await CreditLines.create({
      accountID: accountID,
      orderID: orderID,
      transactionDesc: transactionDesc,
      amount: amount,
      status: status
    });
    return results;
  };

  return Transactions;
};
