const db = require("@config/lib/sequelize");
const citizenFn = require("../src/citizenFn");

const citizen = {
	accounts: async userID => {
		return await db.accounts.getAccounts("user", userID);
	},

	availableCredit: async accountID => {
		return await citizenFn.checkAvailableCredit(accountID);
	},

	verifyAccount: async (userID, accountID) => {
		return await citizenFn.verifyAccount(userID, accountID);
	},

	availablePlans: async accountID => {
		return await citizenFn.getAvailablePlans(accountID);
	}
};

module.exports = citizen;
