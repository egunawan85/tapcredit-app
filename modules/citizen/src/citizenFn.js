const db = require("@config/lib/sequelize");
const { utils, now } = require("@src/utils");
const Torque = require("@modules/torque/lib/Torque");

const citizenFn = {
	addNewAccount: async (userID, currency, platform, amount, type, planID, expiryDate = null) => {
		const account = await db.accounts.createAccount(userID, currency, platform);
		const creditLine = await db.creditLines.createLine(
			account.accountID,
			amount,
			expiryDate,
			type,
			planID
		);
		return [account, creditLine];
	},

	checkAvailableCredit: async accountID => {
		const creditLine = (await db.creditLines.getLine("account", accountID))[0];
		const balanceOutstanding = await Torque.balanceOutstanding(accountID);
		return creditLine.amount - balanceOutstanding;
	},

	verifyAccount: async (userID, accountID) => {
		const accounts = await db.accounts.getAccounts("user", userID);
		const found = accounts.filter(obj => {
			return obj.accountID == accountID;
		}).length;
		return found ? true : false;
	},

	getAvailablePlans: async accountID => {
		const account = (await db.accounts.getAccounts("account", accountID))[0];

		const plans = account.dataValues.planID.map(async plan => {
			return await db.paymentPlans.getPlan(plan);
		});

		return await Promise.all(plans);
	}
};

module.exports = citizenFn;
