const { PAYMENT_STATUS } = require("./enum");
const { utils } = require("@src/utils");
const tapUtils = require("../src/tapUtils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Payments = sequelize.define(
    "payments",
    {
      paymentID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      transactionTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      grossAmount: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      paymentType: {
        type: Sequelize.TINYINT,
        allowNull: false
      },
      orderID: {
        type: Sequelize.STRING(30),
        allowNull: false,
        primaryKey: true
      },
      transactionID: {
        type: Sequelize.STRING(30),
        allowNull: false,
        primaryKey: true
      },
      clearedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        get: function() {
          return this.getDataValue("clearedAt") ? moment(this.getDataValue("clearedAt")).format("YYYY-MM-DD") : null;
        }
      },
      transactionStatus: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID"] }]
    }
  );

  Payments.createPayment = async (accountID, grossAmount, transactionTime, orderID, paymentType, transactionID) => {
    const results = await Payments.create({
      accountID: accountID,
      grossAmount: grossAmount,
      transactionTime: transactionTime,
      orderID: orderID,
      paymentType: paymentType,
      transactionID: transactionID,
      dateCleared: null,
      transactionStatus: PAYMENT_STATUS.SUCCESS
    });
    return results;
  };

  Payments.setStatus = async (paymentID, status) => {
    const results = await Payments.update(
      {
        transactionStatus: status
      },
      {
        where: {
          paymentID: paymentID
        }
      }
    );
    return results;
  };

  Payments.getPayments = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "status":
        type = "transactionStatus";
        break;
    }
    const result =
      notEqual == false
        ? await Payments.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await Payments.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  Payments.byRange = async (key, accountID, startDate, endDate, status = null) => {
    let type;
    switch (key) {
      case "transaction":
        type = "transactionTime";
        break;
      case "created":
        type = "createdAt";
        break;
    }
    let a, conditions;
    a = {
      accountID: accountID,
      [type]: {
        [Op.between]: [startDate, endDate]
      }
    };
    conditions = status != null ? { ...a, ...{ status: status } } : a;
    const results = await Payments.findAll({
      where: conditions
    });
    return results;
  };

  Payments.getUncleared = async accountID => {
    const results = await Payments.findAll({
      where: {
        accountID: accountID,
        transactionStatus: {
          [Op.ne]: PAYMENT_STATUS.CLEARED
        }
      }
    });
    return results;
  };

  Payments.getHistUncleared = async (accountID, date) => {
    const ledger = await sequelize.models.ledger.getLedgerPeriod(accountID, date);
    let transactionTime = {
      $lte: moment(date).endOf("day")
    };
    transactionTime = ledger.startDate
      ? {
          ...transactionTime,
          ...{
            $gt: moment(ledger.startDate).endOf("day")
          }
        }
      : transactionTime;
    const results = await Payments.findAll({
      where: {
        accountID: accountID,
        transactionTime: transactionTime
      }
    });
    return results;
  };

  Payments.setCleared = async (paymentID, dateTime) => {
    const results = await Payments.update(
      {
        clearedAt: dateTime,
        transactionStatus: PAYMENT_STATUS.CLEARED
      },
      {
        where: {
          paymentID: paymentID
        }
      }
    );
    return results;
  };

  return Payments;
};
