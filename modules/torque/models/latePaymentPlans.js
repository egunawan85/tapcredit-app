module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;

  const LatePaymentPlans = sequelize.define(
    "latePaymentPlans",
    {
      latePaymentID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      fee: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      outstFrom: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      outstTo: {
        type: Sequelize.BIGINT,
        allowNull: false
      }
    },
    {
      paranoid: true
    }
  );

  LatePaymentPlans.initialData = () => {
    const plans = [
      {
        fee: 100000,
        outstFrom: 1,
        outstTo: 5000000
      },
      {
        fee: 350000,
        outstFrom: 5000001,
        outstTo: 50000000
      },
      {
        fee: 1000000,
        outstFrom: 50000001,
        outstTo: 10000000000
      }
    ];
    plans.forEach(plan => {
      LatePaymentPlans.create(plan);
    });
  };

  LatePaymentPlans.calcFee = async balanceOutstanding => {
    const results = await LatePaymentPlans.findOne({
      attributes: [["fee", "lateFee"]],
      where: {
        outstFrom: {
          [Op.lte]: balanceOutstanding
        },
        outstTo: {
          [Op.gte]: balanceOutstanding
        }
      },
      raw: true
    });
    return results;
  };
  return LatePaymentPlans;
};
