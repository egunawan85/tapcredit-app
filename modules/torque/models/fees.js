const { FEE_STATUS } = require("./enum");
const tapUtils = require("../src/tapUtils");
const { utils } = require("@src/utils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Fees = sequelize.define(
    "fees",
    {
      feeID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      dateDue: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      feeDue: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      feeType: {
        type: Sequelize.TINYINT,
        allowNull: false
      },
      paidAt: {
        type: Sequelize.DATE,
        allowNull: true,
        get: function() {
          return this.getDataValue("paidAt") ? moment(this.getDataValue("paidAt")).format("YYYY-MM-DD") : null;
        }
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID"] }]
    }
  );

  Fees.createFee = async (accountID, dateDue, feeDue, feeType, status) => {
    const results = await Fees.create({
      accountID: accountID,
      dateDue: dateDue,
      feeDue: feeDue,
      feeType: feeType,
      status: FEE_STATUS.OPEN
    });
    return results;
  };

  Fees.setStatus = async (feeID, status) => {
    const results = await Fees.update(
      {
        status: status
      },
      {
        where: {
          feeID: feeID
        }
      }
    );
    return results;
  };

  Fees.getFees = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "type":
        type = "feeType";
        break;
      case "status":
        type = "status";
        break;
    }
    const result =
      notEqual == false
        ? await Fees.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await Fees.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  Fees.byRange = async (key, accountID, startDate, endDate, status = null) => {
    let type;
    switch (key) {
      case "due":
        type = "dateDue";
        break;
      case "paid":
        type = "paidAt";
        break;
      case "created":
        type = "createdAt";
        break;
    }
    let a, conditions;
    a = {
      accountID: accountID,
      [type]: {
        [Op.between]: [startDate, endDate]
      }
    };
    conditions = status != null ? { ...a, ...{ status: status } } : a;
    const results = await Fees.findAll({
      where: conditions
    });
    return results;
  };

  Fees.getUncleared = async accountID => {
    const results = await Fees.findAll({
      order: [["dateDue", "ASC"]],
      where: {
        accountID: accountID,
        status: {
          [Op.ne]: FEE_STATUS.CLEARED
        }
      }
    });
    return results;
  };

  Fees.getHistUncleared = async (accountID, date) => {
    const ledger = await sequelize.models.ledger.getLedgerPeriod(accountID, date);
    let paidAt = ledger.startDate
      ? {
          $gt: moment(ledger.startDate).endOf("day")
        }
      : {};
    const results = await Fees.findAll({
      order: [["dateDue", "ASC"]],
      where: {
        accountID: accountID,
        createdAt: {
          $lte: moment(date).endOf("day")
        },
        [Op.or]: [{ paidAt: paidAt }, { paidAt: null }]
      }
    });
    return results;
  };

  Fees.setPaid = async (feeID, dateTime) => {
    const results = await Fees.update(
      {
        paidAt: dateTime,
        status: FEE_STATUS.COMPLETED
      },
      {
        where: {
          feeID: feeID
        }
      }
    );
    return results;
  };

  return Fees;
};
