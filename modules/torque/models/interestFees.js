const { INTEREST_FEE_STATUS } = require("./enum");
const tapUtils = require("../src/tapUtils");
const { utils } = require("@src/utils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const InterestFees = sequelize.define(
    "interestFees",
    {
      interestFeeID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      loanID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      dateDue: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      interestDue: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      paidAt: {
        type: Sequelize.DATE,
        allowNull: true,
        get: function() {
          return this.getDataValue("paidAt")
            ? moment(this.getDataValue("paidAt")).format("YYYY-MM-DD")
            : null;
        }
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID", "loanID"] }]
    }
  );

  InterestFees.createInterestFee = async (accountID, loanID) => {
    const loan = await sequelize.models.loans.getLoans(accountID, "loan", loanID);
    const currentInterestFee = await InterestFees.getCurrentInterestFee(loanID);
    const currentPlan = await sequelize.models.paymentPlans.getCurrentPlan(
      loan[0].planID,
      currentInterestFee
    );
    if (currentPlan) {
      const results = await InterestFees.create({
        accountID: accountID,
        loanID: loanID,
        dateDue: loan[0].nPaymentDue,
        interestDue: loan[0].amountMoney * currentPlan.interestRate,
        status: INTEREST_FEE_STATUS.OPEN
      });
      return results;
    } else {
      return [];
    }
  };

  InterestFees.setStatus = async (interestFeeID, status) => {
    const results = await InterestFees.update(
      {
        status: status
      },
      {
        where: {
          interestFeeID: interestFeeID
        }
      }
    );
    return results;
  };

  InterestFees.getInterestFees = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "loan":
        type = "loanID";
        break;
      case "status":
        type = "status";
        break;
    }
    const result =
      notEqual == false
        ? await InterestFees.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await InterestFees.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  InterestFees.byRange = async (key, accountID, startDate, endDate, status = null) => {
    let type;
    switch (key) {
      case "due":
        type = "dateDue";
        break;
      case "paid":
        type = "paidAt";
        break;
      case "created":
        type = "createdAt";
        break;
    }
    let a, conditions;
    a = {
      accountID: accountID,
      [type]: {
        [Op.between]: [startDate, endDate]
      }
    };
    conditions = status != null ? { ...a, ...{ status: status } } : a;
    const results = await InterestFees.findAll({
      where: conditions
    });
    return results;
  };

  InterestFees.getCurrentInterestFee = async loanID => {
    const results = await InterestFees.findAll({
      attributes: [[sequelize.literal("COUNT(*)+1"), "currentInterestFee"]],
      where: {
        loanID: loanID
      },
      raw: true
    });
    return results[0].currentInterestFee;
  };

  InterestFees.getUncleared = async accountID => {
    const results = await InterestFees.findAll({
      attributes: {
        include: [[sequelize.literal("loan.loanDate"), "loanDate"]]
      },
      include: [
        {
          model: sequelize.models.loans
        }
      ],
      order: [["dateDue", "ASC"], [sequelize.literal("loan.loanDate"), "ASC"]],
      where: {
        accountID: accountID,
        status: {
          [Op.ne]: INTEREST_FEE_STATUS.CLEARED
        }
      }
    });
    return results;
  };

  InterestFees.getHistUncleared = async (accountID, date) => {
    const ledger = await sequelize.models.ledger.getLedgerPeriod(accountID, date);
    let paidAt = ledger.startDate
      ? {
          $gt: moment(ledger.startDate).endOf("day")
        }
      : {};
    const results = await InterestFees.findAll({
      attributes: {
        include: [[sequelize.literal("loan.loanDate"), "loanDate"]]
      },
      include: [
        {
          model: sequelize.models.loans
        }
      ],
      order: [["dateDue", "ASC"], [sequelize.literal("loan.loanDate"), "ASC"]],
      where: {
        accountID: accountID,
        createdAt: {
          $lte: moment(date).endOf("day")
        },
        [Op.or]: [{ paidAt: paidAt }, { paidAt: null }]
      }
    });
    return results;
  };

  InterestFees.setPaid = async (interestFeeID, dateTime) => {
    const results = await InterestFees.update(
      {
        paidAt: dateTime,
        status: INTEREST_FEE_STATUS.COMPLETED
      },
      {
        where: {
          interestFeeID: interestFeeID
        }
      }
    );
    return results;
  };

  return InterestFees;
};
