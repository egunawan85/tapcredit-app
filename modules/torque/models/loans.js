const { utils } = require("@src/utils");
const { LOAN_STATUS } = require("../models/enum");
const tapUtils = require("../src/tapUtils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Loans = sequelize.define(
    "loans",
    {
      loanID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      planID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      amountMoney: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      loanDate: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      paidAt: {
        type: Sequelize.DATE,
        allowNull: true,
        get: function() {
          return this.getDataValue("paidAt")
            ? moment(this.getDataValue("paidAt")).format("YYYY-MM-DD")
            : null;
        }
      },
      nPaymentDue: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID", "planID"] }]
    }
  );

  Loans.createLoan = async (accountID, planID, amountMoney) => {
    const results = await Loans.create({
      accountID: accountID,
      planID: planID,
      amountMoney: amountMoney,
      loanDate: utils.currentDate(),
      nPaymentDue: tapUtils.nextPaymentDue(),
      status: LOAN_STATUS.OPEN
    });
    return results;
  };

  Loans.setStatus = async (loanID, status) => {
    const results = await Loans.update(
      {
        status: status
      },
      {
        where: {
          loanID: loanID
        }
      }
    );
    return results;
  };

  Loans.getLoans = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "loan":
        type = "loanID";
        break;
      case "status":
        type = "status";
        break;
    }
    const result =
      notEqual == false
        ? await Loans.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await Loans.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  Loans.byRange = async (key, accountID, startDate, endDate, status = null) => {
    let type;
    switch (key) {
      case "loan":
        type = "loanDate";
        break;
      case "paid":
        type = "paidAt";
        break;
      case "created":
        type = "createdAt";
        break;
    }
    let a, conditions;
    a = {
      accountID: accountID,
      loanDate: {
        [Op.between]: [startDate, endDate]
      }
    };
    conditions = status != null ? { ...a, ...{ status: status } } : a;
    const results = await Loans.findAll({
      where: conditions
    });
    return results;
  };

  Loans.getPrincipalOutstanding = async accountID => {
    const results = await Loans.findAll({
      attributes: {
        include: [
          [
            sequelize.literal(
              "CAST(amountMoney-IFNULL(SUM(repayments.principalDue),0)-IFNULL(SUM(overpayments.amount),0) AS SIGNED)"
            ),
            "principalOutstanding"
          ]
        ]
      },
      include: [
        {
          model: sequelize.models.repayments,
          attributes: [],
          required: false
        },
        {
          model: sequelize.models.overpayments,
          attributes: [],
          required: false
        }
      ],
      group: "loanID",
      where: {
        accountID: accountID,
        status: {
          $ne: LOAN_STATUS.CLEARED
        }
      },
      order: [["loanDate", "ASC"]]
    });
    return results;
  };

  Loans.getHistPrincipalOutstanding = async (accountID, date) => {
    const ledger = await sequelize.models.ledger.getLedgerPeriod(accountID, date);
    let paidAt = ledger.startDate
      ? {
          $gt: moment(ledger.startDate).endOf("day")
        }
      : {};
    const results = await Loans.findAll({
      attributes: {
        include: [
          [
            sequelize.literal(
              "CAST(amountMoney-IFNULL(SUM(repayments.principalDue),0)-IFNULL(SUM(overpayments.amount),0) AS SIGNED)"
            ),
            "principalOutstanding"
          ]
        ]
      },
      include: [
        {
          model: sequelize.models.repayments,
          attributes: [],
          where: {
            createdAt: {
              $lte: moment(date).endOf("day")
            }
          },
          required: false
        },
        {
          model: sequelize.models.overpayments,
          attributes: [],
          where: {
            createdAt: {
              $lt: moment(date).endOf("day")
            }
          },
          required: false
        }
      ],
      group: "loanID",
      where: {
        accountID: accountID,
        createdAt: {
          $lte: moment(date).endOf("day")
        },
        [Op.or]: [{ paidAt: paidAt }, { paidAt: null }]
      },
      order: [["loanDate", "ASC"]]
    });
    return results;
  };

  Loans.setPaid = async (loanID, dateTime) => {
    const results = await Loans.update(
      {
        paidAt: dateTime,
        status: LOAN_STATUS.COMPLETED
      },
      {
        where: {
          loanID: loanID
        }
      }
    );
    return results;
  };

  return Loans;
};
