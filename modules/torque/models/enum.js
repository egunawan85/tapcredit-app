module.exports.LOAN_STATUS = {
    OPEN: 1,
    INPROGRESS: 1,
    COMPLETED: 2,
    CLEARED: 3
};
module.exports.REPAYMENT_STATUS = {
    OPEN: 0,
    COMPLETED: 1,
    CLEARED: 2
};
module.exports.INTEREST_FEE_STATUS = {
    OPEN: 0,
    COMPLETED: 1,
    CLEARED: 2
};
module.exports.FEE_STATUS = {
    OPEN: 0,
    COMPLETED: 1,
    CLEARED: 2
};
module.exports.FEE_TYPE = {
    LATE: 0,
    REGISTRATION: 1
};
module.exports.PAYMENT_TYPE = {
    CASH: 0,
    TRANSFER: 1
};
module.exports.PAYMENT_STATUS = {
    SUCCESS: 0,
    FAILED: 1,
    COMPLETED: 2,
    CLEARED: 3
};
module.exports.BANK_CODE = {
    KLIKBCA_BUSINESS: 1,
    KLIKBCA_INDIVIDUAL: 2,
    MANDIRI_ONLINE: 3
};
module.exports.SERVICE_CODE = {
    BRIDESTORY: 1
};
module.exports.OVERPAYMENT_STATUS = {
    OPEN: 0,
    COMPLETED: 1,
    CLEARED: 2
};
module.exports.ITEM_TYPE = {
    REPAYMENT: 0,
    INTERESTFEE: 1,
    FEE: 2,
    PAYMENT: 3,
    CLEARING: 4,
    OVERPAYMENT: 5
};
module.exports.GL_DETAIL_STATUS = {
    OPEN: 0,
    COMPLETED: 1
};
module.exports.APPLICATION_STATUS = {
    OPEN: 0,
    OTHER_INFORMATION: 1,
    VERIFICATION: 2,
    PENDING: 6,
    COMPLETED: 7
};
module.exports.KEY_DATES = {
    DATE_DUE: "04",
    CUT_OFF_DATE: "14",
    CLEARING_DATE: "08"
};
