module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const PaymentPlans = sequelize.define(
    "paymentPlans",
    {
      planID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      durationMonths: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(30),
        allowNull: false,
        unique: "unique"
      }
    },
    {
      paranoid: true
    }
  );

  PaymentPlans.initialData = () => {
    const plans = [
      {
        name: "Vendor Loan Standard 3-month",
        durationMonths: 3
      },
      {
        name: "Vendor Loan Standard 6-month",
        durationMonths: 6
      },
      {
        name: "Vendor Loan Standard 9-month",
        durationMonths: 9
      },
      {
        name: "Vendor Loan Standard 12-month",
        durationMonths: 12
      }
    ];
    plans.forEach(plan => {
      PaymentPlans.create(plan);
    });
  };

  PaymentPlans.getPlan = async planID => {
    const results = await PaymentPlans.findOne({
      where: {
        planID: planID
      }
    });
    return results;
  };

  PaymentPlans.getCurrentPlan = async (planID, currentRepayment = null) => {
    let a, conditions;
    a = {
      planID: planID
    };
    conditions =
      currentRepayment != null
        ? {
            ...a,
            ...{
              fromMonth: {
                [Op.lte]: currentRepayment
              },
              toMonth: {
                [Op.gte]: currentRepayment
              }
            }
          }
        : a;
    const results = await PaymentPlans.findAll({
      attributes: [
        ["planID", "planID"],
        ["durationMonths", "durationMonths"],
        [sequelize.literal("paymentPlanDetails.fromMonth"), "fromMonth"],
        [sequelize.literal("paymentPlanDetails.toMonth"), "toMonth"],
        [sequelize.literal("paymentPlanDetails.interestRate"), "interestRate"],
        [sequelize.literal("paymentPlanDetails.principalRate"), "principalRate"]
      ],
      include: [
        {
          model: sequelize.models.paymentPlanDetails,
          required: true,
          attributes: [],
          where: conditions
        }
      ],
      raw: true
    });
    return currentRepayment ? results[0] : results;
  };

  return PaymentPlans;
};
