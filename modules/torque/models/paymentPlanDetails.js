module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;

  const PaymentPlanDetails = sequelize.define(
    "paymentPlanDetails",
    {
      planDetailID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      planID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      fromMonth: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      toMonth: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      principalRate: {
        type: Sequelize.DECIMAL(10, 8),
        allowNull: false
      },
      interestRate: {
        type: Sequelize.DECIMAL(6, 4),
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["planID"] }]
    }
  );

  PaymentPlanDetails.initialData = () => {
    const plans = [
      {
        planID: 1,
        fromMonth: 1,
        toMonth: 1,
        principalRate: 0.3333333333,
        interestRate: 0.025
      },
      {
        planID: 1,
        fromMonth: 2,
        toMonth: 3,
        principalRate: 0.333333333,
        interestRate: 0.005
      },
      {
        planID: 2,
        fromMonth: 1,
        toMonth: 2,
        principalRate: 0.16666666,
        interestRate: 0.015
      },
      {
        planID: 2,
        fromMonth: 3,
        toMonth: 6,
        principalRate: 0.16666666,
        interestRate: 0.005
      }
    ];
    plans.forEach(plan => {
      PaymentPlanDetails.create(plan);
    });
  };
  return PaymentPlanDetails;
};
