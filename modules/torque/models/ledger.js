module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;

  const Ledger = sequelize.define(
    "ledger",
    {
      ledgerID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      ledgerDate: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        primaryKey: true
      },
      endingBalance: {
        type: Sequelize.BIGINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID"] }]
    }
  );

  //functions
  Ledger.enterLedgerBalance = async (accountID, ledgerDate, endingBalance) => {
    const results = await Ledger.create({
      accountID: accountID,
      ledgerDate: ledgerDate,
      endingBalance: endingBalance
    });
    return results;
  };

  Ledger.lastEndingBalance = async (accountID, date = null) => {
    const query = {
      attribute: ["endingBalance"],
      where: {
        accountID: accountID
      },
      order: [["ledgerDate", "DESC"]],
      limit: 1
    };
    query.where = date ? { ...query.where, ...{ ledgerDate: { $lt: date } } } : query.where;
    const result = await Ledger.findOne(query);
    return typeof result === "undefined" || result === null ? 0 : result.endingBalance;
  };

  Ledger.getLedgerPeriod = async (accountID, date) => {
    const startDate = await Ledger.findOne({
      attributes: [["ledgerDate", "date"]],
      where: {
        ledgerDate: {
          [Op.lt]: date
        }
      },
      order: [["ledgerDate", "DESC"]]
    });
    const endDate = await Ledger.findOne({
      attributes: [["ledgerDate", "date"]],
      where: {
        ledgerDate: {
          [Op.gte]: date
        }
      },
      order: [["ledgerDate", "ASC"]]
    });

    return {
      startDate: startDate ? startDate.dataValues.date : null,
      endDate: endDate ? endDate.dataValues.date : null
    };
  };
  return Ledger;
};
