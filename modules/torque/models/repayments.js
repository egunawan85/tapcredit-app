const { REPAYMENT_STATUS } = require("./enum");
const { utils } = require("@src/utils");
const tapUtils = require("../src/tapUtils");
const moment = require("moment-timezone");

module.exports = (sequelize, Sequelize) => {
  const Op = Sequelize.Op;
  const Repayments = sequelize.define(
    "repayments",
    {
      repaymentID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      loanID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      dateDue: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      principalDue: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      paidAt: {
        type: Sequelize.DATE,
        allowNull: true,
        get: function() {
          return this.getDataValue("paidAt")
            ? moment(this.getDataValue("paidAt")).format("YYYY-MM-DD")
            : null;
        }
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID", "loanID"] }]
    }
  );

  Repayments.createRepayment = async (accountID, loanID, dateDue, principalDue) => {
    const results = await Repayments.create({
      accountID: accountID,
      loanID: loanID,
      dateDue: dateDue,
      principalDue: principalDue,
      status: REPAYMENT_STATUS.OPEN
    });
    return results;
  };

  Repayments.setStatus = async (repaymentID, status) => {
    const results = await Repayments.update(
      {
        status: status
      },
      {
        where: {
          repaymentID: repaymentID
        }
      }
    );
    return results;
  };

  Repayments.getRepayments = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "loan":
        type = "loanID";
        break;
      case "status":
        type = "status";
        break;
    }
    const result =
      notEqual == false
        ? await Repayments.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await Repayments.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  Repayments.byRange = async (key, accountID, startDate, endDate, status = null) => {
    let type;
    switch (key) {
      case "due":
        type = "dateDue";
        break;
      case "paid":
        type = "paidAt";
        break;
      case "created":
        type = "createdAt";
        break;
    }
    let a, conditions;
    a = {
      accountID: accountID,
      [type]: {
        [Op.between]: [startDate, endDate]
      }
    };
    conditions = status != null ? { ...a, ...{ status: status } } : a;
    const results = await Repayments.findAll({
      where: conditions
    });
    return results;
  };

  Repayments.getCurrentRepayment = async loanID => {
    if (!loanID) return null;
    const results = await Repayments.findAll({
      attributes: [[sequelize.literal("COUNT(*)+1"), "currentRepayment"]],
      where: {
        loanID: loanID
      }
    });
    return results[0].dataValues.currentRepayment;
  };

  Repayments.getUncleared = async accountID => {
    const results = await Repayments.findAll({
      attributes: {
        include: [[sequelize.literal("loan.loanDate"), "loanDate"]]
      },
      include: [
        {
          model: sequelize.models.loans
        }
      ],
      order: [["dateDue", "ASC"], [sequelize.literal("loan.loanDate"), "ASC"]],
      where: {
        accountID: accountID,
        status: {
          [Op.ne]: REPAYMENT_STATUS.CLEARED
        }
      }
    });
    return results;
  };

  Repayments.getHistUncleared = async (accountID, date) => {
    const ledger = await sequelize.models.ledger.getLedgerPeriod(accountID, date);
    let paidAt = ledger.startDate
      ? {
          $gt: moment(ledger.startDate).endOf("day")
        }
      : {};
    const results = await Repayments.findAll({
      attributes: {
        include: [[sequelize.literal("loan.loanDate"), "loanDate"]]
      },
      include: [
        {
          model: sequelize.models.loans
        }
      ],
      order: [["dateDue", "ASC"], [sequelize.literal("loan.loanDate"), "ASC"]],
      where: {
        accountID: accountID,
        createdAt: {
          $lte: moment(date).endOf("day")
        },
        [Op.or]: [{ paidAt: paidAt }, { paidAt: null }]
      }
    });
    return results;
  };

  Repayments.setPaid = async (repaymentID, dateTime) => {
    const results = await Repayments.update(
      {
        paidAt: dateTime,
        status: REPAYMENT_STATUS.COMPLETED
      },
      {
        where: {
          repaymentID: repaymentID
        }
      }
    );
    return results;
  };

  Repayments.issued = async loanID => {
    const results = await Repayments.findOne({
      attributes: {
        include: [[sequelize.literal("SUM(principalDue)"), "repaymentsIssued"]]
      },
      where: {
        loanID: loanID
      }
    });
    return results.dataValues.repaymentsIssued;
  };

  return Repayments;
};
