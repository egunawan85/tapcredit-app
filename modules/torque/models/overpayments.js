const { utils } = require("@src/utils");
const { OVERPAYMENT_STATUS } = require("../models/enum");

module.exports = (sequelize, Sequelize) => {
  const Overpayments = sequelize.define(
    "overpayments",
    {
      overpaymentID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      accountID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      loanID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      transactionTime: {
        type: Sequelize.DATE,
        allowNull: false
      },
      amount: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      status: {
        type: Sequelize.TINYINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["accountID", "loanID"] }]
    }
  );

  Overpayments.createOverpayment = async (accountID, loanID, amount) => {
    const results = await Overpayments.create({
      accountID: accountID,
      loanID: loanID,
      amount: amount,
      transactionTime: utils.currentDateTime(),
      status: OVERPAYMENT_STATUS.OPEN
    });
    return results;
  };

  Overpayments.getOverpayments = async (accountID, key, id, notEqual = false) => {
    let type;
    switch (key) {
      case "overpayment":
        type = "overpaymentID";
        break;
      case "status":
        type = "status";
        break;
    }
    const result =
      notEqual == false
        ? await Overpayments.findAll({
            where: {
              accountID: accountID,
              [type]: id
            }
          })
        : await Overpayments.findAll({
            where: {
              accountID: accountID,
              [type]: {
                [Op.ne]: id
              }
            }
          });
    return result;
  };

  Overpayments.setStatus = async (overpaymentID, status) => {
    const results = await Overpayments.update(
      {
        status: status
      },
      {
        where: {
          overpaymentID: overpaymentID
        }
      }
    );
    return results;
  };

  return Overpayments;
};
