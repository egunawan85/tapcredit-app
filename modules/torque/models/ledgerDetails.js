module.exports = (sequelize, Sequelize) => {
  const LedgerDetails = sequelize.define(
    "ledgerDetails",
    {
      ledgerDetailID: {
        type: Sequelize.BIGINT(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      ledgerID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      refItemID: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      itemType: {
        type: Sequelize.BIGINT(20),
        allowNull: false
      },
      amount: {
        type: Sequelize.BIGINT,
        allowNull: false
      }
    },
    {
      paranoid: true,
      indexes: [{ fields: ["ledgerID"] }]
    }
  );

  LedgerDetails.enterLedgerDetail = async (
    ledgerID,
    refItemID,
    itemType,
    amount
  ) => {
    const results = await LedgerDetails.create({
      ledgerID: ledgerID,
      refItemID: refItemID,
      itemType: itemType,
      amount: amount
    });
    return results;
  };
  return LedgerDetails;
};
