const tapUtils = require("../src/tapUtils");
const { utils } = require("@src/utils");
const db = require("@config/lib/sequelize");
const Torque = require("../lib/Torque");
const loanState = require("../src/loanState");
const loanFn = require("../src/loanFn");
const loanSchedule = require("../src/loanSchedule");
const { KEY_DATES } = require("../models/enum");

module.exports = app => {
	app.get("/api/loans", async (req, res) => {
		const results = await loanState.getMinimumDue(1);
		res.send({ data: results });
	});
	app.get("/api/paymentSchedule/:accountID/all", async (req, res) => {
		const results = await Torque.paymentSchedule(req.params.accountID, true);
		res.send({ data: results });
	});
	app.get("/api/paymentSchedule/new/:planID/:amount", async (req, res) => {
		const results = await Torque.simulateSchedule(req.params.amount, req.params.planID);
		res.send({ data: results });
	});
	app.get("/api/test", async (req, res) => {
		// const schedule = await loanSchedule.simulatePaymentSchedule(10000000, 1);
		// const results = await loanSchedule.getCombinedPaymentSchedule(1, schedule);
		// const perLoan = await loanSchedule.getPaymentSchedulePerLoan(1);
		const results = await loanFn.currentPlan(1, 202);
		res.send({ data: results });
	});
	// app.post("/api/nextStatementDate", (req, res) => {
	// 	const results = tapUtils.nextStatementDate(req.body.nextPaymentDue);
	// 	res.send(results);
	// });
};
