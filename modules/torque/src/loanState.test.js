require("module-alias/register");
const itemFn = require("./itemFn");
const assert = require("assert");
const db = require("@config/lib/sequelize");
const loanFn = require("./loanFn.js");
const loanState = require("./loanState.js");
const moment = require("moment-timezone");
const tapUtils = require("./tapUtils");
const { utils, now } = require("@src/utils");
const {
	PAYMENT_TYPE,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS,
	KEY_DATES
} = require("../models/enum");

const accountID = 1;
const planID = [1, 2];
const interestRate = [0.025, 0.015]; //accordin to planID
const durationMonths = [3, 6]; //according to planID
const loan = [10000000, 5000000];
const endingBalance = 100000; //according to late fee
const fee = 350000;
const payments = [
	fee - endingBalance,
	interestRate[0] * loan[0],
	interestRate[1] * loan[1],
	loan[0] / durationMonths[0],
	loan[1] / durationMonths[1],
	loan[0] - loan[0] / durationMonths[0]
];
const time = moment(now);
let oneDayLater = moment(now)
	.add(1, "days")
	.format("YYYY-MM-DD");

before(async () => {
	await db.repayments.destroy({ where: {}, force: true });
	await db.interestFees.destroy({ where: {}, force: true });
	await db.fees.destroy({ where: {}, force: true });
	await db.ledger.destroy({ where: {}, force: true });
	await db.ledgerDetails.destroy({ where: {}, force: true });
	await db.payments.destroy({ where: {}, force: true });
	await db.overpayments.destroy({ where: {}, force: true });
	await db.loans.destroy({ where: {}, force: true });
});

describe("Loan States", async () => {
	let loanID, paidAmount, interestAmount, loanAmount, fees, balanceOutstanding;

	beforeEach(async () => {
		loans = await db.loans.findAll({ where: { accountID: accountID } });
		paidAmount = payments.reduce((total, payment) => {
			return total + Math.round(payment);
		});
		interestAmount = loan[0] * interestRate[0] + loan[1] * interestRate[1];
		loanAmount = loan[0] + loan[1];
		fees = (await db.fees.findAll())[0].feeDue;
		balanceOutstanding = loanAmount + interestAmount + fees - endingBalance - paidAmount;
	});

	it("checks balance outstanding", async () => {
		assert.equal(await loanState.getBalanceOutstanding(accountID), balanceOutstanding);
	});

	it("checks account loan balance outstanding", async () => {
		assert.equal(await loanState.getLoanBalanceOutstanding(accountID, loans[0].loanID), 0);
		assert.equal(
			await loanState.getLoanBalanceOutstanding(accountID, loans[1].loanID),
			balanceOutstanding
		);
	});

	it("checks historical account balance outstanding", async () => {
		assert.equal(
			await loanState.getBalanceOutstanding(accountID, oneDayLater),
			balanceOutstanding
		);
		assert.equal(
			await loanState.getBalanceOutstanding(
				accountID,
				time.subtract(1, "day").format("YYYY-MM-DD")
			),
			0
		);
		assert.equal(
			await loanState.getBalanceOutstanding(
				accountID,
				moment(time.add(1, "days").format("YYYY-MM-" + KEY_DATES.CLEARING_DATE))
					.add(1, "days")
					.format("YYYY-MM-DD")
			),
			balanceOutstanding
		);
	});

	it("checks historical account loan balance outstanding", async () => {
		assert.equal(
			await loanState.getLoanBalanceOutstanding(
				accountID,
				loans[0].loanID,
				time.format("YYYY-MM-DD")
			),
			loan[0] * (1 + interestRate[0])
		);
		assert.equal(
			await loanState.getLoanBalanceOutstanding(
				accountID,
				loans[0].loanID,
				time.subtract(1, "day").format("YYYY-MM-DD")
			),
			0
		);
		assert.equal(
			await loanState.getBalanceOutstanding(
				accountID,
				loans[0].loanID,
				moment(time.add(1, "days").format("YYYY-MM-" + KEY_DATES.CLEARING_DATE))
					.add(1, "days")
					.format("YYYY-MM-DD")
			),
			0
		);
	});

	it("checks that minimum due is correct", async () => {
		assert.equal(await loanState.getMinimumDue(accountID, moment(now)), fee - endingBalance);
		assert.equal(await loanState.getMinimumDue(accountID, moment(now).add(1, "day")), 0);
	});
});
