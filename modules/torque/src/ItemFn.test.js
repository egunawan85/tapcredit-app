require("module-alias/register");
const itemFn = require("./itemFn");
const assert = require("assert");
const db = require("@config/lib/sequelize");
const loanFn = require("./loanFn.js");
const moment = require("moment-timezone");
const { utils, now } = require("@src/utils");
const tapUtils = require("./tapUtils");
const {
	PAYMENT_TYPE,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS,
	KEY_DATES
} = require("../models/enum");

const accountID = 1;
const planID = [1, 2];
const interestRate = [0.025, 0.015]; //accordin to planID
const durationMonths = [3, 6]; //according to planID
const loan = [10000000, 5000000];
const endingBalance = 100000; //according to late fee
const fee = 350000;
const payments = [
	fee - endingBalance,
	interestRate[0] * loan[0],
	interestRate[1] * loan[1],
	loan[0] / durationMonths[0],
	loan[1] / durationMonths[1],
	loan[0] - loan[0] / durationMonths[0]
]; //ending balance payment 1 equals Fee
let oneDayLater = moment(now)
	.add(1, "days")
	.format("YYYY-MM-DD");

before(async () => {
	await db.repayments.destroy({ where: {}, force: true });
	await db.interestFees.destroy({ where: {}, force: true });
	await db.fees.destroy({ where: {}, force: true });
	await db.ledger.destroy({ where: {}, force: true });
	await db.ledgerDetails.destroy({ where: {}, force: true });
	await db.payments.destroy({ where: {}, force: true });
	await db.overpayments.destroy({ where: {}, force: true });
	await db.loans.destroy({ where: {}, force: true });
});

describe("Items", async () => {
	it("creates two loans, late fee, and an ending balance", async () => {
		assert.ok(await loanFn.addNewLoan(accountID, planID[0], loan[0]));
		assert.ok(await loanFn.addNewLoan(accountID, planID[1], loan[1]));
		assert.ok(await loanFn.addLateFee(accountID, loan[0] + loan[1]));
		assert.ok(db.ledger.enterLedgerBalance(accountID, utils.currentDate(), endingBalance));
	});
	it("Payment 1 is made: checks that fee is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[0],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.surplus, 0);
		assert.equal(results.fees[0].status, FEE_STATUS.COMPLETED);
		assert.equal(results.interestFees[0].status, INTEREST_FEE_STATUS.OPEN);
		assert.equal(results.interestFees[1].status, INTEREST_FEE_STATUS.OPEN);
		assert.equal(results.interestFees[0], itemFn.searchItemNextPay(results));
	});
	it("Payment 2 is made: checks that interestFee 1 is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[1],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.interestFees[0].status, INTEREST_FEE_STATUS.COMPLETED);
		assert.equal(results.interestFees[1].status, INTEREST_FEE_STATUS.OPEN);
		assert.equal(results.interestFees[1], itemFn.searchItemNextPay(results));
	});
	it("Payment 3 is made: checks that interestFee 2 is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[2],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.surplus, 0);
		assert.equal(results.interestFees[0].status, INTEREST_FEE_STATUS.COMPLETED);
		assert.equal(results.interestFees[1].status, INTEREST_FEE_STATUS.COMPLETED);
		assert.equal(results.repayments[0], itemFn.searchItemNextPay(results));
	});
	it("Payment 4 is made: checks that repayments 1 is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[3],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.surplus, 0);
		assert.equal(results.repayments[0].status, REPAYMENT_STATUS.COMPLETED);
		assert.equal(results.repayments[1].status, REPAYMENT_STATUS.OPEN);
		assert.equal(results.repayments[1], itemFn.searchItemNextPay(results));
	});
	it("Payment 5 is made: checks that repayments 2 is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[4],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.surplus, 0);
		assert.equal(results.repayments[0].status, REPAYMENT_STATUS.COMPLETED);
		assert.equal(results.repayments[1].status, REPAYMENT_STATUS.COMPLETED);
		assert.equal(results.loans[0], itemFn.searchItemNextPay(results));
	});
	it("Payment 6 is made: checks that loan 1 is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[5],
			oneDayLater,
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			oneDayLater
		);
		assert.equal(results.surplus, 0);
		assert.equal(results.loans[0].status, LOAN_STATUS.COMPLETED);
		assert.equal(results.loans[1].status, LOAN_STATUS.OPEN);
		assert.equal(results.loans[1], itemFn.searchItemNextPay(results));
	});
	it("Clear all items and generate ledger", async () => {
		const items = await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID));
		await itemFn.setItemsCleared(items, oneDayLater);
		const results = await itemFn.generateLedger(items, accountID);
		assert.ok(results);
	});
	it("Adjusts dates to simulate one month forward", async () => {
		let time = moment(now);
		const clearingTime = time
			.endOf("day")
			.format("YYYY-MM-" + KEY_DATES.CLEARING_DATE + " H:m:s");
		await db.payments.update(
			{
				clearedAt: clearingTime
			},
			{
				where: {
					accountID: accountID,
					clearedAt: {
						$ne: null
					}
				}
			}
		);
		await db.overpayments.update(
			{
				createdAt: clearingTime,
				transactionTime: clearingTime
			},
			{
				where: {
					accountID: accountID
				}
			}
		);
		await db.ledger.update(
			{
				ledgerDate: time
					.endOf("day")
					.subtract(1, "day")
					.format("YYYY-MM-DD")
			},
			{
				where: {
					accountID: accountID,
					ledgerDate: time
						.endOf("day")
						.add(1, "day")
						.format("YYYY-MM-DD")
				}
			}
		);
	});
});
