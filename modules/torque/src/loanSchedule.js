const db = require("@config/lib/sequelize");
const itemFn = require("./itemFn");
const { LOAN_STATUS, REPAYMENT_STATUS, INTEREST_FEE_STATUS } = require("../models/enum");
const { now } = require("@src/utils");
const moment = require("moment-timezone");
const tapUtils = require("../src/tapUtils");

const loanSchedule = {
	getPaymentSchedule: async (accountID, all = false) => {
		const schedule =
			all == true
				? loanSchedule.getCombinedPaymentSchedule(accountID)
				: loanSchedule.getPaymentSchedulePerLoan(accountID);
		return schedule;
	},

	getCombinedPaymentSchedule: async (accountID, schedule) => {
		const schedules = await loanSchedule.getPaymentSchedulePerLoan(accountID);
		if (schedule) {
			schedules.push(schedule);
		}
		let tSchedule = {};

		schedules.forEach(loan => {
			loan.payments.forEach(payment => {
				let ts = new Date(payment.dateDue).getTime() / 1000;
				tSchedule[ts] = tSchedule[ts] || {};
				tSchedule[ts]["dateDue"] = tSchedule[ts]["dateDue"] || payment.dateDue;
				tSchedule[ts]["principalRepayment"] = tSchedule[ts]["principalRepayment"] || 0;
				tSchedule[ts]["principalRepayment"] += payment.principalRepayment;
				tSchedule[ts]["interestDue"] = tSchedule[ts]["interestDue"] || 0;
				tSchedule[ts]["interestDue"] += payment.interestDue;
				tSchedule[ts]["totalPayment"] = tSchedule[ts]["totalPayment"] || 0;
				tSchedule[ts]["totalPayment"] += payment.totalPayment;
			});
		});

		const scheduleArray = Object.keys(tSchedule).map(i => tSchedule[i]);

		return scheduleArray;
	},

	getPaymentSchedulePerLoan: async accountID => {
		const loans = await db.loans.findAll({
			order: [["loanDAte", "DESC"]],
			where: {
				accountID: accountID,
				status: LOAN_STATUS.OPEN
			}
		});

		const items = await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID));

		const schedules = loans.map(async loan => {
			return await loanSchedule.getPaymentScheduleData(loan, items);
		});

		return await Promise.all(schedules);
	},

	getPaymentScheduleData: async (loan, items) => {
		const data = {
			loanID: loan.loanID,
			loanRef: loan.loanID + "00" + loan.planID + moment(now).format("YYYY"),
			loanDate: loan.loanDate,
			amount: loan.amountMoney,
			payments: await loanSchedule.getPayments(loan, items)
		};

		return data;
	},

	getPayments: async (loan, items) => {
		let currentRepayment = loan.loanID
			? (await db.repayments.getCurrentRepayment(loan.loanID)) - 1
			: 1;
		const currentPlan = await db.paymentPlans.getCurrentPlan(loan.planID, currentRepayment);
		const durationMonths = currentPlan.durationMonths;
		let time = moment(loan.nPaymentDue);
		let nPaymentDue = time.format("YYYY-MM-DD");
		const payments = [];

		while (currentRepayment <= durationMonths) {
			const principalRepayment = await loanSchedule.calcRepayment(
				loan,
				items,
				currentRepayment,
				durationMonths,
				nPaymentDue
			);

			const interestDue = await loanSchedule.calcInterestDue(
				loan,
				items,
				currentRepayment,
				nPaymentDue
			);

			payments.push({
				dateDue: moment(nPaymentDue).format("YYYY-MM-DD"),
				principalRepayment: Math.round(principalRepayment),
				interestDue: Math.round(interestDue),
				totalPayment: Math.round(principalRepayment) + Math.round(interestDue)
			});
			currentRepayment++;
			nPaymentDue = time.add(1, "months").format("YYYY-MM-DD");
		}

		if (items) {
			const nextItem = await itemFn.searchItemNextPay(items);

			if (
				nextItem &&
				items.surplus > 0 &&
				nextItem.dataValues.hasOwnProperty("principalOutstanding") &&
				nextItem.loanID == loan.loanID
			) {
				let balance = items.surplus;
				while (balance > 0) {
					payments.forEach(payment => {
						if (balance > payment.principalRepayment) {
							balance -= payment.principalRepayment;
							payment.principalRepayment = 0;
						} else {
							payment.principalRepayment -= balance;
							balance = 0;
						}
						if (balance > payment.interestDue) {
							balance -= payment.interestDue;
							payment.interestDue = 0;
						} else {
							payment.interestDue -= balance;
							balance = 0;
						}
					});
				}
			}
		}

		return payments;
	},

	calcRepayment: async (loan, items, currentRepayment, durationMonths, nPaymentDue) => {
		let principalRepayment;

		if (currentRepayment === durationMonths) {
			let issued = 0;
			let x = currentRepayment;
			while (x > 1) {
				let cplan = await db.paymentPlans.getCurrentPlan(loan.planID, x);
				issued += Math.round(loan.amountMoney * cplan.principalRate);
				x--;
			}
			principalRepayment = loan.amountMoney - issued;
		} else {
			const currentPlan = await db.paymentPlans.getCurrentPlan(loan.planID, currentRepayment);
			principalRepayment = loan.amountMoney * currentPlan.principalRate;
		}

		if (items) {
			const nextItem = await itemFn.searchItemNextPay(items);

			if (
				await loanSchedule.checkRepaymentStatus(items.repayments, nPaymentDue, loan.loanID)
			) {
				principalRepayment = 0;
			}

			if (
				nextItem &&
				nextItem.loanID == loan.loanID &&
				nextItem.dateDue == nPaymentDue &&
				nextItem.dataValues.hasOwnProperty("principalDue")
			) {
				principalRepayment -= items.surplus;
			}
		}

		return principalRepayment;
	},

	checkRepaymentStatus: async (repayments, nPaymentDue, loanID) => {
		if (
			repayments.find(repayment => {
				return (
					repayment.status == REPAYMENT_STATUS.COMPLETED &&
					repayment.loanID == loanID &&
					repayment.dateDue == nPaymentDue
				);
			}) ||
			(await db.repayments.findOne({
				where: {
					status: REPAYMENT_STATUS.CLEARED,
					loanID: loanID,
					dateDue: nPaymentDue
				}
			}))
		) {
			return true;
		} else {
			return false;
		}
	},

	calcInterestDue: async (loan, items, currentRepayment, nPaymentDue) => {
		const planDetail = await db.paymentPlans.getCurrentPlan(loan.planID, currentRepayment);

		let interestDue;
		interestDue = loan.amountMoney * planDetail.interestRate;

		if (items) {
			const nextItem = itemFn.searchItemNextPay(items);

			if (
				await loanSchedule.checkInterestFeeStatus(
					items.interestFees,
					nPaymentDue,
					loan.loanID
				)
			) {
				interestDue = 0;
			}
			if (
				nextItem &&
				nextItem.loanID == loan.loanID &&
				nextItem.dateDue == nPaymentDue &&
				nextItem.dataValues.hasOwnProperty("interestDue")
			) {
				interestDue -= items.surplus;
			}
		}

		return interestDue;
	},

	checkInterestFeeStatus: async (interestFees, nPaymentDue, loanID) => {
		if (
			interestFees.find(interestFee => {
				return (
					interestFee.status == INTEREST_FEE_STATUS.COMPLETED &&
					interestFee.loanID == loanID &&
					interestFee.dateDue == nPaymentDue
				);
			}) ||
			(await db.interestFees.findOne({
				where: {
					status: INTEREST_FEE_STATUS.CLEARED,
					loanID: loanID,
					dateDue: nPaymentDue
				}
			}))
		) {
			return true;
		} else {
			return false;
		}
	},

	simulatePaymentSchedule: async (amount, planID) => {
		const loan = {
			planID: planID,
			amountMoney: amount,
			nPaymentDue: tapUtils.nextPaymentDue(),
			loanID: null
		};
		const schedule = {
			loanID: "simulation",
			loanRef: "simulation",
			loanDate: "simulation",
			amount: amount,
			payments: await loanSchedule.getPayments(loan, null)
		};

		return schedule;
	}
};

module.exports = loanSchedule;
