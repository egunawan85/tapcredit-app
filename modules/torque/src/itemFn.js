const db = require("@config/lib/sequelize");
const moment = require("moment-timezone");
const { now, utils } = require("@src/utils");
const { KEY_DATES } = require("../models/enum");

const {
	PAYMENT_STATUS,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS,
	OVERPAYMENT_STATUS,
	GL_DETAIL_STATUS,
	ITEM_TYPE
} = require("../models/enum");

const itemFn = {
	getItems: async (accountID, date = null) => {
		const items = date
			? await itemFn.getHistUnclearedItems(accountID, date)
			: await itemFn.getUnclearedItems(accountID);
		return items;
	},

	getHistUnclearedItems: async (accountID, date) => {
		const results = {
			payments: await db.payments.getHistUncleared(accountID, date),
			fees: await db.fees.getHistUncleared(accountID, date),
			interestFees: await db.interestFees.getHistUncleared(accountID, date),
			repayments: await db.repayments.getHistUncleared(accountID, date),
			loans: await db.loans.getHistPrincipalOutstanding(accountID, date),
			surplus: await db.ledger.lastEndingBalance(accountID, date)
		};
		const items = itemFn.revertHistItemsState(results, date);
		return items;
	},

	revertHistItemsState: (items, date) => {
		items.payments.forEach(payment => {
			payment.transactionStatus =
				payment.clearedAt < date && payment.clearedAt
					? PAYMENT_STATUS.COMPLETED
					: PAYMENT_STATUS.SUCCESS;
		});
		items.fees.forEach(fee => {
			fee.status = fee.paidAt < date && fee.paidAt ? FEE_STATUS.COMPLETED : FEE_STATUS.OPEN;
		});
		items.interestFees.forEach(interestFee => {
			interestFee.status =
				interestFee.paidAt < date && interestFee.paidAt
					? INTEREST_FEE_STATUS.COMPLETED
					: INTEREST_FEE_STATUS.OPEN;
		});
		items.repayments.forEach(repayment => {
			repayment.status =
				repayment.paidAt < date && repayment.paidAt
					? REPAYMENT_STATUS.COMPLETED
					: REPAYMENT_STATUS.OPEN;
		});
		items.loans.forEach(loan => {
			loan.status =
				loan.paidAt < date && loan.paidAt ? LOAN_STATUS.COMPLETED : LOAN_STATUS.OPEN;
		});
		return items;
	},

	getUnclearedItems: async accountID => {
		const items = {
			payments: await db.payments.getUncleared(accountID),
			fees: await db.fees.getUncleared(accountID),
			interestFees: await db.interestFees.getUncleared(accountID),
			repayments: await db.repayments.getUncleared(accountID),
			loans: await db.loans.getPrincipalOutstanding(accountID),
			surplus: await db.ledger.lastEndingBalance(accountID)
		};
		return items;
	},

	sumPaymentItems: items => {
		let balance = 0;
		items.payments.forEach(payment => {
			balance += payment.grossAmount;
		});
		return balance;
	},

	sumCompletedItems: items => {
		let balance = 0;
		items.fees.forEach(fee => {
			if (fee.status == FEE_STATUS.COMPLETED) {
				balance += fee.feeDue;
			}
		});

		items.interestFees.forEach(interestFee => {
			if (interestFee.status == INTEREST_FEE_STATUS.COMPLETED) {
				balance += interestFee.interestDue;
			}
		});

		items.repayments.forEach(repayment => {
			if (repayment.status == REPAYMENT_STATUS.COMPLETED) {
				balance += repayment.principalDue;
			}
		});

		items.loans.forEach(loan => {
			if (loan.status == LOAN_STATUS.COMPLETED) {
				balance += loan.principalOutstanding;
			}
		});
		return balance;
	},

	allocatePaymentsToItems: items => {
		let stop = false;
		let balance =
			itemFn.sumPaymentItems(items) - itemFn.sumCompletedItems(items) + items.surplus;

		let dataHolder = {};
		items.fees.forEach((item, key) => {
			let timestamp = new Date(item.dateDue).getTime() / 1000;
			dataHolder[timestamp] = dataHolder[timestamp] || {};
			dataHolder[timestamp]["fees"] = dataHolder[timestamp]["fees"] || [];
			dataHolder[timestamp]["fees"].push({
				key: key,
				amount: item.feeDue,
				status: item.status
			});
		});
		items.interestFees.forEach((item, key) => {
			let timestamp = new Date(item.dateDue).getTime() / 1000;
			dataHolder[timestamp] = dataHolder[timestamp] || {};
			dataHolder[timestamp]["interestFees"] = dataHolder[timestamp]["interestFees"] || [];
			dataHolder[timestamp]["interestFees"].push({
				key: key,
				amount: item.interestDue,
				status: item.status
			});
		});
		items.repayments.forEach((item, key) => {
			let timestamp = new Date(item.dateDue).getTime() / 1000;
			dataHolder[timestamp] = dataHolder[timestamp] || {};
			dataHolder[timestamp]["repayments"] = dataHolder[timestamp]["repayments"] || [];
			dataHolder[timestamp]["repayments"].push({
				key: key,
				amount: item.principalDue,
				status: item.status
			});
		});

		let unordered = dataHolder;
		Object.keys(unordered)
			.sort()
			.forEach(function(key) {
				dataHolder[key] = unordered[key];
			});

		let statusComplete;
		Object.keys(dataHolder).forEach(ts => {
			Object.keys(dataHolder[ts]).forEach(dataType => {
				switch (dataType) {
					case "fees":
						statusComplete = FEE_STATUS.COMPLETED;
						break;
					case "interestFees":
						statusComplete = INTEREST_FEE_STATUS.COMPLETED;
						break;
					case "repayments":
						statusComplete = REPAYMENT_STATUS.COMPLETED;
						break;
				}
				dataHolder[ts][dataType].forEach(dataItem => {
					if (dataItem.status != statusComplete && stop == false) {
						if (balance >= dataItem.amount) {
							balance -= dataItem.amount;
							dataItem.status = statusComplete;
							items[dataType][dataItem.key].status = dataItem.status;
						} else {
							stop = true;
						}
					}
				});
			});
		});
		items.loans.forEach(loan => {
			if (loan.status != LOAN_STATUS.COMPLETED && stop == false) {
				if (balance >= loan.dataValues.principalOutstanding) {
					balance -= loan.dataValues.principalOutstanding;
					loan.status = LOAN_STATUS.COMPLETED;
				} else {
					stop = true;
				}
			}
		});

		items.surplus = balance || 0;

		return items;
	},

	setItemsComplete: async (items, dateTime) => {
		try {
			await db.sequelize.transaction(async t => {
				await items.fees.forEach(async fee => {
					if (fee.status == FEE_STATUS.COMPLETED) {
						return await db.fees.setPaid(fee.feeID, dateTime);
					}
				});
				await items.interestFees.forEach(async interestFee => {
					if (interestFee.status == INTEREST_FEE_STATUS.COMPLETED) {
						return await db.interestFees.setPaid(interestFee.interestFeeID, dateTime);
					}
				});
				await items.repayments.forEach(async repayment => {
					if (repayment.status == REPAYMENT_STATUS.COMPLETED) {
						return await db.repayments.setPaid(repayment.repaymentID, dateTime);
					}
				});
				await items.loans.forEach(async loan => {
					if (loan.status == LOAN_STATUS.COMPLETED) {
						return await db.loans.setPaid(loan.loanID, dateTime);
					}
				});
			});
			return items;
		} catch (err) {
			return err;
		}
	},

	setItemsCleared: async (items, dateTime) => {
		try {
			await db.sequelize.transaction(async t => {
				await items.fees.forEach(async fee => {
					if (fee.status == FEE_STATUS.COMPLETED) {
						return await db.fees.setStatus(fee.feeID, FEE_STATUS.CLEARED);
					}
				});
				await items.interestFees.forEach(async interestFee => {
					if (interestFee.status == INTEREST_FEE_STATUS.COMPLETED) {
						return await db.interestFees.setStatus(
							interestFee.interestFeeID,
							INTEREST_FEE_STATUS.CLEARED
						);
					}
				});
				await items.repayments.forEach(async repayment => {
					if (repayment.status == REPAYMENT_STATUS.COMPLETED) {
						return await db.repayments.setStatus(
							repayment.repaymentID,
							REPAYMENT_STATUS.CLEARED
						);
					}
				});
				await items.loans.forEach(async loan => {
					if (loan.status == LOAN_STATUS.COMPLETED) {
						await db.loans.setStatus(loan.loanID, LOAN_STATUS.CLEARED);
						if (loan.dataValues.principalOutstanding > 0) {
							await db.overpayments.createOverpayment(
								loan.accountID,
								loan.loanID,
								loan.dataValues.principalOutstanding
							);
						}
					}
				});
				await items.payments.forEach(async payment => {
					await db.payments.setCleared(payment.paymentID, dateTime);
				});
			});
			return results;
		} catch (err) {
			return err;
		}
	},

	generateLedger: async (items, accountID) => {
		try {
			return await db.sequelize.transaction(async t => {
				let time = moment(now);
				const ledgerID = (await db.ledger.enterLedgerBalance(
					accountID,
					time.format("YYYY-MM-" + KEY_DATES.CLEARING_DATE),
					items.surplus
				)).ledgerID;
				const overpayments = await db.overpayments.getOverpayments(1, "status", 0);
				await items.fees.forEach(async fee => {
					if (fee.status == FEE_STATUS.COMPLETED) {
						await db.ledgerDetails.enterLedgerDetail(
							ledgerID,
							fee.feeID,
							ITEM_TYPE.FEE,
							fee.feeDue
						);
					}
				});
				await items.interestFees.forEach(async interestFee => {
					if (interestFee.status == INTEREST_FEE_STATUS.COMPLETED) {
						await db.ledgerDetails.enterLedgerDetail(
							ledgerID,
							interestFee.interestFeeID,
							ITEM_TYPE.INTERESTFEE,
							interestFee.interestDue
						);
					}
				});
				await items.repayments.forEach(async repayment => {
					if (repayment.status == REPAYMENT_STATUS.COMPLETED) {
						await db.ledgerDetails.enterLedgerDetail(
							ledgerID,
							repayment.repaymentID,
							ITEM_TYPE.REPAYMENT,
							repayment.principalDue
						);
					}
				});
				await items.payments.forEach(async payment => {
					await db.ledgerDetails.enterLedgerDetail(
						ledgerID,
						payment.paymentID,
						ITEM_TYPE.PAYMENT,
						payment.grossAmount
					);
				});
				overpayments.forEach(async overpayment => {
					if (overpayment.status == OVERPAYMENT_STATUS.OPEN) {
						await db.ledgerDetails.enterLedgerDetail(
							ledgerID,
							overpayment.overpaymentID,
							ITEM_TYPE.OVERPAYMENT,
							overpayment.amount
						);
						await db.overpayments.setStatus(
							overpayment.overpaymentID,
							OVERPAYMENT_STATUS.CLEARED
						);
					}
				});
				return overpayments;
			});
		} catch (err) {
			return err;
		}
	},

	searchItemNextPay: items => {
		let result;

		result = items.fees.forEach(fee => {
			return fee.status == FEE_STATUS.OPEN && fee.dateDue < utils.currentDate();
		});
		if (result) return result;

		result = items.interestFees.forEach(interestFee => {
			return (
				interestFee.status == INTEREST_FEE_STATUS.OPEN &&
				interestFee.dateDue < utils.currentDate()
			);
		});
		if (result) return result;

		result = items.repayments.find(repayment => {
			return (
				repayment.status == REPAYMENT_STATUS.OPEN && repayment.dateDue < utils.currentDate()
			);
		});
		if (result) return result;

		result = items.fees.find(fee => {
			return fee.status == FEE_STATUS.OPEN;
		});
		if (result) return result;

		result = items.interestFees.find(interestFee => {
			return interestFee.status == INTEREST_FEE_STATUS.OPEN;
		});
		if (result) return result;

		result = items.repayments.find(repayment => {
			return repayment.status == REPAYMENT_STATUS.OPEN;
		});
		if (result) return result;

		result = items.loans.find(loan => {
			return loan.status == LOAN_STATUS.OPEN;
		});
		if (result) return result;

		if (!result) return null;
	},

	sumPayments: items => {
		return (
			items.payments.reduce((total, payment) => {
				return total + payment.grossAmount;
			}, 0) || 0
		);
	},

	sumFees: (items, status = null, dateDue = null) => {
		let total = 0;
		items.fees.forEach(fee => {
			let conditions = `items`;
			conditions += status != null ? ` && fee.status == status` : ``;
			conditions += dateDue != null ? ` && fee.dateDue <= dateDue` : ``;
			if (eval(conditions)) {
				total += fee.feeDue;
			}
		});
		return total;
	},

	sumInterestFees: (items, loanID = null, status = null, dateDue = null) => {
		let total = 0;
		items.interestFees.forEach(interestFee => {
			let conditions = `items`;
			conditions += loanID != null ? ` && interestFee.loanID == loanID` : ``;
			conditions += status != null ? ` && interestFee.status == status` : ``;
			conditions += dateDue != null ? ` && interestFee.dateDue <= dateDue` : ``;
			if (eval(conditions)) {
				total += interestFee.interestDue;
			}
		});
		return total;
	},

	sumRepayments: (items, loanID = null, status = null, dateDue = null) => {
		let total = 0;
		items.repayments.forEach(repayment => {
			let conditions = `items`;
			conditions += loanID != null ? ` && repayment.loanID == loanID` : ``;
			conditions += status != null ? ` && repayment.status == status` : ``;
			conditions += dateDue != null ? ` && repayment.dateDue <= dateDue` : ``;
			if (eval(conditions)) {
				total += repayment.principalDue;
			}
		});
		return total;
	},

	sumLoans: (items, loanID = null, status = null) => {
		let total = 0;
		items.loans.forEach(loan => {
			let conditions = `items`;
			conditions += loanID != null ? ` && loan.loanID == loanID` : ``;
			conditions += status != null ? ` && loan.status == status` : ``;
			if (eval(conditions)) {
				total += loan.dataValues.principalOutstanding;
			}
		});
		return total;
	}
};

module.exports = itemFn;
