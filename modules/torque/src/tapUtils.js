const moment = require("moment-timezone");
const { KEY_DATES } = require("../models/enum");
const { now } = require("@src/utils");

const tapUtils = {
	nextPaymentDue: (newLoan = true, date = null) => {
		let time = date == null ? moment(now) : moment(date);
		if (newLoan) {
			if (time.format("DD") <= KEY_DATES.CUT_OFF_DATE) {
				return KEY_DATES.CUT_OFF_DATE < KEY_DATES.DATE_DUE
					? time.format("YYYY-MM-" + KEY_DATES.DATE_DUE)
					: time.add(1, "months").format("YYYY-MM-" + KEY_DATES.DATE_DUE);
			} else {
				return KEY_DATES.CUT_OFF_DATE < KEY_DATES.DATE_DUE
					? time.add(1, "months").format("YYYY-MM-" + KEY_DATES.DATE_DUE)
					: time.add(2, "months").format("YYYY-MM-" + KEY_DATES.DATE_DUE);
			}
		} else {
			return time.format("DD") <= KEY_DATES.DATE_DUE
				? time.format("YYYY-MM-" + KEY_DATES.DATE_DUE)
				: time.add(1, "months").format("YYYY-MM-" + KEY_DATES.DATE_DUE);
		}
	},

	nextStatementDate: (date = null) => {
		let time = moment(now);
		if (date) {
			const nextPaymentDue = moment(date);
			const statementDateBefore =
				KEY_DATES.CUT_OFF_DATE < KEY_DATES.DATE_DUE
					? moment(nextPaymentDue.format("YYYY-MM-" + KEY_DATES.CUT_OFF_DATE))
					: moment(
							nextPaymentDue
								.subtract(1, "months")
								.format("YYYY-MM-" + KEY_DATES.CUT_OFF_DATE)
					  );

			return time <= statementDateBefore
				? statementDateBefore.format("YYYY-MM-DD")
				: statementDateBefore.add(1, "months").format("YYYY-MM-" + KEY_DATES.CUT_OFF_DATE);
		} else {
			return time.format("DD") < KEY_DATES.CUT_OFF_DATE
				? time.format("YYYY-MM-" + KEY_DATES.CUT_OFF_DATE)
				: time.add(1, "months").format("YYYY-MM-" + KEY_DATES.CUT_OFF_DATE);
		}
	}
};

module.exports = tapUtils;
