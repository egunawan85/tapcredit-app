require("module-alias/register");
const loanFn = require("./loanFn");
const assert = require("assert");
const db = require("@config/lib/sequelize");
const { utils } = require("@src/utils");
const {
	PAYMENT_TYPE,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS
} = require("../models/enum");

const accountID = 2;
const planID = [1, 2];
const interestRate = [0.025, 0.015]; //accordin to planID
const durationMonths = [3, 6]; //according to planID
const loan = [7000000, 5000000];

before(async () => {
	await db.repayments.destroy({ where: {}, force: true });
	await db.interestFees.destroy({ where: {}, force: true });
	await db.fees.destroy({ where: {}, force: true });
	await db.ledger.destroy({ where: {}, force: true });
	await db.ledgerDetails.destroy({ where: {}, force: true });
	await db.payments.destroy({ where: {}, force: true });
	await db.overpayments.destroy({ where: {}, force: true });
	await db.loans.destroy({ where: {}, force: true });
});

describe("Loan Functions", async () => {
	it("Creates a loan, generate all repayments and checks total", async () => {
		const newLoan = await loanFn.addNewLoan(accountID, planID[0], loan[0]);
		const loanID = newLoan[0].loanID;
		let x = 1;
		while (x < durationMonths[0]) {
			await loanFn.generateRepayment(accountID, loanID);
			x++;
		}
		const repaymentTotal = await db.repayments.sum("principalDue", {
			where: { loanID: loanID }
		});
		assert.equal(repaymentTotal, loan[0]);
	});
});
