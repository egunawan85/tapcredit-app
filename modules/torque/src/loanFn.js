const db = require("@config/lib/sequelize");
const { utils, now } = require("@src/utils");
const tapUtils = require("./tapUtils");
const { FEE_TYPE, FEE_STATUS, REPAYMENT_STATUS, INTEREST_FEE_STATUS } = require("../models/enum");
const itemFn = require("./itemFn");

const loanFn = {
	addNewLoan: async (accountID, planID, amount) => {
		try {
			return await db.sequelize.transaction(async t => {
				await db.loans.sync(); //bug fix?
				const results = [];
				const loan = await db.loans.createLoan(accountID, planID, amount);
				results.push(loan);
				results.push(await loanFn.createRepayment(accountID, loan.loanID));
				results.push(await db.interestFees.createInterestFee(accountID, loan.loanID));
				return results;
			});
		} catch (err) {
			return err;
		}
	},

	addLateFee: async (accountID, balanceOutstanding) => {
		const lateFee = (await db.latePaymentPlans.calcFee(balanceOutstanding)).lateFee;
		const result = await db.fees.createFee(
			accountID,
			utils.currentDate(),
			lateFee,
			FEE_TYPE.LATE,
			FEE_STATUS.OPEN
		);
		return result;
	},

	generateRepayment: async (accountID, loanID) => {
		try {
			return await db.sequelize.transaction(async t => {
				await db.loans.sync(); //bug fix?
				const results = [];
				results.push(
					await loanFn.updateLoan(accountID, loanID),
					await db.interestFees.createInterestFee(accountID, loanID),
					await loanFn.createRepayment(accountID, loanID)
				);
				return results;
			});
		} catch (err) {
			return err;
		}
	},

	createRepayment: async (accountID, loanID) => {
		const currentPlan = await loanFn.currentPlan(accountID, loanID);
		if (currentPlan) {
			const principalDue =
				currentPlan.durationMonths === currentPlan.currentRepayment
					? currentPlan.amountMoney - currentPlan.repaymentsIssued
					: currentPlan.amountMoney * currentPlan.principalRate;
			const results = await db.repayments.createRepayment(
				accountID,
				loanID,
				currentPlan.nPaymentDue,
				principalDue
			);
			return results;
		} else {
			return [];
		}
	},

	updateLoan: async (accountID, loanID) => {
		const currentPlan = await loanFn.currentPlan(accountID, loanID);
		const nPaymentDue = currentPlan != null ? tapUtils.nextPaymentDue() : null;

		const result = await db.loans.update(
			{
				nPaymentDue: nPaymentDue
			},
			{
				where: {
					loanID: loanID
				}
			}
		);

		return result;
	},

	calcAmount: async (key, amountMoney, planID, currentRepayment) => {
		const currentPlan = await db.paymentPlans.getCurrentPlan(planID, currentRepayment);
		if (key == "repayment") return amountMoney * currentPlan.principalRate;
		if (key == "interest") return amountMoney * currentPlan.interestRate;
	},

	currentPlan: async (accountID, loanID) => {
		const loan = await db.loans.getLoans(accountID, "loan", loanID);
		const currentRepayment = await db.repayments.getCurrentRepayment(loanID);
		const currentPlan = await db.paymentPlans.getCurrentPlan(loan[0].planID, currentRepayment);
		const repaymentsIssued = await db.repayments.issued(loan[0].loanID);
		return {
			...currentPlan,
			...{ currentRepayment: currentRepayment },
			...{ amountMoney: loan[0].amountMoney },
			...{ repaymentsIssued: parseInt(repaymentsIssued) },
			...{ nPaymentDue: loan[0].nPaymentDue }
		};
	}
};

module.exports = loanFn;
