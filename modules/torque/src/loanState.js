const db = require("@config/lib/sequelize");
const moment = require("moment-timezone");
const itemFn = require("./itemFn");
const { utils, now } = require("@src/utils");
const tapUtils = require("./tapUtils");
const {
	FEE_TYPE,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS
} = require("../models/enum");

const loanState = {
	getBalanceOutstanding: async (accountID, date = null) => {
		const items = itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID, date));
		const fees = itemFn.sumFees(items, FEE_STATUS.OPEN);
		const interestFees = itemFn.sumInterestFees(items, null, INTEREST_FEE_STATUS.OPEN);
		const repayments = itemFn.sumRepayments(items, null, REPAYMENT_STATUS.OPEN);
		const loans = itemFn.sumLoans(items, null, LOAN_STATUS.OPEN);
		const lastEndingBalance = items.surplus;
		// return items;
		return loans + repayments + interestFees + fees - lastEndingBalance;
	},

	getLoanBalanceOutstanding: async (accountID, loanID, date = null) => {
		const items = itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID, date));
		const nextItem = itemFn.searchItemNextPay(items);

		const interestFees = itemFn.sumInterestFees(items, loanID, INTEREST_FEE_STATUS.OPEN);
		const repayments = itemFn.sumRepayments(items, loanID, REPAYMENT_STATUS.OPEN);
		const loans = itemFn.sumLoans(items, loanID, LOAN_STATUS.OPEN);
		let surplus;
		if (nextItem) {
			surplus = nextItem.loanID == loanID ? items.surplus : 0;
		} else {
			surplus = 0;
		}

		return interestFees + repayments + loans - surplus;
	},

	getMinimumDue: async (accountID, date = null) => {
		const items = itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID, date));

		const dateDue = date ? (await db.ledger.getLedgerPeriod(accountID, date)).endDate : null;

		const fees = itemFn.sumFees(items, FEE_STATUS.OPEN, dateDue);
		const interestFees = itemFn.sumInterestFees(items, null, INTEREST_FEE_STATUS.OPEN, dateDue);
		const repayments = itemFn.sumRepayments(items, null, REPAYMENT_STATUS.OPEN, dateDue);
		const lastEndingBalance = items.surplus;

		return Math.max(0, repayments + interestFees + fees - lastEndingBalance);
	},

	nextPaymentDue: async accountID => {
		const items = await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID));
		const dueDates = [];
		const currentDate = moment(now).format("YYYY-MM-DD");
		items.interestFees.forEach(interestFee => {
			if (interestFee.dateDue >= currentDate) {
				dueDates.push(interestFee.dateDue);
			}
		});
		items.repayments.forEach(repayment => {
			if (repayment.dateDue >= currentDate) {
				dueDates.push(repayment.dateDue);
			}
		});
		items.loans.forEach(loan => {
			if (loan.nPaymentDue >= currentDate) {
				dueDates.push(loan.nPaymentDue);
			}
		});

		return !dueDates.length ? null : dueDates[0];
	}
};

module.exports = loanState;
