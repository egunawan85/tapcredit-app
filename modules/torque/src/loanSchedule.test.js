require("module-alias/register");
const itemFn = require("./itemFn");
const loanSchedule = require("./loanSchedule");
const assert = require("assert");
const moment = require("moment-timezone");
const db = require("@config/lib/sequelize");
const loanFn = require("./loanFn.js");
const { utils } = require("@src/utils");
const tapUtils = require("./tapUtils");
const {
	PAYMENT_TYPE,
	FEE_STATUS,
	INTEREST_FEE_STATUS,
	REPAYMENT_STATUS,
	LOAN_STATUS
} = require("../models/enum");

const accountID = 3;
const planID = [1, 2];
const interestRate = [0.025, 0.015]; //accordin to planID
const durationMonths = [3, 6]; //according to planID
const loan = [10000000, 5000000];
const payments = [
	loan[0] * interestRate[0],
	loan[1] * interestRate[1],
	loan[0] / durationMonths[0],
	loan[1] / durationMonths[1],
	loan[0] - loan[0] / durationMonths[0]
];

before(async () => {
	await db.repayments.destroy({ where: {}, force: true });
	await db.interestFees.destroy({ where: {}, force: true });
	await db.fees.destroy({ where: {}, force: true });
	await db.ledger.destroy({ where: {}, force: true });
	await db.ledgerDetails.destroy({ where: {}, force: true });
	await db.payments.destroy({ where: {}, force: true });
	await db.overpayments.destroy({ where: {}, force: true });
	await db.loans.destroy({ where: {}, force: true });
});

describe("Loan Payment Schedule", async () => {
	it("Two loans are created", async () => {
		const loanOne = await loanFn.addNewLoan(accountID, planID[0], loan[0]);
		const loanTwo = await loanFn.addNewLoan(accountID, planID[1], loan[1]);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.ok(schedule);
	});

	it("Schedule dates are correct", async () => {
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		schedule.forEach((loan, index) => {
			assert.equal(loan.payments.length, durationMonths[index]);
			loan.payments.forEach((payment, key) => {
				if (key < durationMonths[index] - 1) {
					assert.equal(
						tapUtils.nextPaymentDue(
							false,
							moment(payment.dateDue)
								.add(1, "days")
								.format("YYYY-MM-DD")
						),
						loan.payments[key + 1].dateDue
					);
				}
			});
		});
	});

	it("Repayments and interest fees are correct", async () => {
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		schedule.forEach((loan, index) => {
			let totalRepayments = 0;
			loan.payments.forEach((payment, key) => {
				totalRepayments += payment.principalRepayment;
			});
			assert.equal(totalRepayments, loan.amount);
			assert.equal(loan.payments[0].interestDue, loan.amount * interestRate[index]);
		});
	});

	it("Payment 1 is made: Checks interest due is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[0],
			utils.currentDateTime(),
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule[0].payments[0].interestDue, 0);
		assert.equal(schedule[1].payments[0].interestDue, loan[1] * interestRate[1]);
	});

	it("Payment 2 is made: Checks interest due is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[1],
			utils.currentDateTime(),
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule[0].payments[0].interestDue, 0);
		assert.equal(schedule[1].payments[0].interestDue, 0);
	});

	it("Payment 3 is made: Checks principal repayment is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[2],
			utils.currentDateTime(),
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule[0].payments[0].principalRepayment, 0);
		assert.equal(
			schedule[1].payments[0].principalRepayment,
			Math.round(loan[1] / durationMonths[1])
		);
	});

	it("Payment 4 is made: Checks principal repayment is correct", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[3],
			utils.currentDateTime(),
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule[0].payments[0].principalRepayment, 0);
		assert.equal(schedule[1].payments[0].principalRepayment, 0);
	});

	it("Payment 5 is made: Checks all loans are still captured", async () => {
		const payment = await db.payments.createPayment(
			accountID,
			payments[4],
			utils.currentDateTime(),
			"poipqcaf",
			PAYMENT_TYPE.TRANSFER,
			"apcipasdf"
		);
		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule.length, 2);
	});

	it("Checks loan complete after function setItemsComplete", async () => {
		const results = await itemFn.setItemsComplete(
			await itemFn.allocatePaymentsToItems(await itemFn.getItems(accountID)),
			utils.currentDateTime()
		);

		const schedule = await loanSchedule.getPaymentSchedule(accountID);

		assert.equal(schedule.length, 1);
	});
});
