const db = require("@config/lib/sequelize");
const loanState = require("../src/loanState.js");
const loanSchedule = require("../src/loanSchedule.js");

class Torque {
	static async loans(accountID) {
		return await db.loans.getLoans(accountID, "status", null, true);
	}

	static async balanceOutstanding(accountID, loanID = null, date = null) {
		return loanID
			? await loanState.getLoanBalanceOutstanding(accountID, loanID, date)
			: await loanState.getBalanceOutstanding(accountID, date);
	}

	static async minimumDue(accountID, date = null) {
		return await loanState.getMinimumDue(accountID, date);
	}

	static async paymentSchedule(accountID, all = true) {
		return await loanSchedule.getPaymentSchedule(accountID, all);
	}

	static async simulateSchedule(amount, planID) {
		return await loanSchedule.simulatePaymentSchedule(amount, planID);
	}

	static async nextPaymentDue(accountID) {
		return await loanState.nextPaymentDue(accountID);
	}

	//Transactionhistory
	//vendor (cash loans)
	//customer (cash loans / product or service loan)
}

module.exports = Torque;
