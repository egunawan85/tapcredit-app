const gmail = require("../config/lib/gmail");

class Mailer {
	constructor(recipient, subject, content, sender = "support@tapcredit.co.id") {
		this.mail = {
			from: sender,
			to: recipient,
			subject: subject,
			generateTextFromHTML: true,
			html: content
		};
	}

	send() {
		gmail.sendMail(this.mail, (error, response) => {
			error ? console.log(error) : console.log(response);
			gmail.close();
			return error ? error : response;
		});
		// const send = await gmail.sendMail(this.mail);
		// return send;
		//consider this in future.  need loading animation
	}
}

module.exports = Mailer;
