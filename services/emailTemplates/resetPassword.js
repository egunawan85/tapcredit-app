module.exports = url => {
    return `
    <p>Hai!<br>
    <br>
    Untuk mengatur ulang kata sandi dari akun TapCredit bisnis Anda,
    silakan klik tautan di bawah ini. Tautan ini akan mengatur ulang kata
    sandi dan membuat kata sandi yang baru.<br>
    <br>
    ${url}<br>
    <br>
    Jika Anda tidak mengirimkan permintaan pengaturan ulang Lupa Kata
    Sandi, silakan laporkan email ini kepada kami di support@tapcredit.co.id<br>
    <br>
    Terima kasih sudah menggunakan TapCredit!</p>
  `;
};
