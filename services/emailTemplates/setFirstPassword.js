module.exports = url => {
    return `
    <p>Hi!<br>
    <br>
    Untuk mengatur kata sandi dari akun TapCredit Anda, silakan klik tautan di bawah ini.
    Tautan ini akan membuat kata sandi yang baru.<br>
    <br>
    ${url}<br>
    <br>
    Jika Anda tidak mendaftar untuk bergabung dengan TapCredit, silakan laporkan email ini kepada kami di support@tapcredit.co.id<br>
    <br>
    Terima kasih sudah menggunakan TapCredit!</p>
  `;
};
