module.exports = {
  apps: [
    {
      name: "TapCredit.Server",
      script: "index.js",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};
