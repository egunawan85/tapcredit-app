const passport = require("passport");
const bCrypt = require("bcrypt-nodejs");
const LocalStrategy = require("passport-local").Strategy;
const db = require("@config/lib/sequelize");

const User = require("@modules/users/lib/User");
const user = new User();

// LOCAL SIGNIN
passport.use(
	"local-signin",
	new LocalStrategy(
		{
			usernameField: "username",
			passwordField: "password",
			passReqToCallback: true
		},

		async (req, username, password, done) => {
			const isValidPassword = function(userpass, password) {
				return bCrypt.compareSync(password, userpass);
			};
			const isAccountActive = function(userAccount) {
				return userAccount.mainRole != "pending" ? true : false;
			};
			user.initialize("nicename", username)
				.then(userAccount => {
					if (!userAccount) {
						return done(null, false, {
							message: "Tidak dapat menemukan Akun TapCredit Anda"
						});
					}
					if (!isValidPassword(userAccount.userPass, password)) {
						return done(null, false, {
							message: "Sandi salah. Coba lagi atau klik lupa sandi untuk menyetel ulang."
						});
					}
					if (!isAccountActive(userAccount)) {
						return done(null, false, {
							message: "Akun Anda belum aktif."
						});
					}
					return done(null, userAccount);
				})
				.catch(err => {
					console.log("Error:", err);
					return done(null, false, {
						message: "Ada kesalahan yang terjadi"
					});
				});
		}
	)
);

//LOCAL SIGNUP
passport.use(
	"local-signup",
	new LocalStrategy(
		{
			usernameField: "username",
			passwordField: "password",
			passReqToCallback: true
		},

		function(req, username, password, done) {
			const generateHash = function(password) {
				return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
			};
			db.users
				.findOne({
					where: {
						userNicename: username
					},
					raw: true
				})
				.then(found => {
					if (found) {
						return done(null, false, {
							message: "Email sudah terdaftar"
						});
					} else {
						const userPassword = generateHash(password);
						const userRole = `["${req.body.userRole}"]`;
						const data = {
							userLogin: username,
							userPass: userPassword,
							userNicename: username,
							userEmail: username,
							userRoles: userRole
						};
						db.users.create(data).then(async (newUser, created) => {
							if (!newUser) {
								return done(null, false);
							}
							if (newUser) {
								await User.setProfile(newUser.userID, req.body.profile);
								return req.user ? done(null, req.user) : done(null, newUser);
							}
						});
					}
				});
		}
	)
);

passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	// Users.findById(userID, function(err, user) {
	done(null, user);
	// });
});

module.exports = passport;
