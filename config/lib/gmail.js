const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const keys = require("../keys");
const OAuth2 = google.auth.OAuth2;

const oauth2Client = new OAuth2(keys.gmailClientID, keys.gmailClientSecret, keys.gmailRedirectURL);

oauth2Client.setCredentials({
	refresh_token: keys.gmailRefreshToken
});

const accessToken = oauth2Client.refreshAccessToken().then(res => res.credentials.access_token);

module.exports = nodemailer.createTransport({
	service: "gmail",
	auth: {
		type: "OAuth2",
		user: "support@tapcredit.co.id",
		clientId: keys.gmailClientID,
		clientSecret: keys.gmailClientSecret,
		refreshToken: keys.gmailRefreshToken,
		accessToken: accessToken
	}
});
