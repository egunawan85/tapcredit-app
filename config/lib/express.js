const express = require("express");
const app = express();
const cookieSession = require("cookie-session");
const bodyParser = require("body-parser");
const passport = require("./passport");
const path = require("path");
const keys = require("@config/keys");
const proxy = require("http-proxy-middleware");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(
	cookieSession({
		maxAge: 30 * 24 * 60 * 60 * 1000,
		keys: [keys.cookieKey]
	})
);

app.use(passport.initialize());
app.use(passport.session());

module.exports = app;
module.exports.start = function() {
	require("@config/lib/sequelize");
	require("@src/activate");
};
