const Sequelize = require("sequelize");
const env = require("../env");
const cls = require("continuation-local-storage"),
	namespace = cls.createNamespace("namespace");

Sequelize.useCLS(namespace);
const Op = Sequelize.Op;
const operatorsAliases = {
	$gt: Op.gt,
	$gte: Op.gte,
	$lt: Op.lt,
	$lte: Op.lte,
	$ne: Op.ne
};

const sequelize = new Sequelize(env.database, env.username, env.password, {
	host: env.host,
	port: env.port,
	dialect: "mysql",
	operatorsAliases: false,
	logging: false,
	timezone: "+07:00",

	pool: {
		max: 30,
		min: 0,
		acquire: 30000,
		idle: 10000
	},
	operatorsAliases
});

sequelize
	.authenticate()
	.then(() => {
		console.log("Connection has been established successfully.");
	})
	.catch(err => {
		console.error("Unable to connect to the database:", err);
	});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//models/tables
db.users = require("@modules/users/models/users")(sequelize, Sequelize);
db.usermeta = require("@modules/users/models/usermeta")(sequelize, Sequelize);

db.applications = require("@modules/applications/models/applications")(sequelize, Sequelize);
db.applicationFields = require("@modules/applications/models/applicationFields")(sequelize, Sequelize);
db.applicationForms = require("@modules/applications/models/applicationForms")(sequelize, Sequelize);

db.repayments = require("@modules/torque/models/repayments")(sequelize, Sequelize);
db.overpayments = require("@modules/torque/models/overpayments")(sequelize, Sequelize);
db.loans = require("@modules/torque/models/loans")(sequelize, Sequelize);
db.interestFees = require("@modules/torque/models/interestFees")(sequelize, Sequelize);
db.fees = require("@modules/torque/models/fees")(sequelize, Sequelize);
db.latePaymentPlans = require("@modules/torque/models/latePaymentPlans")(sequelize, Sequelize);
db.ledger = require("@modules/torque/models/ledger")(sequelize, Sequelize);
db.ledgerDetails = require("@modules/torque/models/ledgerDetails")(sequelize, Sequelize);
db.paymentPlans = require("@modules/torque/models/paymentPlans")(sequelize, Sequelize);
db.paymentPlanDetails = require("@modules/torque/models/paymentPlanDetails")(sequelize, Sequelize);
db.payments = require("@modules/torque/models/payments")(sequelize, Sequelize);
db.accounts = require("@modules/citizen/models/accounts")(sequelize, Sequelize);
db.creditLines = require("@modules/citizen/models/creditLines")(sequelize, Sequelize);
db.transactions = require("@modules/citizen/models/transactions")(sequelize, Sequelize);

//associations
db.users.hasMany(db.usermeta, { foreignKey: "userID", sourceKey: "userID" });

db.users.hasMany(db.applications, { foreignKey: "ownerUserID", sourceKey: "userID" });
db.users.hasMany(db.applications, { foreignKey: "applicantUserID", sourceKey: "userID" });
db.applications.hasMany(db.applicationFields, { foreignKey: "applicationID", sourceKey: "applicationID" });

db.users.hasMany(db.accounts, { foreignKey: "userID", sourceKey: "userID" });
db.accounts.hasMany(db.loans, { foreignKey: "accountID", sourceKey: "accountID" });
db.accounts.hasMany(db.creditLines, { foreignKey: "accountID", sourceKey: "accountID" });
db.accounts.hasMany(db.payments, { foreignKey: "accountID", sourceKey: "accountID" });
db.loans.hasMany(db.repayments, { foreignKey: "loanID", sourceKey: "loanID" });
db.repayments.belongsTo(db.loans, {
	foreignKey: "loanID",
	targetKey: "loanID"
});
db.loans.hasMany(db.interestFees, {
	foreignKey: "loanID",
	sourceKey: "loanID"
});
db.interestFees.belongsTo(db.loans, {
	foreignKey: "loanID",
	targetKey: "loanID"
});
db.loans.hasMany(db.overpayments, {
	foreignKey: "loanID",
	sourceKey: "loanID"
});
db.paymentPlans.hasMany(db.paymentPlanDetails, {
	foreignKey: "planID",
	sourceKey: "planID",
	onDelete: "cascade"
});
db.ledger.hasMany(db.ledgerDetails, {
	foreignKey: "ledgerID",
	sourceKey: "ledgerID",
	onDelete: "cascade"
});

module.exports = db;
