require("module-alias/register");
const express = require("express");
const path = require("path");
const app = require("./config/lib/express");

require("./modules/users/routes/authRoutes")(app);
require("./modules/users/routes/userRoutes")(app);
require("./modules/applications/routes/applicationRoutes")(app);
require("./modules/snap/routes")(app);
require("./modules/torque/routes/torqueRoutes")(app);
require("./modules/citizen/routes/citizenRoutes")(app);

app.start();

app.get("/", (req, res) => {
	res.redirect("/api/router");
});

if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "production") {
	app.use(express.static("client/build"));
	const path = require("path");
	app.get("/portal*", (req, res) => {
		res.sendFile(path.resolve("client", "build", "index.html"));
	});
	app.get("/login*", (req, res) => {
		res.sendFile(path.resolve("client", "build", "index.html"));
	});
}

if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "production") {
	app.use(express.static("admin/build"));
	const path = require("path");
	app.get("/agent*", (req, res) => {
		req.user ? res.sendFile(path.resolve("admin", "build", "index.html")) : res.redirect("/api/router");
	});
	app.get("/admin*", (req, res) => {
		req.user ? res.sendFile(path.resolve("admin", "build", "index.html")) : res.redirect("/api/router");
	});
}

app.get("/*", (req, res) => {
	res.redirect("/api/router");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);
